'use strict';

// Use local.env.js for environment variables that grunt will set when the server starts locally.
// Use for your api keys, secrets, etc. This file should not be tracked by git.
//
// You will need to set these on the server you deploy to.

module.exports = {
  PORT: 9001,

  AWS_ACCESS_KEY_ID: '',
  AWS_S3_NEWS_BUCKET: 'o',
  AWS_S3_SENTRYFILE_BUCKET: '',
  AWS_SECRET_ACCESS_KEY: '',

  TWO_FACTOR_KEY: '',

  CLIENT_URL: 'http://localhost:9000'
};
