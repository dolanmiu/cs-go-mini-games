/*jslint node: true */
'use strict';

// Development specific configuration
// ==================================
module.exports = {
    // MongoDB connection options
    mongo: {
        uri: 'mongodb://localhost/csgominigames-dev'
    },

    seedDB: true,

    botServerUrl: 'http://localhost:9004',

    port: 9001,

    alwaysAdmin: true,

    tradeTimeOut: 60000,

    item: {
        priceThreshold: 0,
        coinRatio: {
            between1and5: 800,
            between5and10: 900,
            above10: 1000
        }
    },

    game: {
        crate: {
            anounceWinningItemTime: 10000,
            anounceWinnerTime: 10000
        }
    },

    loyaltyPoints: {
        leaderboard: {
            count: 50
        },
        giveaway: {
            amount: 250,
            times: [{
                hours: 0,
                minutes: 0
            }, {
                hours: 8,
                minutes: 0
            }, {
                hours: 16,
                minutes: 0
            }]
        }
    }
};