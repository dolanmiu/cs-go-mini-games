/*jslint node: true */
'use strict';

// Production specific configuration
// =================================
module.exports = {
    // Server IP
    ip: process.env.OPENSHIFT_NODEJS_IP ||
        process.env.IP ||
        undefined,

    // Server port
    port: process.env.OPENSHIFT_NODEJS_PORT ||
        process.env.PORT ||
        8080,

    // MongoDB connection options
    mongo: {
        uri: process.env.MONGOLAB_URI ||
            process.env.MONGOHQ_URL ||
            process.env.OPENSHIFT_MONGODB_DB_URL +
            process.env.OPENSHIFT_APP_NAME ||
            'mongodb://localhost/admin'
    },

    botServerUrl: 'http://localhost:9004',

    tradeTimeOut: 300000,

    item: {
        priceThreshold: 0,
        coinRatio: {
            between1and5: 800,
            between5and10: 900,
            above10: 1000
        }
    },

    game: {
        crate: {
            anounceWinningItemTime: 10000,
            anounceWinnerTime: 10000
        }
    },

    loyaltyPoints: {
        leaderboard: {
            count: 50
        },
        giveaway: {
            amount: 250,
            times: [{
                hours: 0,
                minutes: 0
            }, {
                hours: 8,
                minutes: 0
            }, {
                hours: 16,
                minutes: 0
            }]
        }
    }

};