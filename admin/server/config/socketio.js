/**
 * Socket.io configuration
 */
/*jslint node: true */
'use strict';

import config from './environment';

// When the user disconnects.. perform this
function onDisconnect(socket) {}

// When the user connects.. perform this
function onConnect(socket) {
    // When the client emits 'info', this listens and executes
    socket.on('info', function (data) {
        socket.log(JSON.stringify(data, null, 2));
    });

    // Insert sockets below
    require('../api/item/item.socket').register(socket);
    require('../api/news/news.socket').register(socket);
    require('../api/bot/bot.socket').register(socket);
    require('../api/crate/crate.socket').register(socket);
    require('../api/steam-user/user.socket').register(socket);
    require('../api/jackpot-log/jackpot-log.socket').register(socket);
    require('../api/jackpot-chat/jackpot-chat.socket').register(socket);
    require('../api/crate-game-log/crate-game-log.socket').register(socket);
    require('../api/jackpot-entry/jackpot-entry.socket').register(socket);
    require('../api/trade-offer/trade-offer.socket').register(socket);
    require('../api/announcement/announcement.socket').register(socket);
    require('../api/crate-game-chat/crate-game-chat.socket').register(socket);
}

module.exports = function (socketio) {
    // socket.io (v1.x.x) is powered by debug.
    // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
    //
    // ex: DEBUG: "http*,socket.io:socket"

    // We can authenticate socket.io users and access their token through socket.decoded_token
    //
    // 1. You will need to send the token in `client/components/socket/socket.service.js`
    //
    // 2. Require authentication here:
    // socketio.use(require('socketio-jwt').authorize({
    //   secret: config.secrets.session,
    //   handshake: true
    // }));

    socketio.on('connection', function (socket) {
        socket.address = socket.request.connection.remoteAddress +
            ':' + socket.request.connection.remotePort;

        socket.connectedAt = new Date();

        socket.log = function (...data) {
            console.log(`SocketIO ${socket.nsp.name} [${socket.address}]`, ...data);
        };

        // Call onDisconnect.
        socket.on('disconnect', function () {
            onDisconnect(socket);
            socket.log('DISCONNECTED');
        });

        // Call onConnect.
        onConnect(socket);
        socket.log('CONNECTED');
    });
};