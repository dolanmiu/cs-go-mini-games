/*jslint node: true */
'use strict';

var express = require('express');

var controller = require('./email.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.post('/send', auth.isAuthenticated(), controller.send);
router.post('/testSend', auth.isAuthenticated(), controller.testSend);

module.exports = router;