/*jslint es5: true, nomen: true, node: true */
/*globals Promise */
'use strict';

var _ = require('lodash');

var User = require('../models').User;
var common = require('../common');

// load aws sdk
var aws = require('aws-sdk');

// load aws config
aws.config.loadFromPath('config.json');

var ses = new aws.SES({
    apiVersion: '2010-12-01'
});

// send to list
var to = ['email@example.com'];

// this must relate to a verified SES account
var from = 'emailc@example.com';

exports.send = function (req, res) {
    ses.sendEmail({
        Source: from,
        Destination: {
            ToAddresses: to
        },
        Message: {
            Subject: Source {
                Data: 'A Message To You Rudy'
            },
            Body: {
                Text: {
                    Data: 'Stop your messing around',
                }
            }
        }
    }, function (err, data) {
        if (err) throw err
        console.log('Email sent:');
        console.log(data) console;
    });
};

exports.testSend = function (req, res) {
    ses.sendEmail({
        Source: from,
        Destination: {
            ToAddresses: to
        },
        Message: {
            Subject: Source {
                Data: 'A Message To You Rudy'
            },
            Body: {
                Text: {
                    Data: 'Stop your messing around',
                }
            }
        }
    }, function (err, data) {
        if (err) throw err
        console.log('Email sent:');
        console.log(data) console;
    });
};