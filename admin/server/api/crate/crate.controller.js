/*jslint nomen: true, node: true */
'use strict';

var Crate = require('../models').Crate;
var Item = require('../models').Item;
var common = require('../common');

// Get list of crates
exports.index = function (req, res) {
    Crate.find().populate('inventory.item')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single crate
exports.show = function (req, res) {
    Crate.findById(req.params.id).populate('inventory.item')
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};


// Creates a new crate in the DB.
exports.create = function (req, res) {
    Crate.create(req.body)
        .then(common.responseWithResult(res, 201))
        .then(null, common.handleError(res));
};

// Updates an existing crate in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    console.log(req.body);
    Crate.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a crate from the DB.
exports.destroy = function (req, res) {
    Crate.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};

exports.test = function (req, res) {
    Crate.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(function (crate) {
            return crate.getRandomPrize();
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};