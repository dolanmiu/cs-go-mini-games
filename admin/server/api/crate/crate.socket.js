/*jslint nomen: true, node: true */
'use strict';

var Crate = require('../models').Crate;

function onSave(socket, doc, cb) {
    Crate.populate(doc, {
        path: 'inventory.item'
    }, function (err, crate) {
        socket.emit('crate:save', crate);
    });
}

function onRemove(socket, doc, cb) {
    socket.emit('crate:remove', doc);
}

exports.register = function (socket) {
    Crate.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    Crate.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};