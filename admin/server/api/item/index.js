/*jslint node: true */
'use strict';

var express = require('express');

var controller = require('./item.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/count', auth.isAuthenticated(), controller.count);
router.get('/blacklist', auth.isAuthenticated(), controller.blacklist);
router.get('/custom', auth.isAuthenticated(), controller.getCustom);

router.get('/search/:query', auth.isAuthenticated(), controller.search);
router.get('/:id/list/:quantity', auth.isAuthenticated(), controller.list);

router.put('/:id/price', auth.isAuthenticated(), controller.customPrice);

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;