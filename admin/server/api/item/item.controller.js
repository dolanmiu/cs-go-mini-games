/*jslint es5: true, nomen: true, node: true  */
'use strict';

var _ = require('lodash');

var Item = require('../models').Item;
var common = require('../common');

// Gets a list of Items
exports.index = function (req, res) {
    Item.find()
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Gets a single Item from the DB
exports.show = function (req, res) {
    Item.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Creates a new Item in the DB
exports.create = function (req, res) {
    Item.create(req.body)
        .then(common.responseWithResult(res, 201))
        .then(null, common.handleError(res));
};

// Updates an existing Item in the DB
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Item.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a Item from the DB
exports.destroy = function (req, res) {
    Item.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};

exports.count = function (req, res) {
    Item.getTotalCount()
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get list of items
exports.list = function (req, res) {
    var quantity = req.params.quantity || 100;

    Item.find({
        $or: [{
            'price.steam': {
                $gt: 1
            }
        }, {
            'price.custom': {
                $gt: 1
            }
        }]
    }).skip(req.params.id * quantity).limit(quantity).then(common.handleEntityNotFound(res)).then(common.responseWithResult(res)).then(null, common.handleError(res));
};

// Get list of blacklist items
exports.blacklist = function (req, res, next) {
    Item.find({
        'blacklist': true
    }).then(common.handleEntityNotFound(res)).then(common.responseWithResult(res)).then(null, common.handleError(res));
};

// Change custom itema
exports.getCustom = function (req, res) {
    Item.find({
        'custom': true
    }).then(common.handleEntityNotFound(res)).then(common.responseWithResult(res)).then(null, common.handleError(res));
};

// Search
exports.search = function (req, res) {
    var tokens = req.params.query.split(/[ ,]+/),
        regex = '';

    tokens.forEach(function (token) {
        regex += '(?=.*' + token + ')';
    });

    Item.find({
        'item.marketName': {
            "$regex": regex,
            "$options": "i"
        }
    }).then(common.handleEntityNotFound(res)).then(common.responseWithResult(res)).then(null, common.handleError(res));
};

exports.customPrice = function (req, res) {
    Item.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(function (item) {
            if (req.body.value) {
                item.price.custom = req.body.value;
            } else {
                item.price.custom = undefined;
            }
            return item;
        })
        .then(common.saveEntity(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};