/*jslint nomen: true, node: true */
'use strict';

var JackpotLog = require('../models').JackpotLog;

function onSave(socket, doc, cb) {
    JackpotLog.populate(doc, {
        path: 'inventory.item'
    }, function (err, jackpotLog) {
        socket.emit('jackpotLog:save', jackpotLog);
    });
}

function onRemove(socket, doc, cb) {
    socket.emit('jackpotLog:remove', doc);
}

exports.register = function (socket) {
    JackpotLog.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    JackpotLog.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};