/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

var JackpotLog = require('../models').JackpotLog;
var common = require('../common');

// Get list of jackpotLogs
exports.index = function (req, res) {
    JackpotLog.paginate({}, { page: req.query.page, limit: req.query.limit, sort: '-date', populate: 'winnerInfo.user' })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single jackpotLog
exports.show = function (req, res) {
    JackpotLog.findById(req.params.id).populate('pot.entries.item').populate('pot.entries.user').populate('cut')
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a jackpotLog from the DB.
exports.destroy = function (req, res) {
    JackpotLog.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};