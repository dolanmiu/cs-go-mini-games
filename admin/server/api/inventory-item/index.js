/*jslint node: true */
'use strict';

var express = require('express');

var controller = require('./inventory-item.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/count', auth.isAuthenticated(), controller.count);
router.get('/jackpot', auth.isAuthenticated(), controller.jackpot);
router.put('/sync', auth.isAuthenticated(), controller.sync);
router.get('/duplicates', auth.isAuthenticated(), controller.findDuplicates);
router.get('/staleItems', auth.isAuthenticated(), controller.findStaleItems);
router.get('/unlinkedItems', auth.isAuthenticated(), controller.findUnlinkedItems);
router.get('/desyncedItems', auth.isAuthenticated(), controller.findDesyncedItems);
router.post('/metadata', auth.isAuthenticated(), controller.createMetadata);

router.put('/:id/refundJackpotEntry', auth.isAuthenticated(), controller.refundJackpotEntry);

router.get('/:id', auth.isAuthenticated(), controller.show);
router.post('/', auth.isAuthenticated(), controller.create);
router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;