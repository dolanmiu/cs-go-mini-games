/*jslint es5: true, nomen: true, node: true */
/*globals Promise */
'use strict';

var _ = require('lodash');

var InventoryItem = require('../models').InventoryItem;
var Bot = require('../models').Bot;
var User = require('../models').User;
var trade = require('../../trade');
var common = require('../common');

function createQuery(userId, unowned, botModels) {
    var query = {};

    if (userId) {
        query.user = userId;
    }

    if (unowned) {
        query.user = {
            $exists: false
        };
    }

    if (botModels) {
        query.bot = {
            $in: botModels
        }
    }

    return query;
}

function bankCount(obj) {
    return new Promise(function (resolve, reject) {
        Bot.find({
            bankBot: true
        }).then(function (bots) {
            InventoryItem.count({
                    'bot': {
                        $in: bots
                    }
                })
                .then(function (result) {
                    obj.bank = result;
                    resolve(obj);
                });
        });
    });
}

function depositCount(obj) {
    return new Promise(function (resolve, reject) {
        Bot.find({
            depositBot: true
        }).then(function (bots) {
            InventoryItem.count({
                    'bot': {
                        $in: bots
                    }
                })
                .then(function (result) {
                    obj.deposit = result;
                    resolve(obj);
                })
        });
    });
}

function jackpotCount(obj) {
    return new Promise(function (resolve, reject) {
        InventoryItem.count({
                inJackpot: true
            })
            .then(function (result) {
                obj.jackpot = result;
                resolve(obj);
            });
    });
}

function totalCount(obj) {
    return new Promise(function (resolve, reject) {
        InventoryItem.count()
            .then(function (result) {
                obj.total = result;
                resolve(obj);
            });
    });
}

function createBotQuery(type) {
    var query = {};

    switch (type) {
    case 'bank':
        query.bankBot = true;
        break;
    case 'deposit':
        query.depositBot = true;
        break;
    }
    return query;
}

// Gets a list of InventoryItem
exports.index = function (req, res) {

    req.query.page = req.query.page || 1;
    req.query.limit = req.query.limit || 10000;

    var promise = new Promise(function (resolve, reject) {
        if (req.query.type || req.query.type != 'all') {
            Bot.find(createBotQuery(req.query.type)).then(function (bots) {
                resolve(createQuery(req.query.user, req.query.unowned, bots))
            });
        } else {
            resolve(createQuery(req.query.user, req.query.unowned));
        }
    });

    promise.then(function (query) {
        InventoryItem.paginate(query, {
                page: req.query.page,
                limit: req.query.limit,
                sort: '-date',
                populate: {
                    path: 'item user',
                    select: 'item name'
                }
            })
            .then(common.responseWithResult(res))
            .then(null, common.handleError(res));
    });
};

// Gets a single InventoryItem from the DB
exports.show = function (req, res) {
    InventoryItem.findById(req.params.id).populate('item user')
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Creates a new InventoryItem in the DB
exports.create = function (req, res) {
    InventoryItem.create(req.body)
        .then(common.responseWithResult(res, 201))
        .then(null, common.handleError(res));
};

// Updates an existing InventoryItem in the DB
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    InventoryItem.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a InventoryItem from the DB.
exports.destroy = function (req, res) {
    InventoryItem.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};

exports.count = function (req, res) {
    var obj = {};
    totalCount(obj)
        .then(function (obj) {
            var promise;
            depositCount(obj).then(function (obj) {
                bankCount(obj).then(function (obj) {
                    jackpotCount(obj)
                        .then(common.responseWithResult(res))
                        .then(null, common.handleError(res));
                });
            });
        });
};

exports.sync = function (req, res) {
    trade.syncInventorytoBots()
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.jackpot = function (req, res) {
    InventoryItem.findInJackpot().populate('item user bot')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.findDuplicates = function (req, res) {
    InventoryItem.find().populate('item user').then(function (inventoryItems) {
        var promises = [];

        inventoryItems.forEach(function (inventoryItem) {
            var promise = new Promise(function (resolve, reject) {
                InventoryItem.find({
                    assetId: inventoryItem.assetId
                }).then(function (inventoryItems) {
                    if (inventoryItems.length > 1) {
                        return resolve(inventoryItem);
                    }
                    resolve();
                });
            });

            promises.push(promise);
        });

        Promise.all(promises)
            .then(function (inventoryItems) {
                return _.compact(inventoryItems);
            })
            .then(common.responseWithResult(res))
            .then(null, common.handleError(res));
    });
};

exports.findStaleItems = function (req, res) {
    trade.findStaleItems()
        .then(function (obj) {
            return obj.body;
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.findUnlinkedItems = function (req, res) {
    trade.findUnlinkedItems()
        .then(function (obj) {
            return obj.body;
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.createMetadata = function (req, res) {
    InventoryItem.find().populate('item').then(function (inventoryItems) {
        var promises = [];

        inventoryItems.forEach(function (inventoryItem) {
            inventoryItem.metadata = {
                name: inventoryItem.item.item.marketName,
                price: inventoryItem.item.price.value
            }

            promises.push(inventoryItem.save());
        });

        Promise.all(promises)
            .then(common.responseWithResult(res))
            .then(null, common.handleError(res));
    });
};

exports.refundJackpotEntry = function (req, res) {
    InventoryItem.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(function (inventoryitem) {
            inventoryitem.inJackpot = false;
            return inventoryitem;
        })
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.findDesyncedItems = function (req, res) {
    trade.findDesynced()
        .then(function (obj) {
            return obj.body;
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
}