/*jslint nomen: true, node: true */
'use strict';

var CrateGameChat = require('../models').CrateGameChat;
var common = require('../common');

// Get list of crateGameChats
exports.index = function (req, res) {
    CrateGameChat.find().populate('user').sort('date')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single crateGameChat
exports.show = function (req, res) {
    CrateGameChat.findById(req.params.id).populate('user')
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};


// Creates a new crateGameChat in the DB.
exports.create = function (req, res) {
    CrateGameChat.create(req.body)
        .then(common.responseWithResult(res, 201))
        .then(null, common.handleError(res));
};

// Updates an existing crateGameChat in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    console.log(req.body);
    CrateGameChat.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a crateGameChat from the DB.
exports.destroy = function (req, res) {
    CrateGameChat.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};