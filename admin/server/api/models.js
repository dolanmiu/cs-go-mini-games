/*jslint node: true */
var mongoose = require('bluebird').promisifyAll(require('mongoose'));
var config = require('../config/environment');

module.exports = require('@dolanmiu/mongoose-models')(mongoose, config, process.env);