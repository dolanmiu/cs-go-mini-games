/*jslint nomen: true, node: true */
'use strict';

var _ = require('lodash');
var multiparty = require('multiparty');
var util = require('util');
var fs = require('fs');
var Qs = require('qs');
var AWS = require('aws-sdk');
var path = require('path');

var News = require('../models').News;
var common = require('../common');

var bucket = process.env.AWS_S3_NEWS_BUCKET;
var s3Client = new AWS.S3({
    accessKeyId: process.env.AWS_ACCESS_KEY_ID,
    secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
});

function handleError(res, err) {
    return res.send(500, err);
}

// Get list of newss
exports.index = function (req, res) {
    News.find({}).sort('-date')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.getPage = function (req, res) {
    News.find({}).sort('-date')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single news
exports.show = function (req, res) {
    News.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Creates a new news in the DB.
exports.create = function (req, res) {
    var form = new multiparty.Form();

    form.parse(req, function (err, fields, files) {
        if (!files || !fields) {
            return handleError(res, err);
        }
        var file = files.file[0],
            contentType = file.headers['content-type'],
            body = {},
            buffer = fs.readFileSync(file.path),
            qString = '';

        _.forEach(fields, function (value, key) {
            qString += key + '=' + value + '&';
        });

        body = Qs.parse(qString);
        console.log(body);

        News.create(body, function (err, news) {
            if (err) {
                return handleError(res, err);
            }
            s3Client.putObject({
                Bucket: bucket,
                Key: String(news._id) + '/thumbnail',
                ACL: 'public-read',
                Body: buffer,
                ContentLength: buffer.byteCount
            }, function (err, data) {
                if (err) {
                    throw err;
                }
                return res.json(201, news);
            });
        });
    });
};

exports.updateThumbnail = function (req, res) {
    var form = new multiparty.Form();

    form.parse(req, function (err, fields, files) {
        var file = files.file[0],
            contentType = file.headers['content-type'],
            buffer = fs.readFileSync(file.path);

        News.findById(req.params.id)
            .then(common.handleEntityNotFound(res))
            .then(function (news) {
                s3Client.putObject({
                    Bucket: bucket,
                    Key: String(news._id) + '/thumbnail',
                    ACL: 'public-read',
                    Body: buffer,
                    ContentLength: buffer.byteCount
                }, function (err, data) {
                    return res.json(201, news);
                });
            })
            .then(null, common.handleError(res));
    });
};

// Updates an existing news in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    News.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a news from the DB.
exports.destroy = function (req, res) {
    News.findById(req.params.id, function (err, news) {
        if (err) {
            return handleError(res, err);
        }
        if (!news) {
            return res.status(404).send('Not Found');
        }
        news.remove(function (err) {
            if (err) {
                return handleError(res, err);
            }

            s3Client.listObjects({
                Bucket: bucket,
                Prefix: String(news._id) + '/'
            }, function (err, data) {
                if (err) {
                    return handleError(res, err);
                }

                var params = {
                    Bucket: bucket,
                    Delete: {
                        Objects: []
                    }
                };

                data.Contents.forEach(function (content) {
                    params.Delete.Objects.push({
                        Key: content.Key
                    });
                });

                s3Client.deleteObjects(params, function (err, data) {
                    if (err) {
                        return handleError(res, err);
                    }
                    return res.send(204);
                });
            });
        });
    });
};