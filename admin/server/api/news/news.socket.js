/*jslint node: true */
'use strict';

var News = require('../models').News;

function onSave(socket, doc, cb) {
    socket.emit('news:save', doc);
}

function onRemove(socket, doc, cb) {
    socket.emit('news:remove', doc);
}

exports.register = function (socket) {
    News.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    News.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};
