/*jslint node: true */
'use strict';

var express = require('express');

var controller = require('./news.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/:page/list', auth.isAuthenticated(), controller.getPage);
router.get('/:id', auth.isAuthenticated(), controller.show);

router.post('/', auth.isAuthenticated(), controller.create);
router.post('/:id/thumbnail', auth.isAuthenticated(), controller.updateThumbnail);

router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;