/*jslint es5: true, nomen: true, node: true  */
'use strict';

var _ = require('lodash');
var SteamTotp = require('steam-totp');

var Bot = require('../models').Bot;
var common = require('../common');

// Gets a list of Bots
exports.index = function (req, res) {
    Bot.find().populate('stock.item')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Gets a single Bot from the DB
exports.show = function (req, res) {
    Bot.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Creates a new Bot in the DB
exports.create = function (req, res) {
    Bot.create(req.body)
        .then(common.responseWithResult(res, 201))
        .then(null, common.handleError(res));
};

// Updates an existing Bot in the DB
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Bot.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a Bot from the DB
exports.destroy = function (req, res) {
    Bot.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};

exports.totp = function (req, res, next) {
    Bot.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(function (bot) {
            return {
                code: SteamTotp.generateAuthCode(bot.twoFactor.sharedSecret),
                timeLeft: 30 - new Date().getSeconds() % 30
            };
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};