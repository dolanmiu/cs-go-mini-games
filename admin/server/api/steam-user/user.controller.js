/*jslint es5: true, nomen: true, node: true  */
/*globals Promise */
'use strict';

var _ = require('lodash');
var Q = require('q');

var User = require('../models').User;
var common = require('../common');

// Search
function createQuery(searchTerm) {
    var query = {};

    if (searchTerm) {
        var tokens = searchTerm.split(/[ ,]+/),
            regexes = [];
        
        tokens.forEach(function (token) {
            regexes.push(new RegExp(token, "i"));
        });

        query.name = {
            '$in': regexes
        };
    }
    
    return query;
}

// Gets a list of Users
exports.index = function (req, res) {
    var pageNumber = req.query.page || 1,
        limit = req.query.limit || 5,
        query = createQuery(req.query.search);
    
    User.paginate(query, { select: 'avatar.small name steamId', page: pageNumber, limit: limit })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Gets a single User from the DB
exports.show = function (req, res) {
    User.findById(req.params.id).populate('inventory.item')
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Updates an existing User in the DB
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    User.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Adds items to User
exports.addItems = function (req, res) {
    User.findById(req.params.id).populate('inventory.item')
        .then(common.handleEntityNotFound(res))
        .then(function (user) {
            req.body.itemIds.forEach(function (itemId) {
                user.addItemToInventory(itemId);
            });
            return user;
        })
        .then(common.saveEntity(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.deleteItem = function (req, res) {
    User.findById(req.params.id).populate('inventory.item')
        .then(common.handleEntityNotFound(res))
        .then(function (user) {
            user.deleteItem(req.body.itemId);
            return user;
        })
        .then(common.saveEntity(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a user from the DB.
exports.destroy = function (req, res) {
    User.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};

exports.findDuplicates = function (req, res) {
    User.find().then(function (users) {
        var promises = [];

        users.forEach(function (user) {
            var promise = new Promise(function (resolve, reject) {
                User.find({
                    steamId: user.steamId
                }).then(function (users) {
                    if (users.length > 1) {
                        return resolve(user);
                    }
                    resolve();
                });
            });

            promises.push(promise);
        });

        Promise.all(promises)
            .then(function (users) {
                return _.compact(users);
            })
            .then(common.responseWithResult(res))
            .then(null, common.handleError(res));
    });
};