/*jslint nomen: true, node: true */
'use strict';

var User = require('../models').User;

function onSave(socket, doc, cb) {
    console.log('saving user');
    User.populate(doc, {
        path: 'inventory.item'
    }, function (err, user) {
        socket.emit('steamUser:save', user);
    });
}

function onRemove(socket, doc, cb) {
    console.log('removing user');
    socket.emit('steamUser:remove', doc);
}

exports.register = function (socket) {
    User.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    User.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};