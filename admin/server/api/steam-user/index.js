/*jslint node: true */
'use strict';

import express from 'express';
import controller from './user.controller';
import auth from '../../auth/auth.service';

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/duplicates', auth.isAuthenticated(), controller.findDuplicates);
router.get('/:id', auth.isAuthenticated(), controller.show);

router.put('/:id', auth.isAuthenticated(), controller.update);
router.patch('/:id', auth.isAuthenticated(), controller.update);

router.put('/:id/addItems', auth.isAuthenticated(), controller.addItems);
router.put('/:id/deleteItem', auth.isAuthenticated(), controller.deleteItem);

router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;