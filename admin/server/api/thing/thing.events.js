/*jslint nomen: true, node:true */
'use strict';

var EventEmitter = require('events').EventEmitter;
var Thing = require('./thing.model');
var ThingEvents = new EventEmitter();

// Set max event listeners (0 == unlimited)
ThingEvents.setMaxListeners(0);

// Model events
var events = {
    'save': 'save',
    'remove': 'remove'
};

function emitEvent(event) {
    return function (doc) {
        ThingEvents.emit(event + ':' + doc._id, doc);
        ThingEvents.emit(event, doc);
    };
}

// Register the event emitter to the model events
events.forEach(function (e) {
    var event = events[e];
    Thing.schema.post(e, emitEvent(event));
});

module.exports = ThingEvents;