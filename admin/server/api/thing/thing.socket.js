/**
 * Broadcast updates to client when the model changes
 */
/*jslint node: true */
'use strict';

var ThingEvents = require('./thing.events');
var events = ['save', 'remove']; // Model events to emit

function createListener(event, socket) {
    return function (doc) {
        socket.emit(event, doc);
    };
}

function removeListener(event, listener) {
    return function () {
        ThingEvents.removeListener(event, listener);
    };
}

exports.register = function (socket) {
    // Bind model events to socket events
    events.forEach(function (event) {
        var listener = createListener('thing:' + event, socket);

        ThingEvents.on(event, listener);
        socket.on('disconnect', removeListener(event, listener));
    });
};