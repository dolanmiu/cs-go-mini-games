/*jslint node: true */
'use strict';

var Announcement = require('../models').Announcement;

function onSave(socket, doc, cb) {
    socket.emit('announcement:save', doc);
}

function onRemove(socket, doc, cb) {
    socket.emit('announcement:remove', doc);
}

exports.register = function (socket) {
    Announcement.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    Announcement.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};
