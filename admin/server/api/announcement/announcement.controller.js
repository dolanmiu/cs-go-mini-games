/*jslint es5: true, nomen: true, node: true  */
'use strict';

var _ = require('lodash');

var Announcement = require('../models').Announcement;
var common = require('../common');

// Gets a list of Announcements
exports.index = function (req, res) {
    Announcement.find()
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Gets a single Announcement from the DB
exports.show = function (req, res) {
    Announcement.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Creates a new Announcement in the DB
exports.create = function (req, res) {
    Announcement.create(req.body)
        .then(common.responseWithResult(res, 201))
        .then(null, common.handleError(res));
};

// Updates an existing Announcement in the DB
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    Announcement.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a Announcement from the DB
exports.destroy = function (req, res) {
    Announcement.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};