/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');
var TradeOffer = require('../models').TradeOffer;
var common = require('../common');
var Promise = require('bluebird');

// Get list of tradeOffers
exports.index = function (req, res) {
    TradeOffer.find().populate('user', 'name')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get deposits
exports.deposits = function (req, res) {
    TradeOffer.paginate({
            type: 'deposit'
        }, {
            page: req.query.page,
            limit: req.query.limit,
            populate: 'user',
            sort: '-date'
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get withdraws
exports.withdraws = function (req, res) {
    TradeOffer.paginate({
            type: 'withdraw'
        }, {
            page: req.query.page,
            limit: req.query.limit,
            populate: 'user',
            sort: '-date'
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single tradeOffer
exports.show = function (req, res) {
    TradeOffer.findById(req.params.id).populate('user', 'name').populate('items')
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a tradeOffer from the DB.
exports.destroy = function (req, res) {
    TradeOffer.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};