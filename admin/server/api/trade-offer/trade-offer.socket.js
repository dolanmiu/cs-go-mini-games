/*jslint node: true */
'use strict';

var TradeOffer = require('../models').TradeOffer;

function onSave(socket, doc, cb) {
    socket.emit('tradeOffer:save', doc);
}

function onRemove(socket, doc, cb) {
    socket.emit('tradeOffer:remove', doc);
}

exports.register = function (socket) {
    TradeOffer.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    TradeOffer.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};
