/*jslint node: true */
'use strict';

var express = require('express');

var controller = require('./trade-offer.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/deposits', auth.isAuthenticated(), controller.deposits);
router.get('/withdraws', auth.isAuthenticated(), controller.withdraws);

router.get('/:id', auth.isAuthenticated(), controller.show);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;