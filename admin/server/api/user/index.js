'use strict';

import express from 'express';
import controller from './user.controller';
import auth from '../../auth/auth.service';

var router = express.Router();

router.get('/me', auth.isAuthenticated(), controller.me);

module.exports = router;
