/*jslint node: true */
'use strict';

import User from './user.model';
import passport from 'passport';
import config from '../../config/environment';
import jwt from 'jsonwebtoken';

function validationError(res, statusCode) {
    statusCode = statusCode || 422;
    return function (err) {
        res.status(statusCode).json(err);
    }
}

function handleError(res, statusCode) {
    statusCode = statusCode || 500;
    return function (err) {
        res.status(statusCode).send(err);
    };
}

function respondWith(res, statusCode) {
    statusCode = statusCode || 200;
    return function () {
        res.status(statusCode).end();
    };
}

/**
 * Get my info
 */
exports.me = function (req, res, next) {
    var userId = req.user._id;

    User.findOne({
        _id: userId
    }, '-salt -hashedPassword').then(function (user) { // don't ever give out the password or salt
        if (!user) {
            return res.status(401).end();
        }
        res.json(user);
    }, function (err) {
        return next(err);
    });
};

/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
    res.redirect('/');
};