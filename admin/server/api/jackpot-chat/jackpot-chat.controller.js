/*jslint nomen: true, node: true */
'use strict';

var JackpotChat = require('../models').JackpotChat;
var common = require('../common');

// Get list of jackpotChats
exports.index = function (req, res) {
    JackpotChat.find().populate('user').sort('date')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single jackpotChat
exports.show = function (req, res) {
    JackpotChat.findById(req.params.id).populate('user')
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};


// Creates a new jackpotChat in the DB.
exports.create = function (req, res) {
    JackpotChat.create(req.body)
        .then(common.responseWithResult(res, 201))
        .then(null, common.handleError(res));
};

// Updates an existing jackpotChat in the DB.
exports.update = function (req, res) {
    if (req.body._id) {
        delete req.body._id;
    }
    console.log(req.body);
    JackpotChat.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.saveUpdates(req.body))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a jackpotChat from the DB.
exports.destroy = function (req, res) {
    JackpotChat.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};