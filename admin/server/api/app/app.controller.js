/*jslint es5: true, nomen: true, node: true  */
'use strict';

var request = require('request');

exports.users = function (req, res) {
    request(process.env.CLIENT_URL + '/api/app/users', function (error, response, body) {}).pipe(res);
};