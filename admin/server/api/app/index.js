/*jslint node: true */
'use strict';

var express = require('express');

var controller = require('./app.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/users', auth.isAuthenticated(), controller.users);

module.exports = router;