/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

var Referral = require('../models').Referral;
var common = require('../common');

// Get list of referrals
exports.index = function (req, res) {
    var pageNumber = req.query.page || 1,
        limit = req.query.limit || 30;
    
    Referral.paginate({}, { page: pageNumber, limit: limit, sort: '-date', populate: 'user referredUser' })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single referral
exports.show = function (req, res) {
    Referral.findById(req.params.id).populate('pot.entries.item').populate('pot.entries.user').populate('cut')
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a referral from the DB.
exports.destroy = function (req, res) {
    Referral.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};