/*jslint es5: true, nomen: true, node: true */
'use strict';

var request = require('request');
var config = require('../../config/environment');

exports.index = function (req, res) {
    request(config.botServerUrl + '/api/app', function (err, response, body) {}).pipe(res);
};