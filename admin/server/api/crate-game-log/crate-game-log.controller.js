/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

var CrateGameLog = require('../models').CrateGameLog;
var common = require('../common');

// Get list of crateGameLogs
exports.index = function (req, res) {
    CrateGameLog.paginate({}, { page: req.query.page, limit: req.query.limit, sort: '-date', populate: {
        path: 'prize.item user crate'
    }})
        .then(function (result) {
            return result.docs;
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single crateGameLog
exports.show = function (req, res) {
    CrateGameLog.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a crateGameLog from the DB.
exports.destroy = function (req, res) {
    CrateGameLog.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};

// Gets total count
exports.count = function (req, res) {
    CrateGameLog.count()
        .then(function (result) {
            return {
                count: result
            };
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};