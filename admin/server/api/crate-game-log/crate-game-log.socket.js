/*jslint node: true */
'use strict';

var CrateGameLog = require('../models').CrateGameLog;

function onSave(socket, doc, cb) {
    CrateGameLog.populate(doc, {
        path: 'prize winner.user'
    }, function (err, crate) {
        socket.emit('crateGameLog:save', doc);
    });
}

function onRemove(socket, doc, cb) {
    socket.emit('crateGameLog:remove', doc);
}

exports.register = function (socket) {
    CrateGameLog.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    CrateGameLog.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};