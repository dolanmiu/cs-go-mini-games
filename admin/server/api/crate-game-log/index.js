/*jslint node: true, es5: true */
'use strict';

var express = require('express');

var controller = require('./crate-game-log.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.get('/count', auth.isAuthenticated(), controller.count);

router.get('/:id', auth.isAuthenticated(), controller.show);
router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;