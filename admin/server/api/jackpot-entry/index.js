/*jslint node: true */
'use strict';

var express = require('express');

var controller = require('./jackpot-entry.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
router.delete('/:id', auth.isAuthenticated(), controller.refund);

module.exports = router;