/*jslint es5: true, nomen: true, node: true  */
'use strict';

var _ = require('lodash');

var JackpotEntry = require('../models').JackpotEntry;
var User = require('../models').User;
var common = require('../common');

// Gets a list of JackpotEntrys
exports.index = function (req, res) {
    JackpotEntry.find().populate('user inventoryItem.item')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Refunds a JackpotEntry
exports.refund = function (req, res) {
    JackpotEntry.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(function (jackpotEntry) {
            User.findById(jackpotEntry.user)
                .then(function (user) {
                    console.log(user);
                    user.inventory.push({
                        item: jackpotEntry.inventoryItem.item,
                        assetId: jackpotEntry.inventoryItem.assetId
                    });
                    user.save().then(function (updated) {
                        jackpotEntry.remove().then(function () {
                            res.status(204).end();
                        });
                    });
                });
        })
        .then(null, common.handleError(res));
};
/*exports.refund = function (req, res) {
    var itemId;

    JackpotEntry.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(function (jackpotEntry) {
            itemId = jackpotEntry.item;
            return User.findById(jackpotEntry.user);
        })
        .then(function (user) {
            user.addItem(itemId);
            user.save().then(function (updated) {
                return updated;
            });
        })
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};*/