/*jslint node: true */
'use strict';

var JackpotEntry = require('../models').JackpotEntry;

function onSave(socket, doc, cb) {
    socket.emit('jackpotEntry:save', doc);
}

function onRemove(socket, doc, cb) {
    socket.emit('jackpotEntry:remove', doc);
}

exports.register = function (socket) {
    JackpotEntry.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    JackpotEntry.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};
