/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

var LoginLog = require('../models').LoginLog;
var common = require('../common');

// Get list of loginLogs
exports.index = function (req, res) {
    LoginLog.paginate({}, { page: req.query.page, limit: req.query.limit, sort: '-date', populate: { path: 'user' } })
        .then(function (result) {
            return result.docs;
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single jackpotLog
exports.show = function (req, res) {
    LoginLog.findById(req.params.id).populate('pot.entries.item').populate('pot.entries.user')
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Deletes a jackpotLog from the DB.
exports.destroy = function (req, res) {
    LoginLog.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};

// Gets total count
exports.count = function (req, res) {
    LoginLog.count()
        .then(function (result) {
            return {
                count: result
            };
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};