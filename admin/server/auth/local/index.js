/*jslint nomen: true, node: true */
'use strict';

var express = require('express');
var passport = require('passport');

var auth = require('../auth.service');
var config = require('../../config/environment');
var User = require('../../api/user/user.model');

var router = express.Router();

function bypassLogin(req, res) {
    User.find(function (err, users) {
        var user = users[0];
        req.logIn(user, function (err) {
            var token = auth.signToken(user._id, user.role);

            res.json({
                token: token
            });
        });
    });
}

router.post('/', function (req, res, next) {
    passport.authenticate('local', function (err, user, info) {
        if (config.alwaysAdmin) {
            bypassLogin(req, res);
        } else {
            var error = err || info;
            if (error) {
                return res.status(401).json(error);
            }
            if (!user) {
                return res.status(404).json({
                    message: 'Something went wrong, please try again.'
                });

            }
            req.logIn(user, function (err) {
                var token = auth.signToken(user._id, user.role);

                res.json({
                    token: token
                });
            });
        }
    })(req, res, next);
});

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (obj, done) {
    done(null, obj);
});

module.exports = router;