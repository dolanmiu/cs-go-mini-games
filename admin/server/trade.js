/*jslint node: true */
/*globals Promise */
'use strict';

var request = require('request');

function handleResponse(resolve, reject) {
    return function (err, response, body) {
        if (err) {
            reject(err);
        }
        resolve(response);
    };
}

exports.enable2Fa = function (botId) {
    return new Promise(function (resolve, reject) {
        request.post({
            url: process.env.BOT_SERVER_URL + '/api/twoFactor/' + botId + '/enable',
            json: true,
            body: {}
        }, handleResponse(resolve, reject));
    });
};

exports.confirm2Fa = function (botId) {
    return new Promise(function (resolve, reject) {
        request.post({
            url: process.env.BOT_SERVER_URL + '/api/twoFactor/' + botId + '/confirm',
            json: true,
            body: {}
        }, handleResponse(resolve, reject));
    });
};

exports.syncInventorytoBots = function () {
    return new Promise(function (resolve, reject) {
        request.post({
            url: process.env.BOT_SERVER_URL + '/api/items/syncToBots',
            json: true,
            body: {}
        }, handleResponse(resolve, reject));
    });
};

exports.findStaleItems = function () {
    return new Promise(function (resolve, reject) {
        request({
            url: process.env.BOT_SERVER_URL + '/api/items/stale',
            json: true
        }, handleResponse(resolve, reject));
    });
};

exports.findUnlinkedItems = function () {
    return new Promise(function (resolve, reject) {
        request({
            url: process.env.BOT_SERVER_URL + '/api/items/unlinked',
            json: true
        }, handleResponse(resolve, reject));
    });
};

exports.findDesynced = function () {
    return new Promise(function (resolve, reject) {
        request({
            url: process.env.BOT_SERVER_URL + '/api/items/desynced',
            json: true
        }, handleResponse(resolve, reject));
    });
};

exports.transferBetweenBots = function (sourceBotId, destinationBotId, itemIds) {
    return new Promise(function (resolve, reject) {
        request.post({
            url: process.env.BOT_SERVER_URL + '/api/transfer/betweenBots',
            json: true,
            body: {
                sourceBot: sourceBotId,
                destinationBot: destinationBotId,
                itemIds: itemIds
            }
        }, handleResponse(resolve, reject));
    });
};