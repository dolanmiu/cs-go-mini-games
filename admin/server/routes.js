/**
 * Main application routes
 */
/*jslint node: true */
'use strict';

import errors from './components/errors';
import path from 'path';

module.exports = function (app) {

    // Insert routes below
    app.use('/api/news', require('./api/news'));
    app.use('/api/activeBots', require('./api/active-bot'));
    app.use('/api/items', require('./api/item'));
    app.use('/api/bots', require('./api/bot'));
    app.use('/api/crates', require('./api/crate'));
    app.use('/api/users', require('./api/user'));
    app.use('/api/steamUsers', require('./api/steam-user'));
    app.use('/api/crateGameLogs', require('./api/crate-game-log'));
    app.use('/api/jackpotLogs', require('./api/jackpot-log'));
    app.use('/api/loginLogs', require('./api/login-log'));
    app.use('/api/jackpotEntries', require('./api/jackpot-entry'));
    app.use('/api/tradeOffers', require('./api/trade-offer'));
    app.use('/api/announcements', require('./api/announcement'));
    app.use('/api/inventoryItems', require('./api/inventory-item'));
    app.use('/api/jackpotChats', require('./api/jackpot-chat'));
    app.use('/api/crateGameChats', require('./api/crate-game-chat'));
    app.use('/api/referral', require('./api/referral'));

    app.use('/auth', require('./auth'));

    // All undefined asset or api routes should return a 404
    app.route('/:url(api|auth|components|app|bower_components|assets)/*')
        .get(errors[404]);

    // All other routes should redirect to the index.html
    app.route('/*')
        .get(function (req, res) {
            res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
        });
};