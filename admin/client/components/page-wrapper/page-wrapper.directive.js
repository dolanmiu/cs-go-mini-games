/*globals angular */
angular.module('adminApp').directive('pageWrapper', function () {
    'use strict';

    return {
        templateUrl: 'components/page-wrapper/page-wrapper.html',
        restrict: 'E',
        scope: {
            title: '@'
        },
        //controller: 'PageWrapperController',
        transclude: true
    };
});