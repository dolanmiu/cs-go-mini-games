/*globals angular */
angular.module('adminApp').factory('User', function ($resource) {
    'use strict';

    return $resource('/api/users/:id/:controller', {
        id: '@_id'
    }, {
        get: {
            method: 'GET',
            params: {
                id: 'me'
            }
        }
    });
});