/*jslint  nomen: true */
/*globals _, angular */
angular.module('adminApp').controller('NavbarCtrl', function ($scope, $state, Auth) {
    'use strict';

    $scope.menu = [{
        title: 'Dashboard',
        link: 'main',
        fa: 'dashboard'
    }, {
        title: 'Item',
        fa: 'archive',
        subMenu: [{
            title: 'Overview',
            link: 'item.overview'
        }, {
            title: 'Blacklist',
            link: 'item.blacklist'
        }, {
            title: 'Custom Items',
            link: 'item.custom'
        }, {
            title: 'Stock / Inventory',
            link: 'item.stock'
        }]
    }, {
        title: 'Bots',
        link: 'bots',
        fa: 'bug'
    }, {
        title: 'Crates',
        link: 'crate',
        fa: 'cubes'
    }, {
        title: 'Trade History',
        fa: 'bar-chart',
        link: 'trade'
    }, {
        title: 'Logs',
        fa: 'file-text',
        subMenu: [{
            title: 'Crate',
            link: 'logs.crate'
        }, {
            title: 'Jackpot',
            link: 'logs.jackpot'
        }, {
            title: 'Login',
            link: 'logs.login'
        }]
    }, {
        title: 'News',
        link: 'news',
        fa: 'newspaper-o'
    }, {
        title: 'User',
        link: 'user',
        fa: 'users'
    }, {
        title: 'Giveaway',
        link: 'giveaway',
        fa: 'gift'
    }, {
        title: 'Jackpot',
        link: 'jackpot',
        fa: 'tint'
    }, {
        title: 'Announcements',
        link: 'announcements',
        fa: 'bullhorn'
    }, {
        title: 'Chats',
        link: 'chats',
        fa: 'comments'
    }, {
        title: 'Referral',
        link: 'referral',
        fa: 'heart'
    }, {
        title: 'Email',
        link: 'email',
        fa: 'envelope'
    }];

    $scope.topMenu = [{
        fa: 'envelope'
    }, {
        fa: 'tasks'
    }, {
        fa: 'bell'
    }, {
        fa: 'user',
        subMenu: [{
            title: 'Log out',
            fa: 'sign-out',
            link: 'logout'
        }]
    }];

    $scope.search = function (searchQuery) {
        if (!searchQuery) {
            $scope.searchResults = undefined;
            return;
        }
        $scope.searchResults = _.filter($scope.menu, function (i) {
            return _.includes(i.title.toLowerCase(), searchQuery.toLowerCase());
        });
    };

    $scope.directToPage = function () {
        var state = $scope.searchResults[0];
        $state.go(state.link);
    };


    $scope.isCollapsed = true;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;
});