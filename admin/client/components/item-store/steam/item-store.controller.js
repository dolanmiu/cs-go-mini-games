/*jslint nomen: true */
/*globals angular */
angular.module('adminApp').controller('ItemStoreController', function ($scope, ActiveBots, Crate, $modalInstance) {
    'use strict';

    $scope.bots = ActiveBots.query();
    $scope.crates = Crate.query();

    $scope.getInventory = function (index) {
        if ($scope.bots[index].inventory.length > 0) {
            $scope.currentBot = $scope.bots[index];
            return;
        }

        $scope.loading = true;

        /*ActiveBots.getInventory({
            id: $scope.bots[index]._id
        }).$promise.then(function (inventory) {
            $scope.bots[index].inventory = inventory;
            $scope.currentBot = $scope.bots[index];
            $scope.loading = false;
        });*/
    };

    $scope.selectItem = function (bot, itemIndex) {
        var item = bot.inventory[itemIndex];
        if (item.selected) {
            item.selected = false;
        } else {
            item.selected = true;
        }
    };

    $scope.ok = function () {
        var selectedItems = [];

        $scope.bots.forEach(function (bot) {
            if (angular.isUndefined(bot.inventory)) {
                return;
            }
            bot.inventory.forEach(function (item) {
                if (item.selected) {
                    selectedItems.push(item);
                }
            });
        });
        $modalInstance.close(selectedItems);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});