/*globals angular */
angular.module('adminApp').controller('DatabaseItemModalController', function ($scope, Item, $modalInstance) {
    'use strict';

    $scope.currentPage = 1;
    $scope.itemTotal = Item.count();
    $scope.maxSize = 16;
    $scope.itemsPerPage = 15;
    $scope.selectedItems = [];

    $scope.$watch('currentPage', function (newValue) {
        Item.query({
            id: newValue - 1,
            quantity: 15
        }).$promise.then(function (items) {
            $scope.items = items;
        });
    });

    $scope.$watch('searchCriteria', function (newValue) {
        if (newValue === '') {
            $scope.searchResults = undefined;
        }
    });

    $scope.searchItems = function () {
        Item.search({
            id: $scope.searchCriteria
        }).$promise.then(function (results) {
            $scope.searchResults = results;
        });
    };

    $scope.selectItem = function (item) {
        $scope.selectedItems.push(item);
    };
    
    $scope.removeItem = function (itemIndex) {
        $scope.selectedItems.splice(itemIndex, 1);
    };

    $scope.ok = function () {
        $modalInstance.close($scope.selectedItems);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});