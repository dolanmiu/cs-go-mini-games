/*jslint nomen: true */
/*globals angular, _ */
angular.module('adminApp').controller('BotStoreModalController', function ($scope, Bot, $modalInstance) {
    'use strict';

    $scope.currentPage = 1;
    $scope.maxSize = 16;
    $scope.itemsPerPage = 15;
    $scope.selectedItems = [];

    $scope.$watch('currentPage', function (newValue) {
        Bot.query({}).$promise.then(function (bots) {
            $scope.items = _.flatten(_.pluck(bots, 'stock'));
        });
    });

    $scope.$watch('searchCriteria', function (newValue) {
        if (newValue === '') {
            $scope.searchResults = undefined;
        }
    });

    $scope.selectItem = function (item) {
        $scope.selectedItems.push(item);
    };

    $scope.removeItem = function (itemIndex) {
        $scope.selectedItems.splice(itemIndex, 1);
    };

    $scope.ok = function () {
        $modalInstance.close($scope.selectedItems);
    };

    $scope.cancel = function () {
        $modalInstance.dismiss('cancel');
    };

});