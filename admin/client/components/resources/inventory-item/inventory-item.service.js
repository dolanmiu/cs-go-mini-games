/*globals angular */
angular.module('adminApp').factory('InventoryItem', function ($resource) {
    'use strict';

    return $resource('/api/inventoryItems/:id/:controller', {
        id: '@_id'
    }, {
        query: {
            isArray: false
        },
        update: {
            method: 'PUT'
        },
        count: {
            method: 'GET',
            params: {
                controller: 'count'
            }
        },
        getInJackpot: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'jackpot'
            }
        },
        sync: {
            method: 'PUT',
            params: {
                controller: 'sync'
            }
        },
        findDuplicates: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'duplicates'
            }
        },
        findStaleItems: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'staleItems'
            }
        },
        findUnlinkedItems: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'unlinkedItems'
            }
        },
        findDesyncedItems: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'desyncedItems'
            }
        },
        createMetaData: {
            method: 'POST',
            isArray: true,
            params: {
                controller: 'metadata'
            }
        },
        refundJackpotEntry: {
            method: 'PUT',
            params: {
                controller: 'refundJackpotEntry'
            }
        }
    });
});