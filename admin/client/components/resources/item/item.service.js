/*globals angular */
angular.module('adminApp').factory('Item', function ($resource) {
    'use strict';

    return $resource('/api/items/:id/:controller/:quantity/:query', {
        id: '@_id'
    }, {
        query: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'list'
            }
        },
        update: {
            method: 'PUT'
        },
        count: {
            method: 'GET',
            params: {
                controller: 'count'
            }
        },
        search: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'search'
            }
        },
        getBlacklist: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'blacklist'
            }
        },
        getCustom: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'custom'
            }
        },
        addItem: {
            method: 'POST'
        },
        deleteItem: {
            method: 'DELETE'
        },
        setCustomPrice: {
            method: 'PUT',
            params: {
                controller: 'price'
            }
        }
    });
});