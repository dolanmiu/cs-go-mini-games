/*globals angular */
angular.module('adminApp').factory('Bot', function ($resource) {
    'use strict';

    return $resource('/api/bots/:id/:controller', {
        id: '@_id'
    }, {
        get: {
            method: 'GET'
        },
        create: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        totp: {
            method: 'GET',
            params: {
                controller: 'totp'
            }
        },
        setDepositBot: {
            method: 'PUT',
            params: {
                controller: 'depositBot'
            }
        },
        updateTradeUrl: {
            method: 'PUT',
            params: {
                controller: 'tradeUrl'
            }
        }
    });
});