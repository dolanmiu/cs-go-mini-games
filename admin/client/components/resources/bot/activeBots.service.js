/*globals angular */
angular.module('adminApp').factory('ActiveBots', function ($resource) {
    'use strict';

    return $resource('/api/activeBots/:id/:controller', {
        id: '@_id'
    }, {
        query: {
            method: 'GET',
            isArray: true
        },
        test: {
            method: 'POST',
            params: {
                controller: 'test'
            }
        },
        enable2Fa: {
            method: 'PUT',
            params: {
                controller: 'enableTwoFactor'
            }
        },
        confirm2Fa: {
            method: 'PUT',
            params: {
                controller: 'confirmTwoFactor'
            }
        }
    });
});