/*globals angular */
angular.module('adminApp').factory('Referral', function ($resource) {
    'use strict';

    return $resource('/api/referral/:id/:controller', {
        id: '@_id'
    }, {
        query: {
            method: 'GET'
        }
    });
});