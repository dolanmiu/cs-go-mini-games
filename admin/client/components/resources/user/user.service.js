/*globals angular */
angular.module('adminApp').factory('SteamUser', function ($resource) {
    'use strict';

    return $resource('/api/steamUsers/:id/:controller', {
        id: '@_id'
    }, {
        query: {
            isArray: false
        },
        changeEmail: {
            method: 'PUT',
            params: {
                controller: 'email'
            }
        },
        changeTradeUrl: {
            method: 'PUT',
            params: {
                controller: 'tradeUrl'
            }
        },
        getInventory: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'inventory'
            }
        },
        update: {
            method: 'PUT'
        },
        addItems: {
            method: 'PUT',
            params: {
                controller: 'addItems'
            }
        },
        deleteItem: {
            method: 'PUT',
            params: {
                controller: 'deleteItem'
            }
        },
        findDuplicates: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'duplicates'
            }
        }
    });
});