/*globals angular */
angular.module('adminApp').factory('Announcement', function ($resource) {
    'use strict';

    return $resource('/api/announcements/:id/:controller', {
        id: '@_id'
    }, {
        update: {
            method: 'PUT'
        }
    });
});