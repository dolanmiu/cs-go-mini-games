/*globals angular */
angular.module('adminApp').factory('UserNotification', function ($resource) {
    'use strict';

    return $resource('/api/notifications/:id/:controller', {
        id: '@_id'
    }, {
        get: {
            method: 'GET',
            isArray: true,
            params: {
                id: 'me'
            }
        },
        getUnread: {
            method: 'GET',
            isArray: true,
            params: {
                id: 'me',
                controller: 'unread'
            }
        }
    });
});