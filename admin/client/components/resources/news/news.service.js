/*globals angular */
angular.module('adminApp').factory('News', function ($resource) {
    'use strict';

    return $resource('/api/news/:id/:page/:controller', {
        id: '@_id'
    }, {
        query: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'list'
            }
        },
        create: {
            method: 'POST'
        },
        update: {
            method: 'PUT'
        },
        count: {
            method: 'GET',
            params: {
                controller: 'count'
            }
        },
        search: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'search'
            }
        }
    });
});