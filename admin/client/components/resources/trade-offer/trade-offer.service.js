/*globals angular */
angular.module('adminApp').factory('TradeOffer', function ($resource) {
    'use strict';

    return $resource('/api/tradeoffers/:id/:controller', {
        id: '@_id'
    }, {
        get: {
            method: 'GET'
        },
        getWithdraws: {
            method: 'GET',
            isArray: false,
            params: {
                controller: 'withdraws'
            }
        },
        getDeposits: {
            method: 'GET',
            isArray: false,
            params: {
                controller: 'deposits'
            }
        }
    });
});