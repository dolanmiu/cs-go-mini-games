/*globals angular */
angular.module('adminApp').factory('Crate', function ($resource) {
    'use strict';

    return $resource('/api/crates/:id/:controller', {
        id: '@_id'
    }, {
        update: {
            method: 'PUT'
        },
        test: {
            method: 'GET',
            params: {
                controller: 'test'
            }
        }
    });
});