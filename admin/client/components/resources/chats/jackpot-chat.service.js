/*globals angular */
angular.module('adminApp').factory('JackpotChat', function ($resource) {
    'use strict';

    return $resource('/api/jackpotChats/:id/:controller', {}, {
    });
});