/*globals angular */
angular.module('adminApp').factory('CrateGameChat', function ($resource) {
    'use strict';

    return $resource('/api/crateGameChats/:id/:controller', {}, {
    });
});