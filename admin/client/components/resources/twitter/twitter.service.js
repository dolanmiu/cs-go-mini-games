/*globals angular */
angular.module('adminApp').factory('Twitter', function ($resource) {
    'use strict';

    return $resource('/api/twitter/:id/:controller/:count', {
        id: '@_id'
    }, {
        get: {
            method: 'GET'
        },
        query: {
            method: 'GET',
            isArray: true
        }
    });
});