/*globals angular */
angular.module('adminApp').factory('JackpotEntry', function ($resource) {
    'use strict';

    return $resource('/api/jackpotEntries/:id/:controller', {
        id: '@_id'
    }, {
        refund: {
            method: 'DELETE'
        }
    });
});