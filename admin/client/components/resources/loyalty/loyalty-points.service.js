/*globals angular */
angular.module('adminApp').factory('LoyaltyPoints', function ($resource) {
    'use strict';

    return $resource('/api/loyaltyPoints/:id/:controller', {
        id: '@_id'
    }, {
        getNextGiveaway: {
            method: 'GET',
            params: {
                controller: 'nextGiveaway'   
            }
        }
    });
});