/*globals angular */
angular.module('adminApp').factory('CrateGameLog', function ($resource) {
    'use strict';

    return $resource('/api/crateGameLogs/:id/:controller', {
        id: '@_id'
    }, {
        count: {
            method: 'GET',
            params: {
                controller: 'count'
            }
        }
    });
});