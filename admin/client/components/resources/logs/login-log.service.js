/*globals angular */
angular.module('adminApp').factory('LoginLog', function ($resource) {
    'use strict';

    return $resource('/api/loginLogs/:id/:controller', {
        id: '@_id'
    }, {
        count: {
            method: 'GET',
            params: {
                controller: 'count'
            }
        }
    });
});