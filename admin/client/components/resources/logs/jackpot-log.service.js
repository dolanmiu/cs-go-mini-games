/*globals angular */
angular.module('adminApp').factory('JackpotLog', function ($resource) {
    'use strict';

    return $resource('/api/jackpotLogs/:id/:controller', {
        id: '@_id'
    }, {
        query: {
            isArray: false
        }
    });
});