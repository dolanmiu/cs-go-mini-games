/*globals angular */
angular.module('adminApp').factory('Contact', function ($resource) {
    'use strict';

    return $resource('/api/contact', {}, {
        contact: {
            method: 'POST'
        }
    });
});