/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('giveaway', {
        url: '/giveaway',
        templateUrl: 'app/giveaway/giveaway.html',
        controller: 'AdminGiveawayController',
        authenticateAdmin: true
    });
});