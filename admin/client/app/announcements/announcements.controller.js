/*jslint nomen: true */
/*globals angular */
angular.module('adminApp').controller('AnnouncementController', function ($scope, socket, Announcement) {
    'use strict';

    $scope.announcements = Announcement.query();
    socket.syncUpdates('announcement', $scope.announcements);

    $scope.addAnnouncement = function (message) {
        Announcement.save({
            message: message
        });
    };

    $scope.deleteAnnouncement = function (announcement) {
        Announcement.delete({
            id: announcement._id
        });
    };

    $scope.updateAnnouncement = function (announcement) {
        console.log(announcement._id);
        Announcement.update({
            id: announcement._id
        }, announcement);
    };

    $scope.$on('$destroy', function () {
        socket.unsyncUpdates('announcement');
    });

});