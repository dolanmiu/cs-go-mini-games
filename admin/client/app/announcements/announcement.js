/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('announcements', {
        url: '/announcements',
        templateUrl: 'app/announcements/announcements.html',
        controller: 'AnnouncementController',
        authenticateAdmin: true
    });
});