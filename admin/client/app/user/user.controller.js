/*jslint nomen: true */
/*globals angular, _ */
angular.module('adminApp').controller('UserController', function ($scope, socket, $uibModal, SteamUser, InventoryItem) {
    'use strict';

    $scope.userLimit = 30;

    $scope.openAddItemWindow = function (currentUser) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'components/item-store/database/database.html',
            controller: 'DatabaseItemModalController',
            size: 'lg'
        });

        modalInstance.result.then(function (selectedItems) {
            var itemsIds = _.map(selectedItems, '_id');

            itemsIds.forEach(function (itemId) {
                InventoryItem.save({
                    user: currentUser._id,
                    item: itemId
                });
            });
        });
    };

    $scope.deleteItem = function (item) {
        InventoryItem.delete({
            id: item._id
        });
    };

    $scope.getCurrentUser = function (user) {
        SteamUser.get({
            id: user._id
        }).$promise.then(function (response) {
            $scope.currentUser = response;
            InventoryItem.query({
                user: user._id
            }).$promise.then(function (response) {
                $scope.currentUser.inventory = response.docs;
            });
        });
    };

    $scope.changePage = function (page) {
        SteamUser.query({
            page: page,
            limit: $scope.userLimit
        }).$promise.then(function (response) {
            $scope.users = response;
        });
    };

    $scope.search = function (query) {
        $scope.searchResults = SteamUser.query({
            search: query,
            limit: $scope.userLimit
        });
    };

    $scope.updateUser = function (user) {
        SteamUser.update({
            id: user._id
        }, user);
    };

    $scope.findDuplicates = function () {
        SteamUser.findDuplicates().$promise.then(function (response) {
            $scope.duplicates = response;
        });
    };

    $scope.deleteUser = function (user) {
        SteamUser.delete({
            id: user._id
        }).$promise.then(function () {
            _.remove($scope.duplicates, function (i) {
                return String(i._id) === String(user._id);
            });
        });
    };

    $scope.changePage(1);

});