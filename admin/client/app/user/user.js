/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('user', {
        url: '/user',
        templateUrl: 'app/user/user.html',
        controller: 'UserController',
        authenticateAdmin: true
    });
});