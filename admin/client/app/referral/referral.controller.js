/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('adminApp').controller('ReferralController', function ($scope, Referral) {
    'use strict';

    $scope.limit = 30;
    $scope.currentPage = 1;

    $scope.changePage = function (page) {
        Referral.query({
            page: page
        }).$promise.then(function (response) {
            $scope.referrals = response;
        });
    };

    $scope.deleteReferral = function (referral, page) {
        Referral.delete({
            id: referral._id
        }).$promise.then(function () {
            $scope.changePage(page);
        });
    };

    $scope.changePage(1);
});