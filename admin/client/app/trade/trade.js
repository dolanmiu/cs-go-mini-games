/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('trade', {
            url: '/trade',
            templateUrl: 'app/trade/trade.html',
            controller: 'TradeController'
        });
});