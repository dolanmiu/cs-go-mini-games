/*globals angular */
angular.module('adminApp').controller('TradeMoreInfoController', function ($scope, $modal, socket, $modalInstance, TradeOffer, tradeId) {
    'use strict';
        
    $scope.trade = TradeOffer.get({
        id: tradeId
    });

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss();
    };
});