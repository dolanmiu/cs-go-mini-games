/*globals angular */
/*jslint nomen: true */
angular.module('adminApp').controller('TradeController', function ($scope, $uibModal, TradeOffer) {
    'use strict';

    $scope.tradeOfferLimit = 30;
    $scope.currentWithdrawPage = 1;
    $scope.currentDepositPage = 1;

    function deleteTradeOffer(tradeOffer) {
        return TradeOffer.delete({
            id: tradeOffer._id
        }).$promise;
    }

    $scope.deleteDeposit = function (tradeOffer, page) {
        deleteTradeOffer(tradeOffer).then(function () {
            $scope.changeDeposit(page);
        });
    };

    $scope.deleteWithdraw = function (tradeOffer, page) {
        deleteTradeOffer(tradeOffer).then(function () {
            $scope.changeWithdraw(page);
        });
    };

    $scope.moreInfo = function (trade) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/trade/trade-more-info.html',
            controller: 'TradeMoreInfoController',
            size: 'lg',
            resolve: {
                tradeId: function () {
                    return trade._id;
                }
            }
        });
    };

    $scope.changeDeposit = function (page) {
        TradeOffer.getDeposits({
            page: page,
            limit: $scope.tradeOfferLimit
        }).$promise.then(function (response) {
            $scope.deposits = response;
        });
    };

    $scope.changeWithdraw = function (page) {
        TradeOffer.getWithdraws({
            page: page,
            limit: $scope.tradeOfferLimit
        }).$promise.then(function (response) {
            $scope.withdraws = response;
            
        });
    };

    $scope.changeDeposit(1);
    $scope.changeWithdraw(1);
});