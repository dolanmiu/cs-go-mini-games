/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'app/login/login.html',
            controller: 'AdminLoginController'
        })
        .state('logout', {
            url: '/logout?referrer',
            referrer: 'main',
            template: '',
            controller: function ($state, Auth) {
                var referrer = $state.params.referrer ||
                    $state.current.referrer ||
                    'main';
                Auth.logout();
                $state.go(referrer);
            }
        });
});