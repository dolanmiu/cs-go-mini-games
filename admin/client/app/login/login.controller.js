/*globals angular */
angular.module('adminApp').controller('AdminLoginController', function ($scope, $state, Auth) {
    'use strict';

    $scope.errors = {};

    $scope.login = function (formValid) {
        $scope.submitted = true;

        if (!formValid) {
            return;
        }

        Auth.login({
            username: $scope.username,
            password: $scope.password,
            twoFactor: $scope.twoFactor
        }).then(function () {
            // Logged in, redirect to home
            $state.go('main');
        }).catch(function (err) {
            $scope.errors.other = err.message;
        });
    };
});