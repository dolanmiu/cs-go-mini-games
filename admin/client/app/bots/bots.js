/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('bots', {
        url: '/bots',
        templateUrl: 'app/bots/bots.html',
        controller: 'AdminBotsController',
        authenticateAdmin: true
    });
});