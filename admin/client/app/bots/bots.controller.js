/*jslint nomen: true */
/*globals angular, _ */
angular.module('adminApp').controller('AdminBotsController', function ($scope, $interval, Bot, socket, ActiveBots) {
    'use strict';

    $scope.bots = Bot.query();
    socket.syncUpdates('bot', $scope.bots);

    var botPollPromise = $interval(function () {
            /*ActiveBots.query().$promise.then(function (response) {
                $scope.activeBots = response;
            });*/
        }, 3000),
        totpInverval;

    $scope.$on('$destroy', function () {
        $interval.cancel(botPollPromise);
    });

    $scope.createBot = function (formValid, botUsername, botPassword, tradeUrl) {
        $scope.submitted = true;
        if (!formValid) {
            return;
        }

        Bot.create({
            username: botUsername,
            password: botPassword,
            tradeUrl: tradeUrl
        }).$promise.then(function () {
            $scope.botUsername = undefined;
            $scope.botPassword = undefined;
        });
    };

    $scope.$on('$destroy', function () {
        socket.unsyncUpdates('bot');
    });

    $scope.deleteBot = function (bot) {
        Bot.delete({
            id: bot._id
        });
    };

    $scope.updateBot = function (bot) {
        Bot.update({
            id: bot._id
        }, bot);
    };

    $scope.rebootActiveBots = function () {
        ActiveBots.reboot();
    };

    function handleBotActionSuccess(bot) {
        return function (response) {
            bot.success = true;
            bot.busy = false;
        };
    }

    function handleBotActionError(bot) {
        return function (error) {
            bot.error = error;
            bot.busy = false;
        };
    }

    $scope.test = function (bot) {
        bot.busy = true;
        bot.success = undefined;
        bot.error = undefined;

        ActiveBots.test({
            id: bot._id
        }, {
            authCode: bot.authCode
        }).$promise.then(handleBotActionSuccess(bot), handleBotActionError(bot));
    };

    $scope.enable2Fa = function (bot) {
        bot.busy = true;
        bot.success = undefined;
        bot.error = undefined;

        ActiveBots.enable2Fa({
            id: bot._id
        }, {}).$promise.then(handleBotActionSuccess(bot), handleBotActionError(bot));
    };

    $scope.confirm2Fa = function (bot, code) {
        bot.busy = true;
        bot.success = undefined;
        bot.error = undefined;

        ActiveBots.confirm2Fa({
            id: bot._id
        }, {
            code: code
        }).$promise.then(handleBotActionSuccess(bot), handleBotActionError(bot));
    };

    $scope.getTotp = function (bot) {
        Bot.totp({
            id: bot._id
        }).$promise.then(function (response) {
            bot.totp = response;
            $interval.cancel(totpInverval);
            totpInverval = $interval(function () {
                bot.totp.timeLeft -= 1;
            }, 1000, 30);
        });
    };

    $scope.calculateTotalStock = function (bots) {
        var count = _.sum(bots, function (b) {
            return b.stock.length;
        });
        return count;
    };

});