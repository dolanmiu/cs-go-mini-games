/*globals angular */
angular.module('adminApp').controller('CustomItemsController', function ($scope, Item) {
    'use strict';

    function splitMarketName(marketName) {
        var myRegexp = /([^|]+) \| ([a-zA-Z \-]+) \(([a-zA-Z \-]+)\)/g,
            match = myRegexp.exec(marketName);
        return match;
    }

    function getValueFromSplitMarketName(splitMarketName, index) {
        if (splitMarketName === null) {
            return '';
        } else {
            return splitMarketName[index];
        }
    }

    function itemFromMarketName(marketName, price, iconUrl) {
        var names = splitMarketName(marketName);
        return {
            item: {
                marketName: marketName,
                model: getValueFromSplitMarketName(names, 1),
                set: getValueFromSplitMarketName(names, 2),
                exterior: getValueFromSplitMarketName(names, 3)
            },
            price: {
                custom: price,
                currency: 'USD'
            },
            iconUrl: iconUrl,
            quantity: 0,
            custom: true
        };
    }

    Item.getCustom({}).$promise.then(function (items) {
        $scope.items = items;
    });

    $scope.addItem = function (form, marketName, price, iconUrl) {
        $scope.submitted = true;
        if (form.$invalid) {
            return;
        }

        var item = itemFromMarketName(marketName, price, iconUrl);

        Item.addItem(item).$promise.then(function (item) {
            $scope.items.push(item);
        });
    };

    $scope.deleteItem = function (itemId, index) {
        Item.deleteItem({
            id: itemId
        }).$promise.then(function () {
            $scope.items.splice(index, 1);
        });
    };
});