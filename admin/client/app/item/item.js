/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('item', {
            abstract: true,
            url: '/item',
            template: '<ui-view />'
        })
        .state('item.overview', {
            url: '/overview',
            templateUrl: 'app/item/overview/overview.html',
            controller: 'ItemOverviewController',
            authenticateAdmin: true
        })
        .state('item.blacklist', {
            url: '/blacklist',
            templateUrl: 'app/item/blacklist/blacklist.html',
            controller: 'ItemBlacklistController',
            authenticateAdmin: true
        })
        .state('item.custom', {
            url: '/custom',
            templateUrl: 'app/item/custom/custom.html',
            controller: 'CustomItemsController',
            authenticateAdmin: true
        })
        .state('item.stock', {
            url: '/stock',
            templateUrl: 'app/item/stock/stock.html',
            controller: 'ItemStockController',
            authenticateAdmin: true
        });
});