/*jslint nomen: true */
/*globals angular, _ */
angular.module('adminApp').controller('ItemStockController', function ($scope, InventoryItem, SteamUser) {
    'use strict';

    $scope.inventoryItemLimit = 30;
    $scope.currentPage = 1;
    $scope.inventoryItemType = 'all';

    $scope.inventoryItemCount = InventoryItem.count();

    $scope.changePage = function (page, type) {
        InventoryItem.query({
            page: page,
            limit: $scope.inventoryItemLimit,
            type: type || $scope.inventoryItemType
        }).$promise.then(function (response) {
            $scope.inventoryItems = response;
        });
    };

    $scope.deleteItem = function (item) {
        InventoryItem.delete({
            id: item._id
        }).$promise.then(function () {
            $scope.changePage($scope.currentPage);
        });
    };

    $scope.sync = function () {
        InventoryItem.sync({}, {});
    };

    $scope.findDuplicates = function () {
        $scope.duplicates = InventoryItem.findDuplicates();
    };

    $scope.findStaleItems = function () {
        $scope.staleItems = InventoryItem.findStaleItems();
    };

    $scope.findUnlinkedItems = function () {
        $scope.unlinkedItems = InventoryItem.findUnlinkedItems();
    };

    $scope.findDesycnedItems = function () {
        $scope.desyncedItems = InventoryItem.findDesyncedItems();
    };

    $scope.removeStaleItem = function (inventoryItem) {
        _.remove($scope.staleItems, function (i) {
            return String(i._id) === String(inventoryItem._id);
        });
    };

    $scope.getUnownedItems = function () {
        $scope.inventoryItems = InventoryItem.query({
            page: 1,
            limit: 1000,
            unowned: true
        });
    };

    $scope.getCurrentInventoryItem = function (inventoryItem) {
        InventoryItem.get({
            id: inventoryItem._id
        }).$promise.then(function (inventoryItem) {
            $scope.currentInventoryitem = inventoryItem;
        });
    };

    $scope.searchUser = function (query) {
        $scope.searchResults = SteamUser.query({
            search: query,
            limit: 1000
        });
    };

    $scope.updateInventoryItem = function (inventoryItem) {
        InventoryItem.update({
            id: inventoryItem._id
        }, inventoryItem);
    };

    $scope.createMetaData = function (inventoryItem) {
        InventoryItem.createMetaData();
    };

    $scope.clearUser = function (inventoryItem) {
        inventoryItem.user = null;
    };

    $scope.changePage(1);
});