/*globals angular */
/*jslint nomen: true */
angular.module('adminApp').controller('ItemOverviewController', function ($scope, Item) {
    'use strict';

    $scope.currentPage = 1;
    Item.count({}).$promise.then(function (count) {
        $scope.itemTotal = count;
    });

    $scope.maxSize = 10;
    $scope.itemsPerPage = 100;

    function queryItems(pageNumber) {
        Item.query({
            id: pageNumber - 1,
            quantity: 100
        }).$promise.then(function (items) {
            $scope.items = items;
        });
    }

    $scope.$watch('currentPage', function (newValue) {
        queryItems(newValue);
    });

    $scope.paginate = function (pageNumber) {
        queryItems(pageNumber);
    };

    $scope.$watch('searchCriteria', function (newValue) {
        if (newValue === '') {
            $scope.searchResults = undefined;
        }
    });

    $scope.searchItems = function (searchCriteria) {
        Item.search({
            query: searchCriteria
        }).$promise.then(function (results) {
            $scope.searchResults = results;
        });
    };

    $scope.toggleBlacklist = function (item, index) {
        var blacklist = item.blacklist;
        if (angular.isUndefined(item.blacklist)) {
            blacklist = false;
        }
        Item.update({
            id: item._id
        }, {
            blacklist: !blacklist
        }).$promise.then(function () {
            $scope.items[index].blacklist = !blacklist;
        });
    };

    $scope.updateItem = function (item, price) {
        Item.setCustomPrice({
            id: item._id
        }, {
            value: price
        }).$promise.then(function () {
            if (price) {
                item.price.value = price;
                item.price.isCustom = true;
            } else {
                item.price.isCustom = false;
            }
        });
    }
});