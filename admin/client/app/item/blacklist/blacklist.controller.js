/*globals angular */
/*jslint nomen: true */
angular.module('adminApp').controller('ItemBlacklistController', function ($scope, Item) {
    'use strict';

    $scope.currentPage = 1;
    //$scope.itemTotal = Item.blacklistCount();
    $scope.maxSize = 10;
    $scope.itemsPerPage = 100;

    function getBlackList() {
        Item.getBlacklist({}).$promise.then(function (items) {
            $scope.items = items;
        });
    }

    $scope.$watch('currentPage', function () {
        getBlackList();
    });

    $scope.paginate = function (pageNumber) {
        getBlackList();
    };

    $scope.unBlacklist = function (item, index) {
        Item.changeBlacklist({
            id: item._id
        }, {
            blacklist: false
        }).$promise.then(function () {
            $scope.items.splice(index, 1);
        });
    };
});