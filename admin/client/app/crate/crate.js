/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('crate', {
        url: '/crate',
        templateUrl: 'app/crate/crate.html',
        controller: 'CrateController',
        authenticateAdmin: true
    });
});