/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('adminApp').controller('CrateController', function ($scope, Crate, socket, User, $uibModal, $log) {
    'use strict';

    $scope.crates = Crate.query();
    socket.syncUpdates('crate', $scope.crates);

    $scope.createCrate = function (formValid, crateName, maxSlots, costPerSlot) {
        Crate.save({
            name: crateName,
            cost: costPerSlot
        }).$promise.then(function () {
            crateName = undefined;
            maxSlots = undefined;
            costPerSlot = undefined;
        });
    };

    $scope.deleteCrate = function (crate) {
        Crate.delete({
            id: crate._id
        });
    };

    $scope.openBotStoreModel = function (crate) {
        var modalInstance = $uibModal.open({
            animation: true,
            templateUrl: 'components/item-store/bots/bot-store.html',
            controller: 'BotStoreModalController',
            size: 'lg'
        });

        modalInstance.result.then(function (selectedItems) {
            var items = _.map(selectedItems, function (i) {
                return {
                    item: i.item._id,
                    assetId: i.assetId
                };
            });
            crate.inventory = crate.inventory.concat(items);
            $scope.updateCrate(crate);
        });
    };

    $scope.deleteItem = function (itemIndex, crate) {
        crate.inventory.splice(itemIndex, 1);
        $scope.updateCrate(crate);
    };

    $scope.removeCoin = function (crate, coinIndex) {
        crate.coins.splice(coinIndex, 1);
        $scope.updateCrate(crate);
    };

    $scope.updateCrate = function (crate) {
        Crate.update({
            id: crate._id
        }, crate);
    };

    $scope.addCoin = function (crate, amount, stock, rarity) {
        crate.coins.push({
            amount: amount,
            chance: stock,
            maxStock: stock,
            rarity: rarity
        });
        $scope.updateCrate(crate);
    };

    $scope.testCrate = function (crate) {
        crate.prizeWon = Crate.test({
            id: crate._id
        });
    };
});