/*jslint nomen: true */
/*globals angular */
angular.module('adminApp').controller('JackpotController', function ($scope, socket, $interval, JackpotEntry, InventoryItem) {
    'use strict';
    
    $scope.jackpotEntries = InventoryItem.getInJackpot();

    $scope.refundItem = function (jackpotEntry) {
        InventoryItem.refundJackpotEntry({
            id: jackpotEntry._id
        }, {});
    };
});

/*jslint nomen: true */
/*globals angular */
/*angular.module('adminApp').controller('JackpotController', function ($scope, $interval, InventoryItem) {
    'use strict';

    $scope.jackpotEntries = InventoryItem.getInJackpot();

    $scope.refundItem = function (jackpotEntry) {
        InventoryItem.refundJackpotEntry({
            id: jackpotEntry._id
        });
    };
});*/