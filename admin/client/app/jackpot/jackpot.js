/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('jackpot', {
        url: '/jackpot',
        templateUrl: 'app/jackpot/jackpot.html',
        controller: 'JackpotController',
        authenticateAdmin: true
    });
});