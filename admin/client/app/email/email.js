/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('email', {
        url: '/email',
        templateUrl: 'app/email/email.html',
        controller: 'EmailController',
        authenticateAdmin: true
    });
});