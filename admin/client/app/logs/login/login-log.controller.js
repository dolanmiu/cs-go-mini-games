/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('adminApp').controller('LoginLogController', function ($scope, LoginLog) {
    'use strict';

    $scope.logsLimit = 30;

    LoginLog.count().$promise.then(function (response) {
        $scope.logsTotal = response.count;
    });

    $scope.logs = LoginLog.query({
        page: 1,
        limit: $scope.logsLimit
    });

    $scope.deleteLog = function (log) {
        LoginLog.delete({
            id: log._id
        });
    };

    $scope.changePage = function (page) {
        LoginLog.query({
            page: page,
            limit: $scope.logsLimit
        }).$promise.then(function (response) {
            $scope.logs = response;
        });
    };
});