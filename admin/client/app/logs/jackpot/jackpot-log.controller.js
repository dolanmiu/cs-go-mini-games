/*jslint nomen: true */
/*globals angular, _ */
angular.module('adminApp').controller('JackpotLogController', function ($scope, $uibModal, JackpotLog) {
    'use strict';

    $scope.logsLimit = 30;

    $scope.deleteLog = function (log) {
        JackpotLog.delete({
            id: log._id
        });
    };

    $scope.moreInfo = function (log) {
        var modalInstance = $uibModal.open({
            animation: $scope.animationsEnabled,
            templateUrl: 'app/logs/jackpot/jackpot-more-info.html',
            controller: 'JackpotLogMoreInfoController',
            size: 'lg',
            resolve: {
                logId: function () {
                    return log._id;
                }
            }
        });
    };

    $scope.changePage = function (page) {
        JackpotLog.query({
            page: page,
            limit: $scope.logsLimit
        }).$promise.then(function (response) {
            $scope.logs = response.docs;
            $scope.logsTotal = response.total;
        });
    };
    
    $scope.changePage(1);
});