/*globals angular */
angular.module('adminApp').controller('JackpotLogMoreInfoController', function ($scope, $modal, socket, $modalInstance, JackpotLog, logId) {
    'use strict';
        
    $scope.log = JackpotLog.get({
        id: logId
    });

    $scope.ok = function () {
        $modalInstance.close();
    };

    $scope.cancel = function () {
        $modalInstance.dismiss();
    };
});