/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('adminApp').controller('CrateLogController', function ($scope, CrateGameLog) {
    'use strict';

    $scope.logsLimit = 30;

    CrateGameLog.count().$promise.then(function (response) {
        $scope.logsTotal = response.count;
    });

    $scope.logs = CrateGameLog.query({
        page: 1,
        limit: $scope.logsLimit
    });

    $scope.deleteLog = function (log) {
        CrateGameLog.delete({
            id: log._id
        });
    };

    $scope.changePage = function (page) {
        CrateGameLog.query({
            page: page,
            limit: $scope.logsLimit
        }).$promise.then(function (response) {
            $scope.logs = response;
        });
    };
});