/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('logs', {
            abstract: true,
            url: '/logs',
            template: '<ui-view />'
        })
        .state('logs.crate', {
            url: '/crate',
            templateUrl: 'app/logs/crate/crate.html',
            controller: 'CrateLogController',
            authenticateAdmin: true
        })
        .state('logs.jackpot', {
            url: '/jackpot',
            templateUrl: 'app/logs/jackpot/jackpot.html',
            controller: 'JackpotLogController',
            authenticateAdmin: true
        })
        .state('logs.login', {
            url: '/login',
            templateUrl: 'app/logs/login/login.html',
            controller: 'LoginLogController',
            authenticateAdmin: true
        });
});