/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('adminApp').controller('NewsController', function ($scope, socket, News, Upload, $timeout) {
    'use strict';

    $scope.news = News.query({
        page: 1
    });
    socket.syncUpdates('news', $scope.news);

    $scope.labelTypes = ['Default', 'Primary', 'Success', 'Info', 'Warning', 'Danger'];
    
    $scope.categories = ['General', 'Patch Notes', 'Giveaways'];

    $scope.newPost = {
        label: {}
    };

    $scope.addPost = function (formValid) {
        if (!formValid || !$scope.newPostFile) {
            $scope.submitted = true;
            return;
        }

        var tags = _.map($scope.newPost.tags, 'text'),
            file = $scope.newPostFile;

        if (file && !file.$error) {
            file.upload = Upload.upload({
                url: '/api/news',
                file: file,
                fields: {
                    title: $scope.newPost.title,
                    body: $scope.newPost.body,
                    tags: tags,
                    label: $scope.newPost.label,
                    category: $scope.newPost.category
                }
            });

            file.upload.then(function (response) {
                $timeout(function () {
                    file.result = response.data;
                });
            }, function (response) {
                if (response.status > 0) {
                    $scope.errorMsg = response.status + ': ' + response.data;
                }
            });

            file.upload.progress(function (evt) {
                file.progress = Math.min(100, parseInt(100.0 * evt.loaded / evt.total, 10));
            });
        }
    };

    $scope.updatePost = function (post) {
        post.tags = _.map(post.tags, 'text');

        News.update({
            id: post._id
        }, post);
    };

    $scope.updateThumbnail = function (post, file) {
        post.file = file;
        Upload.upload({
            url: '/api/news/' + post._id + '/thumbnail',
            file: file
        });
    };

    $scope.deletePost = function (post) {
        News.delete({
            id: post._id
        });
    };

    $scope.uploadFiles = function (file) {
        $scope.newPostFile = file;
    };
});