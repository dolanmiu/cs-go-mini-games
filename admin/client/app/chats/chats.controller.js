/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('adminApp').controller('ChatsController', function ($scope, socket, JackpotChat, CrateGameChat) {
    'use strict';

    $scope.jackpotChats = JackpotChat.query();
    socket.syncUpdates('jackpotChat', $scope.jackpotChats);

    $scope.crateGameChats = CrateGameChat.query();
    socket.syncUpdates('crateGameChat', $scope.crateGameChats);

    $scope.deleteJackpotChat = function (chat) {
        JackpotChat.delete({
            id: chat._id
        });
    };

    $scope.deleteCrateGameChat = function (chat) {
        CrateGameChat.delete({
            id: chat._id
        });
    };

    $scope.$on('$destroy', function () {
        socket.unsyncUpdates('jackpotChat');
        socket.unsyncUpdates('crateGameChat');
    });
});