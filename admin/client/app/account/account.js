/*globals angular */
angular.module('adminApp').config(function ($stateProvider) {
    'use strict';

    /*$stateProvider
        .state('login', {
            url: '/login',
            templateUrl: 'app/account/login/login.html',
            controller: 'LoginCtrl'
        })
        .state('logout', {
            url: '/logout?referrer',
            referrer: 'main',
            template: '',
            controller: function ($state, Auth) {
                var referrer = $state.params.referrer ||
                    $state.current.referrer ||
                    'main';
                Auth.logout();
                $state.go(referrer);
            }
        })
        .state('settings', {
            url: '/settings',
            templateUrl: 'app/account/settings/settings.html',
            controller: 'SettingsCtrl',
            authenticate: true
        });*/
}).run(function ($rootScope) {
    'use strict';

    $rootScope.$on('$stateChangeStart', function (event, next, nextParams, current) {
        if (next.name === 'logout' && current && current.name && !current.authenticate) {
            next.referrer = current.name;
        }
    });
});