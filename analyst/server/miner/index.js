/*jslint node: true */
'use strict';

var request = require('request');
var Q = require('q');

var formatter = require('./formatter');
var scheduler = require('./scheduler');
var Item = require('../api/models').Item;

function getItemXml(pageNumber) {
    var startItemNumber = pageNumber * 100,
        url = 'http://steamcommunity.com/market/search/render/?query=&start=' + startItemNumber + '&count=100&appid=730',
        deferred = Q.defer();

    request(url, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            var html;
            try {
                html = JSON.parse(body).results_html.replace(/(?:\\[rnt]|[\r\n\t]+)+/g, "");
            } catch (err) {
                return deferred.reject(err);
            }
            formatter(html).done(function (items) {
                var itemsString = JSON.stringify(items);
                if (itemsString.includes('FAILED')) {
                    deferred.reject('Steam XML has some errors. Did not parse correctly.');
                } else {
                    deferred.resolve(items);
                }
            }, function (err) {
                deferred.reject(err);
            });
        } else {
            deferred.reject(error);
        }
    });
    return deferred.promise;
}

module.exports = function () {
    scheduler(getItemXml);
};