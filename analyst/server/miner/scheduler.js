/*jslint node: true */
'use strict';

var CronJob = require('cron').CronJob;
var Item = require('../api/models').Item;

function updateItem(item) {
    Item.findOne({
        'item.marketName': item.item.marketName
    }, function (err, doc) {
        if (doc) {
            // Updating doc
            doc.price = item.price;
            doc.save(function (err) {
                if (err) {
                    console.error(err);
                }
            });
        } else {
            // Saving doc
            item.save(function (err) {
                if (err) {
                    console.error(err);
                }
            });
        }
    });
}

module.exports = function (getItemXml) {
    var counter = 0,
        job = new CronJob('*/30 * * * * *', function () {
            getItemXml(counter).then(function (items) {
                if (items.length !== 100) {
                    console.info('Scheduler found no more items, page count is now reset. Last page at: ' + counter);
                    counter = 0;
                }
                items.forEach(function (element) {
                    var newItem = new Item(element);
                    updateItem(newItem);
                });
                counter += 1;
                console.info('Scheduler retrieved ' + items.length + ' items from steam community.');
            }, function (err) {
                console.error(err); //connection or request error.
            });
        }, null, true);
};