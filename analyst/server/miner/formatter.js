/*jslint regexp: true, node: true */
'use strict';

var htmlToJson = require('html-to-json');

function splitMarketName(marketName) {
    var myRegexp = /([^|]+) \| ([a-zA-Z \-]+) \(([a-zA-Z \-]+)\)/g,
        match = myRegexp.exec(marketName);
    return match;
}

function getPrice(priceText) {
    var myRegexp = /Starting at:\$([0-9\.]+) ([A-Z]+)/g,
        match = myRegexp.exec(priceText);
    return match;
}

function getImageUrl(imageText) {
    var myRegexp = /http:\/\/steamcommunity-a.akamaihd.net\/economy\/image\/(.+)\//g,
        match = myRegexp.exec(imageText);
    return match;
}

module.exports = function (html) {
    var promise = htmlToJson.parse(html, ['.market_listing_row_link', {
        'price': {
            'steam': function ($product) {
                var price = getPrice($product.find('.market_listing_their_price').text());
                if (price === null) {
                    return 'FAILED';
                }
                return price[1];
            },
            'currency': function ($product) {
                var price = getPrice($product.find('.market_listing_their_price').text());
                if (price === null) {
                    return 'FAILED';
                }
                return price[2];
            }
        },
        'quantity': function ($product) {
            return $product.find('.market_listing_num_listings_qty').text().replace(/\,/g, '');
        },
        'item': {
            'marketName': function ($product) {
                return $product.find('.market_listing_item_name').text();
            },
            'model': function ($product) {
                var splitName = splitMarketName($product.find('.market_listing_item_name').text());
                if (splitName !== null) {
                    return splitName[1];
                } else {
                    return '';
                }
            },
            'set': function ($product) {
                var splitName = splitMarketName($product.find('.market_listing_item_name').text());
                if (splitName !== null) {
                    return splitName[2];
                } else {
                    return '';
                }
            },
            'exterior': function ($product) {
                var splitName = splitMarketName($product.find('.market_listing_item_name').text());
                if (splitName !== null) {
                    return splitName[3];
                } else {
                    return '';
                }
            }
        },
        'iconUrl': function ($product) {
            var imageUrl = getImageUrl($product.find('img').attr('src'));
            return imageUrl[1];
        }
    }]);

    return promise;
};