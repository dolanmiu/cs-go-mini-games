/*jslint node: true */
var express = require('express');

var controller = require('./controller');

var router = express.Router();

router.get('/item/:name', controller.getMarketInfo);

module.exports = router;