/*jslint node: true */
'use strict';

var request = require('request');

function handleError(res) {
    res.status(500).json({
        success: false
    });
}

exports.getMarketInfo = function (req, res, next) {
    var url = 'http://steamcommunity.com/market/priceoverview/?currency=3&appid=730&market_hash_name=' + req.params.name;

    request(url, function (error, response, body) {
        if (!error && response.statusCode === 200) {
            return res.json(JSON.parse(body));
        } else {
            return handleError(res);
        }
    });
};