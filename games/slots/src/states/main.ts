module CrateSlot.States {
    export interface IMainState extends Phaser.State {
        rollSlots(uniqueKey: string, callback: Function): void;
        setCoinData(slots: Array<Models.Coin>): void;
        setInventoryData(slots: Array<Models.InventoryItem>): void;
    }

    export class MainState extends Phaser.State implements IMainState {

        private wheel: Prefabs.Wheel;
        private slotLoader: Prefabs.SlotLoader;
        private slotFactory: Prefabs.SlotFactory;
        private meter: Prefabs.Meter;
        private graphics: Phaser.Graphics;

        constructor() {
            super();
        }

        preload() {
            this.game.stage.disableVisibilityChange = true;
            this.game.load.crossOrigin = "anonymous";
            this.game.load.image("slot-one-bg", "/assets/images/slot-background/one.png");
            this.game.load.image("slot-two-bg", "/assets/images/slot-background/two.png");
            this.game.load.image("slot-three-bg", "/assets/images/slot-background/three.png");
            this.game.load.image("slot-four-bg", "/assets/images/slot-background/four.png");
            this.game.load.image("slot-five-bg", "/assets/images/slot-background/five.png");
            this.game.load.image("slot-six-bg", "/assets/images/slot-background/six.png");
        }

        create() {
            var wheelGroup = new Phaser.Group(this.game);
            wheelGroup.z = 10;

            var slotBgGroup = new Phaser.Group(this.game);
            slotBgGroup.z = 0;

            //slotBgGroup.add();
            
            this.graphics = this.game.add.graphics(0, 0);
            this.wheel = new Prefabs.Wheel(this.game, wheelGroup);
            this.slotLoader = new Prefabs.SlotLoader(this.game);
            this.slotLoader.crossOrigin = "anonymous";
            this.slotFactory = new Prefabs.SlotFactory(this.game, slotBgGroup, wheelGroup);
            var meterFactory = new Prefabs.MeterFactory(this.game);
            this.meter = meterFactory.create(this.game.width / 2, 1);
            this.meter.draw(this.graphics);

            var group = new Phaser.Group(this.game)
            group.add(this.graphics);
            group.z = 100;

            this.game.stateLoadedCallback();
        }

        update() {
            this.wheel.update(this.game.time.elapsed / 1000);
        }

        rollSlots(uniqueKey: string, callback: Function): void {
            this.wheel.setSpeed(2500);
            setTimeout(() => {
                this.wheel.stopAt(uniqueKey, callback);
            }, 2000);
        }

        setCoinData(coins: Array<Models.Coin>): void {
            this.slotLoader.batchLoadCoins(coins, () => {
                coins.forEach(coin => {
                    var slot = this.slotFactory.create(coin.name, coin._id, coin.amount + ' Coins');
                    this.wheel.addSlot(slot);
                });
            });
        }

        setInventoryData(inventoryItems: Array<Models.InventoryItem>): void {
            this.slotLoader.batchLoadInventory(inventoryItems, () => {

                inventoryItems.forEach(inventoryItem => {
                    var slot = this.slotFactory.create(inventoryItem.assetId, inventoryItem._id, inventoryItem.item.item.marketName);
                    this.wheel.addSlot(slot);
                });

            });
        }
    }
}