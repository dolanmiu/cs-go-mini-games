module CrateSlot {
    export class Game {
        private game: Phaser.Game;
        private width: number;
        private height: number;

        constructor(width: number, height: number) {
            this.width = width;
            this.height = height;
        }

        run(container: string, loadedCallback: Function) {
            this.game = new Phaser.Game(this.width, this.height, Phaser.AUTO, container, CrateSlot.States.MainState, false);
            this.game.stateLoadedCallback = loadedCallback;
        }

        setCoinData(coinInput: Array<Models.Coin>) {
            var state = <States.IMainState>this.game.state.getCurrentState();
            if (state) {
                var coins = new Array<Models.Coin>();
                coinInput.forEach(coin => {
                   coins.push(new Models.Coin(coin.amount, coin.rarity, coin._id)); 
                });
                state.setCoinData(_.shuffle(coins));
            }
        }

        setInventoryData(inventoryItems: Array<Models.InventoryItem>) {
            var state = <States.IMainState>this.game.state.getCurrentState();
            if (state) {
                state.setInventoryData(inventoryItems);
            }
        }

        rollSlots(uniqueKey: string, callback: Function) {
            var state = <States.IMainState>this.game.state.getCurrentState();
            if (state) {
                state.rollSlots(uniqueKey, callback);
            }
        }
        
        destroy() {
            this.game.destroy();
        }
    }
}