module CrateSlot.Prefabs {
    export class Meter extends Phaser.Polygon {

        constructor(...points: Phaser.Point[]) {
            super(points);
        }

        draw(graphics: Phaser.Graphics) {
            graphics.beginFill(0xFFFFFF);
            graphics.drawPolygon(this.points);
            graphics.endFill();
        }
    }
}