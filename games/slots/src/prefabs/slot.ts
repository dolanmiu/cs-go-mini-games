module CrateSlot.Prefabs {
    export class Slot extends Phaser.Sprite {

        public originalX: number;
        public uniqueKey: string;
        public travelDistance: number;

        private graphics: Phaser.Graphics;
        private background: Phaser.Image;
        private text: Phaser.Text;

        constructor(game: Phaser.Game, key: string, uniqueKey: string, foregroundGroup: Phaser.Group, backgroundGroup: Phaser.Group, text: string) {
            this.background = game.add.image(0, 5, "slot-" + key + "-bg");
            super(game, 0, 0, key);
            this.uniqueKey = uniqueKey;
            this.y = game.height / 2 - 150 / 2;
            this.width = 150;
            this.height = 84;

            backgroundGroup.add(this.background);
            foregroundGroup.add(this);

            this.text = new Phaser.Text(game, 0, 0, text, { font: 'Source Sans Pro', fill: '#ffffff', align: 'center', fontSize: 16 });
            this.text.setShadow(0, 1, 'rgba(0,0,0,0.7)', 0);
            this.text.anchor.set(0.5);
            this.text.y = game.height - 30;
            foregroundGroup.add(this.text);
        }

        update() {
            this.background.x = this.x;
            this.text.x = Math.round(this.x + this.width / 2);
        }

        resetPosition() {
            this.x = this.originalX;
        }
    }
}