module CrateSlot.Prefabs {
    export class SlotLoader extends Phaser.Loader {

        constructor(game: Phaser.Game) {
            super(game);
        }

        batchLoadCoins(coins: Array<Models.Coin>, callback: () => void) {
            coins.forEach(coin => {
                this.image(coin.name, coin.imageUrl, false);
            });

            this.onFileComplete.add((progress, cacheKey) => {

            }, this);

            this.onLoadComplete.addOnce(() => {
                callback();
            });

            this.start();
        }

        batchLoadInventory(inventoryItems: Array<Models.InventoryItem>, callback: () => void) {
            inventoryItems.forEach(inventoryItem => {
                this.image(inventoryItem.assetId, inventoryItem.item.iconUrl.large, false);
            });

            this.onLoadComplete.addOnce(() => {
                callback();
            });

            this.start();
        }
    }
}