module CrateSlot.Prefabs {
    export class Wheel {

        private speed: number;
        private game: Phaser.Game;
        private slots: Array<Slot>;
        private imgGroup: Phaser.Group;
        private bgGroup: Phaser.Group;
        private offset: number;
        private slotSpacing: number;
        private graphics: Phaser.Graphics;

        constructor(game: Phaser.Game, imgGroup: Phaser.Group) {
            this.speed = 0;
            this.game = game;
            this.slots = new Array<Slot>();
            this.imgGroup = imgGroup;
            this.offset = 0;
            this.slotSpacing = 10;
            this.graphics = game.add.graphics(0, 0);
            this.bgGroup = new Phaser.Group(game);
            this.bgGroup.z = -10;
        }

        private findSlot(uniqueKey: string): Slot {
            for (var i = 0; i < this.slots.length; i++) {
                if (this.slots[i].uniqueKey === uniqueKey) {
                    return this.slots[i];
                }
            }
        }

        private getTimeTaken(distancetoStoppingPoint): number {
            var time = (2 * distancetoStoppingPoint) / this.speed;
            return time * 1000;
        }

        private getDistanceForSlotToCenter(slot: Slot): number {
            var targetPoint = this.game.width / 2;
            var distance = targetPoint - slot.x;
            var targetVariation = Math.floor(Math.random() * slot.width);
            //var targetVariation = 0;

            var nearestDistance = 0;
            if (distance > 0) {
                nearestDistance = distance - targetVariation;
            } else {
                var remainingDistance = this.game.width - slot.x;
                var newDistance = this.getSlotsWidth() - targetPoint
                nearestDistance = remainingDistance + newDistance - targetVariation;
            }

            return nearestDistance + 5 * this.getSlotsWidth();
        }

        private getSlotsWidth() {
            return this.slots.length * (this.slots[0].width + this.slotSpacing);
        }

        update(delta: number) {

            this.slots.forEach(slot => {
                var addedDelta = this.speed * delta;
                slot.travelDistance += addedDelta;
                slot.x = slot.width + (slot.travelDistance + this.offset) % this.getSlotsWidth() - (this.getSlotsWidth() / 2);

                slot.update();
            });
        }

        setSpeed(speed: number) {
            this.speed = speed;
        }

        addSlot(slot: Slot) {
            if (this.slots.length > 0) {
                var lastSlot = this.slots[this.slots.length - 1];
                var startX = lastSlot.position.x + lastSlot.width + this.slotSpacing;

                slot.position.x = startX;
                slot.originalX = startX;
                slot.travelDistance = startX;
            } else {
                var startX = 0;

                slot.position.x = startX;
                slot.originalX = startX;
                slot.travelDistance = startX;
            }
            this.slots.push(slot);
            this.imgGroup.add(slot);
        }

        reset() {
            this.slots.forEach(slot => {
                slot.resetPosition();
            });
            this.offset = 0;
        }

        stopAt(uniqueKey: string, callback: Function) {
            var slot = this.findSlot(uniqueKey);
            var tween = this.game.add.tween(this);
            var distance = this.getDistanceForSlotToCenter(slot);
            var time = this.getTimeTaken(distance);

            this.speed = 0;

            tween.to({ offset: this.offset + distance }, time, Phaser.Easing.Quadratic.Out, true);
            tween.onComplete.add(callback, this);
        }
    }
}