module CrateSlot.Prefabs {
    export class SlotFactory {

        private game: Phaser.Game;
        private graphics: Phaser.Graphics;
        private foregroundGroup: Phaser.Group;
        private backgroundGroup: Phaser.Group;

        constructor(game: Phaser.Game, foregroundGroup: Phaser.Group, backgroundGroup: Phaser.Group) {
            this.game = game;
            this.foregroundGroup = foregroundGroup; 
            this.backgroundGroup = backgroundGroup;
        }

        create(key: string, uniqueId: string, text: string): Slot {
            var slot = new Slot(this.game, key, uniqueId, this.foregroundGroup, this.backgroundGroup, text);
            return slot;
        }
    }
}