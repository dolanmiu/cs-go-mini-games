module CrateSlot.Prefabs {
    export class MeterFactory {
        
        private game: Phaser.Game;
        
        constructor(game: Phaser.Game) {
            this.game = game;
        }
        
        create(x: number,  width: number) : Meter {
            var meter = new Meter(new Phaser.Point(x, 0), new Phaser.Point(x + width, 0), new Phaser.Point(x + width, this.game.height), new Phaser.Point(x, this.game.height));
            
            return meter;
        }
    }
}