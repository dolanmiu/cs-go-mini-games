module CrateSlot.Models {
    export class Coin {

        public amount: number;
        public imageUrl: string;
        public name: string;
        public _id: string;
        public rarity: number;

        constructor(amount: number, rarity: number, _id: string) {
            this.amount = amount;
            this.rarity = rarity;
            this._id = _id;

            if (this.rarity === 1) {
                this.imageUrl = "/assets/images/coin/one.png";
                this.name = "one";
            } else if (this.rarity  === 2) {
                this.imageUrl = "/assets/images/coin/two.png";
                this.name = "two";
            } else if (this.rarity === 3) {
                this.imageUrl = "/assets/images/coin/three.png";
                this.name = "three";
            } else if (this.rarity === 4) {
                this.imageUrl = "/assets/images/coin/four.png";
                this.name = "four";
            } else if (this.rarity === 5) {
                this.imageUrl = "/assets/images/coin/five.png";
                this.name = "five";
            } else if (this.rarity === 6) {
                this.imageUrl = "/assets/images/coin/six.png";
                this.name = "six";
            }
        }
    }
}