module CrateSlot.Models {
    export class InventoryItem {
        public item: {
            iconUrl: {
                large: string
            }
        };
        public assetId: string;
        public _id: string;
    }
}