(function e(t,n,r){function s(o,u){if(!n[o]){if(!t[o]){var a=typeof require=="function"&&require;if(!u&&a)return a(o,!0);if(i)return i(o,!0);var f=new Error("Cannot find module '"+o+"'");throw f.code="MODULE_NOT_FOUND",f}var l=n[o]={exports:{}};t[o][0].call(l.exports,function(e){var n=t[o][1][e];return s(n?n:e)},l,l.exports,e,t,n,r)}return n[o].exports}var i=typeof require=="function"&&require;for(var o=0;o<r.length;o++)s(r[o]);return s})({1:[function(require,module,exports){
module.exports = {
    init: function() {
        // add here your scaling options
    },

    preload: function() {

    },

    create: function() {
        game.state.start('load');
    }
};

},{}],2:[function(require,module,exports){
module.exports = {
    preload: function() {
        console.log('will now attempt to load images');

        game.load.image('menu_title', 'src/images/menu/menu_game_title.png');
        game.load.image('menu_arrow', 'src/images/menu/menu_arrow.png');
        game.load.image('menu_button1', 'src/images/menu/menu_button.png');
        game.load.image('menu_button2', 'src/images/menu/menu_button2.png');
        game.load.image('menu_button3', 'src/images/menu/menu_button3.png');
    },

    create: function() {
    	console.log('Will now attempt to enter menu state');
        game.state.start('menu');
    }
};

},{}],3:[function(require,module,exports){
var buttons = require('./menu_buttons.js'),
    arrow = require('./menu_arrow.js');

module.exports = {
    create: function() {
    	console.log('creating stuff in menu state');
        this.cursors = game.input.keyboard.createCursorKeys();
        this.gameTitle = game.add.image(game.world.centerX, game.world.centerY - 200, 'menu_title');
        this.gameTitle.anchor.setTo(0.5, 0.5);
        buttons.draw();
        arrow.draw();
    },

    update: function() {
        arrow.move(this.cursors, buttons);
    }
};

},{"./menu_arrow.js":4,"./menu_buttons.js":5}],4:[function(require,module,exports){
module.exports = {
    draw: function() {
        //Add it with initial position at first button
        this.arrow = game.add.image(game.world.centerX - 100,
            game.world.centerY - 50, 'menu_arrow');
        this.arrow.anchor.setTo(0.5, 0.5);

        //Arrow will take 200ms to go up/down the menu
        this.arrow.moveDelay = 200;

        //Control if the arrow should keep moving or not
        this.arrow.canMove = true;

        //Keep track of the current button the pointer is at
        this.arrow.currentButton = 1;

        //We add an horizontal tween so that the arrow feels nicer
        game.add.tween(this.arrow)
            .to({
                x: this.arrow.x - 10
            }, 700, Phaser.Easing.Quadratic.Out)
            .to({
                x: this.arrow.x
            }, 400, Phaser.Easing.Quadratic.In)
            .loop()
            .start();
    },

    //Here we will set the rules for how it moves
    //We need to pass the variable holding the cursor keys
    //and the object that holds the buttons.
    move: function(cursors, buttons) {
        if (cursors.down.isDown && this.arrow.canMove) {
            //This stops the arrow from traveling way too fast
            this.arrow.canMove = false;

            //Which is reset to true after a 255ms delay
            this.allowMovement();

            if (this.arrow.currentButton === 1) {
                //I made a custom tween function for this
                this.tween(buttons, 2);
            } else if (this.arrow.currentButton === 2) {
                this.tween(buttons, 3);
            } else {
                this.tween(buttons, 1);
            }
        }

        if (cursors.up.isDown && this.arrow.canMove) {
            this.arrow.canMove = false;
            this.allowMovement();
            if (this.arrow.currentButton === 1) {
                this.tween(buttons, 3);
            } else if (this.arrow.currentButton === 2) {
                this.tween(buttons, 1);
            } else {
                this.tween(buttons, 2);
            }
        }

        if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            //This will activate the button that the pointer is at
            this.activateButton(buttons, this.arrow.currentButton);
        }
    },

    tween: function(buttons, buttonNum) {
        game.add.tween(this.arrow)
            .to({
                y: game.world.centerY + buttons.pos[buttonNum - 1]
            }, this.arrow.moveDelay, Phaser.Easing.Quadratic.In)
            .start();
        this.arrow.currentButton = buttonNum;
    },

    allowMovement: function() {
        game.time.events.add(255, (function() {
            this.arrow.canMove = true;
        }), this);
    },

    activateButton: function(buttons, currentButton) {
        buttons[buttons.callbacks[currentButton - 1]]();
    }
};

},{}],5:[function(require,module,exports){
module.exports = {
    //We track the offset of each button
    pos: [-50, 50, 150],
    //We track which callback each button has
    callbacks: ['playState', 'playState', 'playState'],
    draw: function() {
        //We now create our buttons using a constructor function, YAY!
        this.button1 = this.addButton(1, this.playState);
        // this.button1.inputEnabled = true;
        this.button1.anchor.setTo(0.5, 0.5);
        // this.button1.events.onInputUp.add(displaySomething, this);

        this.button2 = this.addButton(2, this.playState);
        this.button2.anchor.setTo(0.5, 0.5);

        this.button3 = this.addButton(3, this.playState);
        this.button3.anchor.setTo(0.5, 0.5);
    },

    addButton: function(button, func) {
        return game.add.button(game.world.centerX,
            game.world.centerY + this.pos[button - 1],
            'menu_button' + button, func);
    },

    playState: function() {
        game.state.start('play');
    }

};

},{}],6:[function(require,module,exports){
var Crate = require('./play/crate');
var LabelButton = require('./shared_components/LabelButton');

module.exports = {
    create: function () {
        //console.log('hi');
        //this.crate = game.add.image(game.world.centerX, game.world.centerY, 'falcon-crate');
        this.players = [];

        var style = {
                font: 'bold 20pt Arial',
                fill: 'white'
            },
            skins = {};

        this.instructionText = game.add.text(20, 20, 'Click on your crate', style);
        this.instructionText.visible = true;

        this.alphaCrate = new Crate(skins, 5, 200);
        //game.add.button(game.world.height / 2, game.world.width / 2, 'falcon-crate', this.alphaCrate.enterDraw);
        //this.openCrate = game.add.button(game.world.height / 2, game.world.width / 2, 'falcon-crate', function () {
        //    console.log('crate opened');
        //    this.alphaCrate.enter();
        //
        //});

        this.enterButton = game.add.button(50,50,'button',this.alphaCrate.enterDraw, this.alphaCrate);

        console.log(this.alphaCrate.getPlayers());


        //this.alphaCrate.enterDraw();

    },
    update: function () {
    },
    preload: function () {
        game.load.image('falcon-crate', 'src/images/crate/crate/falcon/falcon-crate.jpg');
    },
    enterDraw: function (playerName, numberOfPPs) {
        var player = {
            name: playerName,
            points: numberOfPPs
        };


        if (this.players.length < 20) {
            this.players.push(player);
        } else {
            console.log('20 player limit already reached');
        }
    },
    selectWinner: function () {

    }
};


},{"./play/crate":7,"./shared_components/LabelButton":8}],7:[function(require,module,exports){
/**
 * Created by Kelv on 03/06/2015.
 */
module.exports = Crate = function (skins, maxEntries, entryPrice) {
    this.skins = skins;
    this.maxEntries = maxEntries;
    this.entryPrice = entryPrice;
    this.players = [];
    console.log('Crate object has been created');

};

Crate.prototype.enterDraw = function () {
    console.log(this.maxEntries);
    if (this.players.length < this.maxEntries) {
        this.players.push('Player ' + this.players.length);
        console.log('Pushed: ' + this.players);
    } else {
        console.log('Entry has closed, ' + this.maxEntries + ' people have already joined');
    }
    //console.log(steamID + ' has entered');

};

Crate.prototype.getPlayers = function () {
    return this.players;
}
},{}],8:[function(require,module,exports){
module.exports = LabelButton = function (game, x, y, key, label, callback,
                                         callbackContext, overFrame, outFrame, downFrame, upFrame) {
    Phaser.Button.call(this, game, x, y, key, callback,
        callbackContext, overFrame, outFrame, downFrame, upFrame);

    //Style how you wish...
    this.style = {
        'font': '10px Arial',
        'fill': 'black'
    };
    this.anchor.setTo(0.5, 0.5);
    this.label = new Phaser.Text(game, 0, 0, label, this.style);

    //puts the label in the center of the button
    this.label.anchor.setTo(0.5, 0.5);

    this.addChild(this.label);
    this.setLabel(label);

    //adds button to game
    game.add.existing(this);
};

LabelButton.prototype = Object.create(Phaser.Button.prototype);
LabelButton.prototype.constructor = LabelButton;

LabelButton.prototype.setLabel = function (label) {

    this.label.setText(label);

};
},{}],9:[function(require,module,exports){
// We use window.game instead of var game because we want it to be accessible from everywhere.
window.game = new Phaser.Game(800, 600, Phaser.AUTO);

game.state.add('play', require('./states/play.js'));
game.state.add('load', require('./states/load.js'));
game.state.add('menu', require('./states/menu.js'));
game.state.add('boot', require('./states/boot.js'));
game.state.start('boot');

},{"./states/boot.js":1,"./states/load.js":2,"./states/menu.js":3,"./states/play.js":6}]},{},[9]);
