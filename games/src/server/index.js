/**
 * Created by Kelv on 19/06/2015.
 */

var app = require('express')(),
    server = require('http').Server(app),
    io = require('socket.io')(server),
    routes = require('./routes.js'),
    socketEvents = require('./socketEvents.js'),
    cors = require('cors');

var players = [],
    totalPp = 0;

app.use(cors());
//app.listen(3333);

server.listen(9000);

app.get('/', routes.root);
app.get('/crates', routes.crates);


io.on('connection', function (socket) {
    console.log('a user connected: ' + socket.id);

    io.emit('room join', socket.id + ' has connected');

    socket.on('disconnect', socketEvents.disconnect);
    socket.on('chat message', function (msg) {
        console.log('message: ' + msg);
        io.emit('chat message', socket.id + ': ' + msg);
    });
    socket.on('room join', function () {
        io.emit('room join', socket.id + ' has connected');
    });
    socket.on('typing', function () {
        io.emit('typing', socket.id);
    });
    socket.on('not-typing', function () {
        io.emit('not-typing', socket.id);
    });
    socket.on('buy-in', socketEvents.buyIn);
});


console.log('Running on port 3333!');