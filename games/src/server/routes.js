var config = require('./config.json'),
    monk = require('monk'),
    db = monk(config.host + '/' + config.dbName),
    collection = db.get('bugs'),
    crates = db.get('crates');

module.exports.root = function (req, res) {
    res.send('This is the API root');
};

module.exports.crates = function (req, res) {
    crates.find({}, function (err, docs) {
        res.send(docs);
    });
};