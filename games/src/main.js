/*globals module, require, Game, Phaser, console, game, window */
var playState = require('./states/play');
var loadState = require('./states/load');
var menuState = require('./states/menu');
var bootState = require('./states/boot');
var buyState = require('./states/buy');
// var awardState = require('./states/award');
// var nameState = require('./states/name-rolling');

module.exports = Game = function (width, height, socket, assetUrl, token, crateDetails) {
    'use strict';

    this.width = width;
    this.height = height;
    this.socket = socket;
    this.assetUrl = assetUrl;
    this.token = token;
    this.crateDetails = crateDetails;

};

Game.prototype.run = function (container) {
    'use strict';

    // We use window.game instead of var game because we want it to be accessible from everywhere.
    window.game = new Phaser.Game(this.width, this.height, Phaser.AUTO, container);
    // console.log(this.assetUrl)
    this.scale = new Phaser.ScaleManager(game,this.width,this.height);
    game.state.add('play', playState(this.socket, this.assetUrl, this.token));
    game.state.add('load', loadState(this.assetUrl));
    game.state.add('menu', menuState());
    game.state.add('boot', bootState());
    game.state.add('buy', buyState(this.socket, this.token, this.assetUrl, this.crateDetails));
    // game.state.add('name-rolling', nameState(this.socket, this.assetUrl, this.names));
    // game.state.start('buy');
    // game.state.add('award', awardState(this.socket));
    game.state.start('boot');
    // game.state.start('name-rolling');
};

Game.prototype.rescale = function (width,height) {
    'use strict';
    // this.scale = {};
    if (game.device.desktop) {
        this.scale.scaleMode = Phaser.ScaleManager.RESIZE;
        // this.scale.minWidth = this.width / 2;
        // this.scale.minHeight = this.height / 2;
        // this.scale.maxWidth = width;
        // this.scale.maxHeight = height;
        // this.scale.setMinMax(0,0,width,height);
        // this.scale.pageAlignHorizontally = true;
        // this.scale.pageAlignVertically = true;
        // this.scale.setScreenSize(true);
      }
    // } else {
    //     this.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
    //     // this.scale.minWidth = this.width / 2;
    //     // this.scale.minHeight = this.height / 2;
    //     // this.scale.maxWidth = 2048; //You can change this to gameWidth*2.5 if needed
    //     // this.scale.maxHeight = 1228; //Make sure these values are proportional to the gameWidth and gameHeight
    //     this.scale.pageAlignHorizontally = true;
    //     this.scale.pageAlignVertically = true;
    //     this.scale.forceOrientation(true, false);
    //     this.scale.hasResized.add(this.gameResized, this);
    //     this.scale.enterIncorrectOrientation.add(this.enterIncorrectOrientation, this);
    //     this.scale.leaveIncorrectOrientation.add(this.leaveIncorrectOrientation, this);
    //     // this.scale.setScreenSize(true);
    // }
    // var ow = parseInt(game.canvas.style.width, 10);
    // var oh = parseInt(game.canvas.style.height, 10);
    // var r = Math.max(window.innerWidth / ow, window.innerHeight / oh);
    // var nw = ow * r;
    // var nh = oh * r;
    // game.canvas.style.width = nw + "px";
    // game.canvas.style.height = nh + "px";
    // game.canvas.style.marginLeft = (window.innerWidth / 2 - nw / 2) + "px";
    // game.canvas.style.marginTop = (window.innerHeight / 2 - nh / 2) + "px";
    // document.getElementById("game").style.width = window.innerWidth + "px";
    // document.getElementById("game").style.height = window.innerHeight - 1 + "px"; //The css for body includes 1px top margin, I believe this is the cause for this -1
    // document.getElementById("game").style.overflow = "hidden";
}
