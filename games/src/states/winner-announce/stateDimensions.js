/*globals module, StateDimensions, game */

module.exports = StateDimensions = function () {
    'use strict';
    this.background = {
        x: 0,
        y: 0
    };
    this.stars = {
        x: 0,
        y: 0
    };
    this.ground = {
        x: 0,
        y: game.world.height - game.cache.getImage('winner_ground').height
    };
    this.box = {
        x: game.world.width / 2 - game.cache.getImage('winner_box').width / 2,
        y: game.world.height / 2 - game.cache.getImage('winner_box').height / 2
    };
    this.buildings = {
        x: 0,
        y: this.ground.y - game.cache.getImage('winner_buildings').height
    };
    this.ct = {
        x: this.box.x + game.cache.getImage('winner_box').width,
        y: this.ground.y - game.cache.getImage('winner_ct').height
    };
    this.fire = {
        x: 0,
        y: this.ground.y - game.cache.getImage('winner_fire').height
    };
    this.fireworks = {
        x: 0,
        y: this.fire.y
    };

    this.moon = {
        x: this.ct.x,
        y: game.world.height / 4
    };

};
