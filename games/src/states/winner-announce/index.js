/*globals module, game, require*/
var StateDimensions = require('./StateDimensions'),
    awardState = require('../award');

module.exports = function (assetUrl, winner) {
    'use strict';
    return {
        preload: function () {
            this.assetUrl = assetUrl;
            this.winner = winner;

            game.load.image('winner_background', this.assetUrl + '/winner-announce/blue-background.png');
            game.load.image('winner_box', this.assetUrl + '/winner-announce/box.png');
            game.load.image('winner_buildings', this.assetUrl + '/winner-announce/buildings.png');
            game.load.image('winner_ct', this.assetUrl + '/winner-announce/ct.png');
            game.load.image('winner_fire', this.assetUrl + '/winner-announce/fire.png');
            game.load.image('winner_fireworks', this.assetUrl + '/winner-announce/fireworks.png');
            game.load.image('winner_ground', this.assetUrl + '/winner-announce/ground.png');
            game.load.image('winner_moon', this.assetUrl + '/winner-announce/moon.png');
            game.load.image('winner_stars', this.assetUrl + '/winner-announce/stars.png');

            // game.load.spritesheet()
        },
        create: function () {
            this.stateDimensions = new StateDimensions();

            game.add.image(this.stateDimensions.background.x, this.stateDimensions.background.y, 'winner_background');
            game.add.image(this.stateDimensions.box.x, this.stateDimensions.box.y, 'winner_box');
            game.add.image(this.stateDimensions.buildings.x, this.stateDimensions.buildings.y, 'winner_buildings');
            game.add.image(this.stateDimensions.ct.x, this.stateDimensions.ct.y, 'winner_ct');
            game.add.image(this.stateDimensions.fire.x, this.stateDimensions.fire.y, 'winner_fire');
            game.add.image(this.stateDimensions.fireworks.x, this.stateDimensions.fireworks.y, 'winner_fireworks');
            game.add.image(this.stateDimensions.ground.x, this.stateDimensions.ground.y, 'winner_ground');
            game.add.image(this.stateDimensions.moon.x, this.stateDimensions.moon.y, 'winner_moon');
            game.add.image(this.stateDimensions.stars.x, this.stateDimensions.stars.y, 'winner_stars');

            this.announceText = game.add.bitmapText(this.stateDimensions.box.x, this.stateDimensions.box.y, 'pixi_font', 'The winner is....', 50);
            this.nameText = game.add.bitmapText(this.stateDimensions.box.x + 30, this.stateDimensions.box.y + 30, 'pixi_font', this.winner, 90);
            this.congratulationsText = game.add.bitmapText(this.stateDimensions.box.x + 30, this.stateDimensions.box.y + 90, 'pixi_font', 'congratulations!', 50);

            setTimeout(function () {
                game.state.start('boot', true, false);
            }, 3000);
        },
        update: function () {}
    };
};
