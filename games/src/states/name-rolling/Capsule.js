/*globals module, Capsule, game, Phaser, console, require */

var winnerAnnounceState = require('../winner-announce');

module.exports = Capsule = function (assetUrl, names, winner) {
    'use strict';
    this.assetUrl = assetUrl;
    this.namesHolder = names;
    this.winner = winner;
    console.log(this.namesHolder);
};

Capsule.prototype.preload = function (startTime) {
    'use strict';
    game.load.image('name_capsule', this.assetUrl + '/name-rolling/capsule.png');
    game.load.image('name_capsule_background', this.assetUrl + '/name-rolling/middle-section.png');
    game.load.image('name_line', this.assetUrl + '/name-rolling/black-line.png');
    this.startTime = startTime;
};

Capsule.prototype.create = function (x, y) {
    'use strict';
    this.capsuleX = x;
    this.capsuleY = y;
    this.index = 0;

    this.cornerX = x - 156 / 2;
    this.cornerY = y - 135 / 2;
    this.capsuleBackground = game.add.image(this.cornerX, this.cornerY, 'name_capsule_background');
    this.lowerY = y + 135 / 2;

    game.physics.startSystem(Phaser.Physics.ARCADE);

    this.lines = game.add.group();
    this.lines.enableBody = true;
    this.lines.createMultiple(10, 'name_line');

    this.names = [];

    console.dir(this.names);
};

Capsule.prototype.addLine = function () {
    'use strict';
    this.line = this.lines.getFirstDead();
    this.line.reset(this.cornerX, this.cornerY);
    // if (game.time.elapsedSecondsSince(this.startTime) > 5) {
    //     this.line.body.velocity.y = 50;
    // } else {
    //     this.line.body.velocity.y = 50;
    // }
    this.line.body.velocity.y = this.speed;
    this.line.body.acceleration.y = this.decel;

    this.line.body.immovable = true;

    this.line.boundsRect = this.bounds;

    this.line.checkWorldBounds = true;
    this.line.outOfBoundsKill = true;
};

Capsule.prototype.addName2 = function () {
    'use strict';
    var i, spliceCounter = 0;
    this.deadNamesPool = [];
    for (i = 0; i < this.namesHolder.length; i += 1) {
        this.names[i] = game.add.bitmapText(this.cornerX, this.cornerY + i * 50, 'pixi_font', this.namesHolder[i], 60);
        game.physics.arcade.enable(this.names[i]);
        this.names[i].body.immovable = true;
        this.names[i].body.velocity.y = this.speed;
        // this.names[i].body.velocity.y =

        if (this.names[i].y > this.lowerY) {
            this.names[i].kill();

            this.deadNamesPool.push(this.names[i]);
            spliceCounter += 1;

        }
    }
    this.names.splice(this.names.length - spliceCounter, spliceCounter);
    console.log(this.names);
    this.textHeight = 30;

    this.capsule = game.add.image(this.capsuleX, this.capsuleY, 'name_capsule');
    this.capsule.anchor.x = 0.5;
    this.capsule.anchor.y = 0.5;

};
// May be deprecated soon.
Capsule.prototype.update = function () {
    'use strict';
    // console.dir(this.lines);
    var self = this;
    this.lines.forEachAlive(function (line) {
        if (line.y > self.lowerY) {
            line.kill();
            // console.dir(line);
        }
    });
};
// This function updates the text and manages the pool
Capsule.prototype.updateText = function () {
    'use strict';
    var self = this,
        i,
        j;

    for (i = 0; i < this.names.length; i += 1) {
        console.log(this.names[i].body.velocity.y);
        if (this.names[i].body.velocity.y < 110 && this.names[i].text === this.winner && Math.floor(this.names[i].y) > game.world.height / 2 - 5 && Math.ceil(this.names[i].y) < game.world.height / 2 + 5) {
            console.log('stop conditions are met');
            for (j = 0; j < this.names.length; j += 1) {
                this.names[j].body.velocity.y = 0;
                this.names[j].body.acceleration.y = 0;
            }

            setTimeout(function () {
                game.state.add('winner-announce', winnerAnnounceState(self.assetUrl, self.winner));
                game.state.start('winner-announce');
            }, 2000);

        }
        if (this.names[i].y > this.lowerY) {
            this.speed = this.names[i].body.velocity.y;

            this.names[i].kill();
            this.deadNamesPool.push(this.names[i]);
            this.names.splice(i, 1);
        }

    }
    if (this.names[0].y > this.cornerY + this.textHeight && this.deadNamesPool.length > 0) {

        this.deadNamesPool[0].reset(this.cornerX, this.cornerY - this.textHeight);
        game.physics.arcade.enable(this.deadNamesPool[0]);
        this.deadNamesPool[0].body.immovable = true;
        this.deadNamesPool[0].body.velocity.y = this.speed;
        this.deadNamesPool[0].body.acceleration.y = this.decel;
        this.names.unshift(this.deadNamesPool[0]);
        this.deadNamesPool.splice(0, 1);
    }
};

Capsule.prototype.setSpeed = function (speed) {
    'use strict';
    this.speed = speed;
};

Capsule.prototype.setDecel = function (decel) {
    'use strict';
    var i;
    this.decel = -decel;

    for (i = 0; i < this.names.length; i += 1) {
        this.names[i].body.acceleration.y = this.decel;
    }

};
