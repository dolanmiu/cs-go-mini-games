/*globals module, NameDimensions, game */

module.exports = NameDimensions = function () {
    'use strict';
    var ctDim = game.cache.getImage('name_ct'),
        groundDim = game.cache.getImage('name_ground'),
        buildingsDim = game.cache.getImage('name_buildings'),
        capsuleDim = game.cache.getImage('name_capsule');

    this.background = {
        x: 0,
        y: 0
    };
    this.capsuleBox = {
        x: 0,
        y: 0
    };
    this.ground = {
        x: 0,
        y: game.world.height - groundDim.height
    };
    this.buildings = {
        x: 0,
        y: this.ground.y - buildingsDim.height
    };
    this.fire = {
        x: 0,
        y: this.buildings.y
    };
    this.capsule = {
        x: game.world.width / 2,
        y: game.world.height / 2
    };
    this.ct = {
        x: this.capsule.x + capsuleDim.width / 2,
        y: this.ground.y - ctDim.height
    };
    this.stars = {
        x: 0,
        y: 0
    };
    this.line = {
        x: 0,
        y: 0
    };
    this.moon = {
        x: 0,
        y: 0
    };
    this.textBox = {
        x: this.ct.x +ctDim.width,
        y: this.ct.y - ctDim.height / 2
    };
    this.textBoxText = {
        x: this.textBox.x,
        y: this.textBox.y
    };
};
