/*globals console, module, game, require */
var Capsule = require('./Capsule.js');

var NameDimensions = require('./NameDimensions');

module.exports = function (socket, assetUrl, losers, winner) {
    'use strict';
    return {
        preload: function () {

            this.socket = socket;
            this.assetUrl = assetUrl;
            this.startTime = game.time.time;
            this.decelCalled = false;
            this.losers = losers;
            this.names = ['a', 'b', 'c', 'd', 'e'];
            this.names.push(winner);
            this.winner = winner;

            this.capsule = new Capsule(this.assetUrl, this.names, this.winner);
            game.load.image('name_background', this.assetUrl + '/name-rolling/blue-background.png');
            game.load.image('name_capsulebox', this.assetUrl + '/name-rolling/box.png');
            game.load.image('name_buildings', this.assetUrl + '/name-rolling/buildings.png');
            // game.load.image('name_capsule', this.assetUrl + '/name-rolling/capsule.png');
            game.load.image('name_ct', this.assetUrl + '/name-rolling/ct.png');
            game.load.image('name_fire', this.assetUrl + '/name-rolling/fire.png');
            game.load.image('name_ground', this.assetUrl + '/name-rolling/ground.png');
            game.load.image('name_stars', this.assetUrl + '/name-rolling/stars.png');
            game.load.image('name_line', this.assetUrl + '/name-rolling/black-line.png');
            game.load.image('name_moon', this.assetUrl + '/name-rolling/moon.png');
            this.capsule.preload();

        },
        create: function () {

            this.nameDimensions = new NameDimensions();
            console.dir(this.nameDimensions);
            this.nameBackground = game.add.image(this.nameDimensions.background.x, this.nameDimensions.background.y, 'name_background');
            this.stars = game.add.image(this.nameDimensions.stars.x, this.nameDimensions.stars.y, 'name_stars');
            this.moon = game.add.image(this.nameDimensions.moon.x, this.nameDimensions.moon.y, 'name_moon');
            this.ground = game.add.image(this.nameDimensions.ground.x, this.nameDimensions.ground.y, 'name_ground');
            this.fire = game.add.image(this.nameDimensions.fire.x, this.nameDimensions.fire.y, 'name_fire');
            this.buildings = game.add.image(this.nameDimensions.buildings.x, this.nameDimensions.buildings.y, 'name_buildings');
            this.capsule.setSpeed(300);
            this.capsule.create(this.nameDimensions.capsule.x, this.nameDimensions.capsule.y);
            this.ct = game.add.image(this.nameDimensions.ct.x, this.nameDimensions.ct.y, 'name_ct');

            this.textBox = game.add.image(this.nameDimensions.textBox.x, this.nameDimensions.textBox.y, 'textbox');
            this.textBoxText = game.add.bitmapText(this.nameDimensions.textBoxText.x, this.nameDimensions.textBoxText.y, 'pixi_font', 'hi', 50);

            // this.capsule.create(game.world.width / 2, game.world.height / 2);
            this.timer = game.time.events.loop(2000, this.capsule.addLine, this.capsule);
            // this.timer2 = game.time.events.loop(2000, this.capsule.addName, this.capsule);
            this.capsule.addName2();

            // this.capsule.setDecel()
        },
        update: function () {
            this.capsule.updateText();

            if (game.time.elapsedSecondsSince(this.startTime) > 5 && this.decelCalled === false) {
                this.decelCalled = true;
                this.capsule.setDecel(10);
            }
        }
    };
};
