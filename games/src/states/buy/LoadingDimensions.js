/*globals module, LoadingDimensions, game */

module.exports = LoadingDimensions = function () {
    'use strict';
    this.background = {
        x: 0,
        y: 0
    };
    this.clouds = {
        x: 0,
        y: 0
    };
    this.ground = {
        x: 0,
        y: game.world.height - game.cache.getImage('buy_ground')
    };
    this.buildings = {
        x: 0,
        y: game.world.height - game.cache.getImage('loading_buildings').height
    };
    this.terrorist = {
        x: game.world.width * 3 / 4,
        y: game.world.height - game.cache.getImage('loading_terrorist').height - 15
    };
    this.chicken = {
        x: this.terrorist.x - game.cache.getImage('loading_chicken').height,
        y: game.world.height - 50
    };
    this.textBox = {
        x: this.terrorist.x - game.cache.getImage('textbox').width / 2,
        y: this.terrorist.y - 20
    };
    this.text = {
        x: this.textBox.x - game.cache.getImage('textbox').width / 2,
        y: this.textBox.y
    };
};
