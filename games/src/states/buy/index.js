//============ BUY STATE ================================================
/*globals module, game, console, require, addNumber, minusNumber, buyCrate*/
/*jslint nomen:true*/

var request = require('request');
var awardState = require('../award');
var PurchaseHud = require('./purchaseHud');
var HudDimensions = require('./HudDimensions');
var LoadingDimensions = require('./LoadingDimensions');

module.exports = function (socket, token, assetUrl, details) {
    'use strict';
    return {
        preload: function () {
            var i;
            this.details = details;
            this.socket = socket;
            this.assetUrl = assetUrl;
            this.token = token;
            this.quantity = '0';

            game.load.spritesheet('chicken', this.assetUrl + '/spritesheets/chicken.png', 36, 36, 6);
            game.load.spritesheet('shooting_t', this.assetUrl + '/spritesheets/shooting-t.png', 91, 137);
            this.loadingDimensions = new LoadingDimensions();

            for (i = 0; i < this.details.inventory.length; i += 1) {
                game.load.image('rolling_skin' + i, 'http://localhost:9000/api/proxy/' + this.details.inventory[i].item.iconUrl + '/image/200');
            }

        },
        create: function () {
            var self = this;
            // this.quantity = '0';
            console.log(this.details._id);
            this.socket.on('crate:full:' + this.details._id, function (response) {
                console.log("Winner will now be announced");
                // console.log('The winner is: ' + JSON.stringify(response));
                // console.log('self.quantity is: ' + self.quantity);
                console.dir(response);
                game.state.add('award', awardState(self.socket, self.assetUrl, self.details, self.token, response));
                game.state.start('award');

            });

            // console.log(groundDimensions[1].y);
            // groundDimensions[1].y = ;

            this.background = game.add.image(0, 0, 'loading_background');
            this.clouds = game.add.image(0, 0, 'loading_clouds');
            this.clouds2 = game.add.image(game.world.width, 0, 'loading_clouds');

            // this.chicken = game.add.image(this.loadingDimensions.ground.x, this.loadingDimensions.ground.y, 'loading_chicken');
            this.buildings = game.add.image(this.loadingDimensions.buildings.x, this.loadingDimensions.buildings.y, 'loading_buildings');
            this.terrorist = game.add.image(game.world.width - 100, this.loadingDimensions.terrorist.y, 'loading_terrorist');

            this.terrorist2 = game.add.sprite(game.world.width - 200, game.world.height - 150, 'shooting_t');
            this.terrorist2.animations.add('standby', [37, 38, 39, 40, 41, 42, 43], 5, true);
            this.terrorist2.animations.add('alert', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17], 5, false).onComplete.add(alertStopped, this);
            this.terrorist2.animations.play('alert', 5, false);

            this.chicken = game.add.sprite(this.loadingDimensions.chicken.x, this.loadingDimensions.chicken.y, 'chicken');
            game.physics.arcade.enable(this.chicken);
            this.chicken.animations.add('flap', [0, 1, 2, 3, 5], 30, true);
            this.chicken.animations.play('flap', 30, true);
            this.textBox = game.add.image(this.loadingDimensions.textBox.x, this.loadingDimensions.textBox.y, 'textbox');
            this.guideText = game.add.bitmapText(this.loadingDimensions.text.x + 5, this.loadingDimensions.text.y, 'pixi_font', 'El gringo,\njust filling up \nsome crates,\nboss...', 20);

            this.textBox.anchor.x = 0.5;
            this.textBox.scale.x = -1;
            this.chicken.body.velocity.x = -280;

            function alertStopped(sprite, animation) {
                self.terrorist2.animations.play('standby', 5, true);
            }
            // this.returnBox = game.add.button(returnDimensions[1].x, returnDimensions[1].y, 'buy_return', function () {
            //     game.state.start('play');
            // });

        },
        update: function () {
            this.clouds.x -= 1;
            this.clouds2.x -= 1;

            if (this.chicken.x < game.world.width / 4) {
                this.chicken.scale.x = -1;
                this.chicken.x = game.world.width / 4 + 36;
                this.chicken.body.velocity.x = -this.chicken.body.velocity.x;
            } else if (this.chicken.x > game.world.width * 3 / 4) {
                this.chicken.scale.x = 1;
                this.chicken.x = game.world.width * 3 / 4 - 36;
                this.chicken.body.velocity.x = -this.chicken.body.velocity.x;
            }

            if (this.clouds.x <= 0 - game.world.width) {
                this.clouds.x = game.world.width;
            }
            if (this.clouds2.x <= 0 - game.world.width) {
                this.clouds2.x = game.world.width;
            }
        }
    };
};
