/*globals Dimensions, module, game */

module.exports = Dimensions = function () {
    'use strict';
    this.background = {
        size: game.cache.getImage('hud_background'),
        x: 0,
        y: game.world.height - game.cache.getImage('hud_background').height
    };
    this.quantityBox = {
        size: game.cache.getImage('hud_quantitybox'),
        x: game.world.width / 4 - game.cache.getImage('hud_quantitybox').width / 2,
        y: this.background.y + 30
    };
    this.plus = {
        size: game.cache.getImage('hud_plus'),
        x: this.quantityBox.x + game.cache.getImage('hud_quantitybox').width + 20,
        y: this.background.y + 35
    };
    this.minus = {
        size: game.cache.getImage('hud_minus'),
        x: this.quantityBox.x - game.cache.getImage('hud_minus').width - 20,
        y: this.background.y + 35
    };
    this.purchase = {
        size: game.cache.getImage('hud_purchase'),
        x: game.world.width / 4 - game.cache.getImage('hud_purchase').width / 2,
        y: this.quantityBox.y + game.cache.getImage('hud_quantitybox').height + 5
    };
    this.crate = {
        size: game.cache.getImage('hud_crate'),
        x: game.world.width * 3 / 4 - game.cache.getImage('hud_crate').width / 2,
        y: this.background.y + game.cache.getImage('hud_background').height / 2 - game.cache.getImage('hud_crate').height / 2
    };
};
