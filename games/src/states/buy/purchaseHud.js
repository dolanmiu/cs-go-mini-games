/*globals module, PurchaseHud, game, require, addNumber, minusNumber, console, purchase */
/*jslint nomen:true */
var Dimensions = require('./HudDimensions'),
    request = require('request'),
    awardState = require('../award');

module.exports = PurchaseHud = function (assetUrl, quantity, details, token, socket) {
    'use strict';
    this.assetUrl = assetUrl;
    this.quantity = quantity;
    this.details = details;
    this.token = token;
    this.socket = socket;

};

PurchaseHud.prototype.preload = function () {
    'use strict';
    this.dimensions = new Dimensions();
    // console.log('HUD assets loaded');// console.log(this.dimensions.background.x);
};

PurchaseHud.prototype.create = function () {
    'use strict';
    var self = this;

    // this.quantity = '0';
    this.hudBackground = game.add.image(this.dimensions.background.x, this.dimensions.background.y, 'hud_background');
    this.quantityBox = game.add.image(this.dimensions.quantityBox.x, this.dimensions.quantityBox.y, 'hud_quantitybox');
    this.plus = game.add.button(this.dimensions.plus.x, this.dimensions.plus.y, 'hud_plus', addNumber);
    this.minus = game.add.button(this.dimensions.minus.x, this.dimensions.minus.y, 'hud_minus', minusNumber);
    this.purchase = game.add.button(this.dimensions.purchase.x, this.dimensions.purchase.y, 'hud_purchase', purchase);
    this.crate = game.add.image(this.dimensions.crate.x, this.dimensions.crate.y, 'hud_crate');
    this.quantityText = game.add.bitmapText(this.dimensions.quantityBox.x + 10, this.dimensions.quantityBox.y + 10, 'pixi_font', this.quantity, 55);
    this.purchaseText = game.add.bitmapText(this.dimensions.purchase.x + 20, this.dimensions.purchase.y + 10, 'pixi_font', 'PURCHASE', 55);

    // this.socket.on('crate:winner', function (response) {
    //     console.log("Winner will now be announced");
    //     console.log('The winner is: ' + JSON.stringify(response));
    //     game.state.add('award', awardState(response, self.socket, self.assetUrl));
    //     game.state.start('award');
    // });

    function addNumber() {
        if (self.quantity < self.details.maxSlots) {
            // console.log('Adding number.' + self.quantity);
            self.quantity = parseInt(self.quantity, 10);
            self.quantity += 1;
            self.quantity = self.quantity.toString();
        }
    }

    function minusNumber() {
        if (self.quantity > 0) {
            self.quantity = parseInt(self.quantity, 10);
            self.quantity -= 1;
            self.quantity = self.quantity.toString();
        }
    }

    function purchase() {
        // var self = this;
        request({
            uri: 'http://localhost:9000/api/crates/' + self.details._id + '/buy',
            method: 'PUT',
            headers: {
                authorization: self.token
            },
            form: {
                'quantity': self.quantity
            }
        }, function (error, response, body) {
            if (error !== null) {
                console.log('The error is: ' + error);
            } else {
                console.log('The response is: ' + response);
                console.log('The body is: ' + body);
            }
        });
    }
};

PurchaseHud.prototype.update = function () {
    'use strict';
    this.quantityText.text = this.quantity;
};

PurchaseHud.prototype.getQuantity = function () {
    'use strict';
    return this.quantity;
};
