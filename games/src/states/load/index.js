/*globals module, game */
module.exports = function (assetUrl) {
    'use strict';

    return {
        preload: function () {
            game.load.crossOrigin = 'Anonymous';
            game.state.disableVisibilityChange = true;
// should be removed
            game.load.image('menu_title', assetUrl + '/menu/menu_game_title.png');
            game.load.image('menu_arrow', assetUrl + '/menu/menu_arrow.png');
            game.load.image('menu_button1', assetUrl + '/menu/menu_button.png');
            game.load.image('menu_button2', assetUrl + '/menu/menu_button2.png');
            game.load.image('menu_button3', assetUrl + '/menu/menu_button3.png');
            game.load.image('menu_background', assetUrl + '/menu/menu final.png');

            game.load.image('textbox', assetUrl + '/name-rolling/text-box.png');


            game.load.image('play_background', assetUrl + '/crate-screen/blue-background.png');
            game.load.image('play_buildings', assetUrl + '/crate-screen/buildings.png');
            game.load.image('play_water', assetUrl + '/crate-screen/crate2.png');
            game.load.image('play_crateborder', assetUrl + '/crate-screen/crate-border.png');
            game.load.image('play_clouds', assetUrl + '/crate-screen/clouds.png');

            game.load.image('loading_background', assetUrl + '/loading/blue-background.png');
            game.load.image('loading_buildings', assetUrl + '/loading/buildings.png');
            game.load.image('loading_chicken', assetUrl + '/loading/chicken.png');
            game.load.image('loading_clouds', assetUrl + '/loading/clouds.png');
            game.load.image('loading_terrorist', assetUrl + '/loading/terrorist2.png');

            game.load.image('buy_background', assetUrl + '/loading/blue-background.png');
            game.load.image('buy_stars', assetUrl + '/loading/clouds.png');
            game.load.image('loading_buildings', assetUrl + '/loading/buildings.png');
            game.load.image('buy_ground', assetUrl + '/buy/ground.png');
            game.load.image('buy_crate', assetUrl + '/buy/crate.png');
            game.load.image('buy_return', assetUrl + '/buy/return-button.png');
            game.load.image('buy_add', assetUrl + '/buy/add-button.png');
            game.load.image('buy_minus', assetUrl + '/buy/minus-button.png');
            game.load.image('buy_quantbox', assetUrl + '/buy/slot-number.png');
            game.load.image('buy_purchase', assetUrl + '/buy/purchase-button.png');
            game.load.image('loading_t', assetUrl + '/loading/terrorist2.png');
            game.load.image('loading_chicken', assetUrl + '/loading/chicken.png');

            game.load.image('hud_background', assetUrl + '/purchase-hud/background.png');
            game.load.image('hud_quantitybox', assetUrl + '/purchase-hud/enter-number.png');
            game.load.image('hud_plus', assetUrl + '/purchase-hud/increment.png');
            game.load.image('hud_minus', assetUrl + '/purchase-hud/increment.png');
            game.load.image('hud_purchase', assetUrl + '/purchase-hud/purchase-button.png');
            game.load.image('hud_crate', assetUrl + '/purchase-hud/water-crate.png');

            game.load.bitmapFont('ugly_font', assetUrl + '/font/font.png', assetUrl + '/font/font.fnt');
            game.load.bitmapFont('pixi_font', assetUrl + '/font/pixi/font.png', assetUrl + '/font/pixi/font.fnt');
        },

        create: function () {
            game.state.start('buy');
        }
    };
};
