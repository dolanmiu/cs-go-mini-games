/*globals module, Dimensions, game, console*/

module.exports = Dimensions = function () {
    'use strict';
    // this.hudDimensions = hudDimensions;
    this.ground = {
        size: game.cache.getImage('award_ground'),
        x: 0,
        y: game.world.height - game.cache.getImage('award_ground').height
    };
    console.log(this.ground.y);
    // console.log(game.world.height - this.hudDimensions.background.size.height);
    // console.log(game.cache.getImage('award_ground').height);

    this.background = {
        size: game.cache.getImage('award_background'),
        x: 0,
        y: 0
    };

    this.buildings = {
        size: game.cache.getImage('award_buildings'),
        x: 0,
        y: this.ground.y - game.cache.getImage('award_buildings').height
    };
    this.fires = {
        size: game.cache.getImage('award_fires'),
        x: 0,
        y: this.buildings.y
    };
    this.stars = {
        size: game.cache.getImage('award_stars'),
        x: 0,
        y: 0
    };
    this.moon = {
        size: game.cache.getImage('award_moon'),
        x: game.world.width * 7 / 8 - game.cache.getImage('award_moon').width / 2,
        y: 0
    };
    this.ct = {
        size: {
            width: 128,
            height: 163
        },
        x: game.world.width * 3 / 4,
        y: game.world.height / 2 - game.cache.getImage('award_box') / 2 - 120
    };
    this.textbox = {
        size: game.cache.getImage('award_textbox'),
        x: 0,
        y: 0
    };
};

Dimensions.prototype.getAll = function () {
    'use strict';
    return this.ground;
};
