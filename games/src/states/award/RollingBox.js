/*globals RollingBox, module, require, game, console */
var request = require('request');

module.exports = RollingBox = function (boxNumber, iconUrl, winningItem, speed) {
    'use strict';

    // this.assetUrl = assetUrl;
    // this.iconUrl = iconUrl;
    this.boxNumber = boxNumber;
    this.winningItem = winningItem;
    console.log(this.winningItem);
    this.front = false;
    this.stop = false;
    this.speed = speed;
    this.iconUrl = iconUrl;
    if (this.winningItem === true) {
        console.log('http://localhost:9000/api/proxy/' + this.iconUrl + '/image/200');
    }
    // this.iconUrl = iconUrl.substring(0, iconUrl.length - 8);

};

RollingBox.prototype.load = function () {
    'use strict';

    // this.box = game.load.image('rolling_box' + this.boxNumber, this.assetUrl +'/award/single-rolling-box');
    // game.cache.addImage('rolling_skin' + this.boxNumber, this.iconUrl, new Image());
    game.load.image('rolling_skin' + this.boxNumber, 'http://localhost:9000/api/proxy/' + this.iconUrl + '/image/200');
    // console.log('this.boxNumber is: ' + this.boxNumber);
    // console.log('this.iconUrl is: ' + this.iconUrl);
};

RollingBox.prototype.create = function () {
    'use strict';

    this.boxWidth = game.cache.getImage('award_box').width;
    // console.log(game.cache.getImage('rolling_skin' + this.boxNumber.width);
    this.box = game.add.image(this.boxWidth * this.boxNumber, 0, 'award_box');
    this.skin = game.add.image(this.boxWidth * this.boxNumber, 0, 'rolling_skin' + this.boxNumber);
    // this.box = game.add.image(boxName, )
    var self = this,
        decel,
        offset;

    if (this.winningItem === true) {
        offset = (game.world.width / 2 - this.box.x) / 2;
        this.decel = 2 * (game.world.width * 5 + offset - this.speed * 10) / (10 * 10);
    }

};

RollingBox.prototype.roll = function (speed) {
    'use strict';
    // if (this.box.x === game.world.width) {
    //     this.box.x = 0 - this.boxWidth;
    // }z`
    // this.speed = speed;
    // this.decel = decel;
    // if (this.speed > 0) {
    //     this.speed -= decel;
    // } else if (this.speed <= 0) {
    //     this.speed = 0;
    // }
    // console.log(this.decel);

    this.box.x += speed;
    this.skin.x += speed;

    // if (this.stop === true && this.winningItem === true) {
    //     var offset = game.world.width / 2 - this.box.x,
    //         decel = 2 * (game.world.width * 5 + offset - speed * decelTime) / (decelTime * decelTime);
    // } else {
    //
    // }

};

RollingBox.prototype.getDecel = function () {
    'use strict';
    // var decelTime = decelTime,
    // var self = this,
    //     decel,
    //     offset;
    //
    // if (this.winningItem === true) {
    //     offset = (game.world.width / 2 - this.box.x) / 2;
    //     decel = 2 * (game.world.width * 5 + offset - this.speed * decelTime) / (decelTime * decelTime);
    // }
    return this.decel;
};

RollingBox.prototype.stop = function () {
    'use strict';

};

RollingBox.prototype.getX = function () {
    'use strict';
    return this.box.x;
};

RollingBox.prototype.getWidth = function () {
    'use strict';
    return this.boxWidth;
};
