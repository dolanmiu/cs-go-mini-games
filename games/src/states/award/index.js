/*globals module, console, game, require */
var AwardDimensions = require('./AwardDimensions'),
    PurchaseHud = require('../buy/PurchaseHud'),
    RollingBox = require('./RollingBox'),
    RollingWheel = require('./RollingWheel'),
    RollingWheel2 = require('./RollingWheel2'),
    nameRollingState = require('../name-rolling');

module.exports = function (socket, assetUrl, details, token, winningCrateDetails) {
    'use strict';
    return {
        preload: function () {
            console.log('Welcome to Award state');
            var self = this;
            this.socket = socket;
            this.assetUrl = assetUrl;
            this.details = details;
            this.token = token;
            this.winningCrateDetails = winningCrateDetails;
            this.losers = winningCrateDetails.losers;
            this.time = game.time.time;

            game.load.image('award_ground', assetUrl + '/award/ground.png');
            game.load.image('award_background', assetUrl + '/award/blue-background.png');
            game.load.image('award_buildings', assetUrl + '/award/buildings.png');
            game.load.image('award_fires', assetUrl + '/award/fires.png');
            game.load.image('award_stars', assetUrl + '/award/stars.png');
            game.load.image('award_moon', assetUrl + '/award/moon.png');
            game.load.image('award_textbox', assetUrl + '/award/textbox1.png');
            game.load.image('award_box', assetUrl + '/award/single-rolling-box.png');
            game.load.spritesheet('ct_sprite', assetUrl + '/spritesheets/ct-cheer.png', 128, 162, 31);
            game.load.spritesheet("star1", assetUrl + '/spritesheets/star.png', 32, 32, 6);

            this.rollingWheel = new RollingWheel2(this.socket, this.assetUrl, this.details.inventory);
            this.rollingWheel.preload();

            console.log('main preload complete');
        },
        create: function () {
            console.log("Starting create");
            var self = this,
                i;

            this.dimensions = new AwardDimensions();

            this.background = game.add.image(this.dimensions.background.x, this.dimensions.background.y, 'award_background');
            this.ground = game.add.image(this.dimensions.ground.x, this.dimensions.ground.y, 'award_ground');
            this.buildings = game.add.image(this.dimensions.buildings.x, this.dimensions.buildings.y, 'award_buildings');
            this.fires = game.add.image(this.dimensions.fires.x, this.dimensions.fires.y, 'award_fires');
            this.stars = game.add.image(this.dimensions.stars.x, this.dimensions.stars.y, 'award_fires');
            this.moon = game.add.image(this.dimensions.moon.x, this.dimensions.moon.y, 'award_moon');
            this.textbox = game.add.image(this.dimensions.textbox.x, this.dimensions.textbox.y, 'award_textbox');

            // Fireworks
            this.fwkSprites = game.add.group();
            game.time.events.loop(50,createFwk, this);

            function createFwk() {
              var firework = sprite.create(Math.random * game.world.width, Math.random() * (game.world.height - game.cache.getImage('award_buildings').height), 'star1');
              firework.animations.add('sparkle');
              firework.play('sparkle',15, true);
            }

            this.ct = game.add.sprite((this.dimensions.ct.x - 80), (game.world.height / 2 - 260), 'ct_sprite');
            this.ct.animations.add('ct_cheer', [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28, 29, 30, 30, 29, 28, 27, 26, 25, 24, 23, 22, 21, 20, 19, 18, 17, 16, 15, 14, 13, 12, 11, 10, 9, 8, 7, 6, 5, 4, 3, 2]);
            this.ct.animations.play('ct_cheer', 240, true);
            console.log("main chunk of create done");

            // for (i = 0; i < 10; i += 1) {
            //     this.anim = this.firework.animations.add('fwk_sparkle');
            //     this.anim.play(10, true);
            // }

            this.rollingWheel.create();
            this.rollingWheel.setSpeed(1000);
            this.rollingWheel.addSkins();
            this.socket.on('crate:winningItem:' + this.details._id, function (response) {
                console.log('crate:winnerItem response is: ' + response);
                self.rollingWheel.setDecel(100, response._id);
            });

            this.socket.on('crate:winner:' + this.details._id, function (response) {
                console.dir(response);

                game.state.add('name-rolling', nameRollingState(self.socket, self.assetUrl, response.losers, response.winner.name));
                game.state.start('name-rolling');

            });
            console.log('main create complete');
        },
        update: function () {
            var self = this;
            this.rollingWheel.updateSkins();

        }
    };
};
