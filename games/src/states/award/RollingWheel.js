/*globals module, RollingWheel, require, console, game*/
/*jslint plusplus:true */
var RollingBox = require('./RollingBox'),
    nameRollingState = require('../name-rolling');

module.exports = RollingWheel = function (socket, assetUrl, time, inventory, speed) {
    'use strict';
    this.socket = socket;
    this.assetUrl = assetUrl;
    this.inventory = inventory;
    this.time = time;
    this.speed = speed;
    // this.losers = losers;
    // this.deceleration = deceleration;
    // this.prizeId = prize.item._id;
    this.winner = winner;
    // console.log('this.inventory is: ' + JSON.stringify(this.inventory
    console.log('THIS PRIZE ID IS: ' + this.prizeId);
    this.finished = false;
    this.finishTime = 0;

};
RollingWheel.prototype.load = function () {
    'use strict';
    var i,
        self = this;
    // winningItem = false;
    this.rollingBoxes = [];

    for (i = 0; i < this.inventory.length; i++) {

        if (this.inventory[i].item._id === this.prizeId) {
            this.rollingBoxes[i] = new RollingBox(i, this.inventory[i].item.iconUrl, true, self.speed);
        } else {
            this.rollingBoxes[i] = new RollingBox(i, this.inventory[i].item.iconUrl, false, self.speed);
        }

        // console.dir(this.inventory[i].item.iconUrl);
        this.rollingBoxes[i].load();
        // this.decel = this.rollingBoxes[i].getDecel();
    }

};

RollingWheel.prototype.create = function () {
    'use strict';
    var self = this,
        i;

    for (i = 0; i < self.inventory.length; i++) {
        self.rollingBoxes[i].create();
        if (self.rollingBoxes[i].winningItem === true) {
            self.decel = self.rollingBoxes[i].getDecel();
        }

    }

};

RollingWheel.prototype.update = function () {
    'use strict';
    var i,
        numberOfBoxes = this.rollingBoxes.length - 1;

    // console.dir(game.time);
    // console.log(game.time.elapsedSecondsSince(this.time));

    if (this.rollingBoxes[0].box.x >= 0) {
        var b = this.rollingBoxes[numberOfBoxes];

        this.rollingBoxes.splice(numberOfBoxes, 1);
        this.rollingBoxes.unshift(b);
        this.rollingBoxes[0].box.x = this.rollingBoxes[1].box.x - this.rollingBoxes[1].boxWidth;
        this.rollingBoxes[0].skin.x = this.rollingBoxes[1].box.x - this.rollingBoxes[1].boxWidth;
    }

    if (game.time.elapsedSecondsSince(this.time) > 10) {
        var middle;
        for (i = 0; i < this.inventory.length; i++) {
            middle = game.cache.getImage('award_box').width / 2;
            if (this.speed > 0) {
                this.speed -= 0.002;
            }
            if (this.rollingBoxes[i].winningItem === true && this.rollingBoxes[i].box.x > game.world.width / 2 - middle - 10 && this.rollingBoxes[i].box.x < game.world.width / 2 - middle + 10) {
                this.speed = 0;
                console.log('NOW IS IN MIDDLE');
                if (this.finished === false) {
                    this.finished = true;
                    this.finishedTime = game.time.time;
                } else if (this.finished === true && game.time.elapsedSecondsSince(this.finishedTime) > 5) {
                    game.state.add('name-rolling', nameRollingState(this.socket, this.assetUrl, this.losers, this.winner));
                    game.state.start('name-rolling');
                }
            }
            this.rollingBoxes[i].roll(this.speed);
        }
    } else if (game.time.elapsedSecondsSince(this.time) > 5) {
        if (this.speed > 0) {
            this.speed -= 0.01;
        }
        for (i = 0; i < this.inventory.length; i++) {
            this.rollingBoxes[i].roll(this.speed);
        }
    } else {
        for (i = 0; i < this.inventory.length; i++) {
            this.rollingBoxes[i].roll(this.speed);
        }
    }

};

// RollingWheel.prototype.stop = function () {//     'use strict';
// };
