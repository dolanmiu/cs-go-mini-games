/*globals module, Capsule, game, Phaser, console, RollingWheel2, setTimeout, require */

var winnerAnnounceState = require('../winner-announce');
var nameRollingState = require('../name-rolling');

module.exports = RollingWheel2 = function (socket, assetUrl, inventory) {
    'use strict';
    this.assetUrl = assetUrl;
    this.socket = socket;
    // this.namesHolder = names;
    // this.winner = winner;
    this.inventory = inventory;

    // console.log(this.namesHolder);
};

RollingWheel2.prototype.preload = function (startTime) {
    'use strict';
    var i;
    // game.load.image('name_capsule', this.assetUrl + '/name-rolling/no-line-capsule.png');
    // game.load.image('name_line', this.assetUrl + '/name-rolling/black-line.png');
    // for (i = 0; i < this.inventory.length; i += 1) {
    //     game.load.image('rolling_skin' + i, 'http://localhost:9000/api/proxy/' + this.inventory[i].item.iconUrl + '/image/200');
    // }

    this.startTime = startTime;
};

RollingWheel2.prototype.create = function () {
    'use strict';
    this.skins = [];

};

RollingWheel2.prototype.addSkins = function () {
    'use strict';
    var i, spliceCounter = 0,
        boxHeight = game.cache.getImage('award_box').height;
    this.wheelY = game.world.height / 2 - boxHeight / 2;
    this.deadSkinsPool = [];
    this.boxWidth = game.cache.getImage('award_box').width;
    this.halfBoxWidth = this.boxWidth / 2;
    console.log("this.inventory is: ");
    console.dir(this.inventory);
    game.physics.startSystem(Phaser.Physics.ARCADE);
    for (i = 0; i < this.inventory.length; i += 1) {
        // this.names[i] = game.add.bitmapText(this.cornerX, this.cornerY + i * 50, 'pixi_font', this.namesHolder[i], 60);
        this.skins[i] = {
            id: this.inventory[i].id,
            box: game.add.sprite(this.boxWidth * i, this.wheelY, 'award_box'),
            skin: game.add.sprite(this.boxWidth * i, this.wheelY, 'rolling_skin' + i)
        };
        // console.dir(this.skins);
        console.log("All IDs are :\n" + this.skins[i].id);

        game.physics.arcade.enable(this.skins[i].box);
        game.physics.arcade.enable(this.skins[i].skin);
        this.skins[i].box.body.immovable = true;
        this.skins[i].box.body.velocity.x = this.speed;
        this.skins[i].skin.body.immovable = true;
        this.skins[i].skin.body.velocity.x = this.speed;
        // this.names[i].body.velocity.y =

        if (this.skins[i].box.x > game.world.width - 1) {
            this.skins[i].box.kill();
            this.skins[i].skin.kill();

            this.deadSkinsPool.push(this.skins[i]);
            spliceCounter += 1;

        }
    }
    this.skins.splice(this.skins.length - spliceCounter, spliceCounter);
    // console.log(this.skins);
    // this.textHeight = 30;

};

RollingWheel2.prototype.update = function () {
    'use strict';
    // console.dir(this.lines);
    // var self = this;
    // this.lines.forEachAlive(function (line) {
    //     if (line.y > self.lowerY) {
    //         line.kill();
    //         // console.dir(line);
    //     }
    // });
};
RollingWheel2.prototype.updateSkins = function () {
    'use strict';
    var self = this,
        i,
        j;

    for (i = 0; i < this.skins.length; i += 1) {
        // if (this.skins[i].id === this.winningId) {
        //     console.log(true);
        // }
        // console.log(this.skins[i].box.body.velocity.x);

        if (this.skins[i].box.body.velocity.x < 600 && this.skins[i].id === this.winningId && Phaser.Math.fuzzyEqual(this.skins[i].box.x + this.halfBoxWidth, game.world.width / 2, 50)) {
            // console.log('stop conditions are met');
            for (j = 0; j < this.skins.length; j += 1) {
                this.skins[j].box.body.velocity.x = 0;
                this.skins[j].box.body.acceleration.x = 0;
                this.skins[j].skin.body.velocity.x = 0;
                this.skins[j].skin.body.acceleration.x = 0;
            }

            // setTimeout(function () {
            //     // game.state.add('winner-announce', winnerAnnounceState(self.assetUrl, self.winner));
            //     // game.state.start('winner-announce');
            //     game.state.add('name-rolling', nameRollingState(self.socket, self.assetUrl, losers, winner));
            //     game.state.start('name-rolling');
            // }, 2000);

        }
        // if (this.skins[i].box.x > game.world.width - 1) {
        if (Phaser.Math.fuzzyGreaterThan(this.skins[i].box.x, game.world.width, 1)) {
            this.speed = this.skins[i].box.body.velocity.x;

            this.skins[i].box.kill();
            this.skins[i].skin.kill();
            this.deadSkinsPool.push(this.skins[i]);
            this.skins.splice(i, 1);
        }

    }
    if (Phaser.Math.fuzzyGreaterThan(this.skins[0].box.x, 0, 1) && this.deadSkinsPool.length > 0) {

        this.deadSkinsPool[0].box.reset(this.skins[0].box.x - this.boxWidth + 10, this.wheelY);
        this.deadSkinsPool[0].skin.reset(this.skins[0].box.x - this.boxWidth + 10, this.wheelY);
        // game.physics.arcade.enable(this.deadSkinsPool[0].box);
        // game.physics.arcade.enable(this.deadSkinsPool[0].skin);
        this.deadSkinsPool[0].box.body.immovable = true;
        this.deadSkinsPool[0].skin.body.immovable = true;
        this.deadSkinsPool[0].box.body.velocity.x = this.speed;
        this.deadSkinsPool[0].skin.body.velocity.x = this.speed;
        this.deadSkinsPool[0].box.body.acceleration.x = this.decel;
        this.deadSkinsPool[0].skin.body.acceleration.x = this.decel;
        this.skins.unshift(this.deadSkinsPool[0]);
        this.deadSkinsPool.splice(0, 1);
    }
};

RollingWheel2.prototype.setSpeed = function (speed) {
    'use strict';
    this.speed = speed;
    console.log("speed set");
};

RollingWheel2.prototype.setDecel = function (decel, winningId) {
    'use strict';
    var i;
    this.decel = -decel;
    this.winningId = winningId;
    console.log("this.winningId in rollingWheel is: " + this.winningId);
    for (i = 0; i < this.skins.length; i += 1) {
        console.log(this.skins[i].id);
        // console.log(this.deadSkinsPool[i].id);
        this.skins[i].box.body.acceleration.x = this.decel;
        this.skins[i].skin.body.acceleration.x = this.decel;
    }

};
