/**
 * Created by Kelv on 03/06/2015.
 */
//var io = require('socket.io')(http),
//    http = require('http').Server(app);
/*globals module, require, Crate, console, game */
var request = require('request'),
    buyState = require('../buy');

module.exports = Crate = function (socket, token, assetUrl, details) {
    'use strict';

    this.socket = socket;
    this.token = token;
    this.assetUrl = assetUrl;
    this.details = details;
    // this.assetUrl = assetUrl;
    console.log('Crate object has been created');

};

Crate.prototype.enterDraw = function () {
    'use strict';
    console.log("Enterring the draw. Contents of this.details is: " + this.details);
    console.log('this.token is ' + this.token);
    // request({
    //     uri: 'http://localhost:9000/api/crates/55d4e0f7e0eec3ac26099ac0/buy',
    //     method: 'PUT',
    //     headers: {
    //         authorization: this.token
    //     },
    //     form: {
    //         'quantity': 1
    //     }
    // }, function (response) {
    //     console.log(response);
    // });

    game.state.add('buy', buyState(this.socket, this.token, this.assetUrl, this.details));
    game.state.start('buy');

};

Crate.prototype.getDetails = function () {
    'use strict';
    return this.details;
};

Crate.prototype.emit = function () {
    'use strict';

    this.socket.emit('buyIn');
};
