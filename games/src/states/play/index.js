/*globals module, require, game, Promise, console*/
var Crate = require('./crate'),
    request = require('request');
//socket = require('socket.io');
//io = require('socket.io')(http),
//http = require('http').Server(app);
//var LabelButton = require('./shared_components/LabelButton');

module.exports = function (socket, assetUrl, token) {
    'use strict';

    return {
        create: function () {
            var buildingDimensions = game.cache.getImage('play_buildings'),
                crateDimensions = game.cache.getImage('play_water'),
                firstColumn = game.world.width * 5 / 16 - crateDimensions.width / 2,
                secondColumn = game.world.width * 11 / 16 - crateDimensions.width / 2,
                firstRow = game.world.height / 4 - crateDimensions.height / 2,
                secondRow = game.world.height * 3 / 4 - crateDimensions.height,
                options,
                crateDetails = [],
                self = this,
                promiseCount = 0;

            this.gameBackground = game.add.image(0, 0, 'play_background');
            this.clouds = game.add.image(0, 130, 'play_clouds');
            this.buildings = game.add.image(0, game.world.height - buildingDimensions.height, 'play_buildings');
            this.socket = socket;
            this.token = token;
            this.assetUrl = assetUrl;
            this.players = [];

            // console.log('Play state this.token: ' + this.token);

            function testPromise() {
                var thisPromiseCount = ++promiseCount,
                    p1;
                p1 = new Promise(
                    function (resolve, reject) {
                        options = {
                            url: 'http://localhost:9000/api/crates',
                            method: 'GET'
                        };

                        request(options, function (error, response, body) {
                            // console.log(body);
                            var parsedBody = JSON.parse(body),
                                i;

                            for (i = 0; i < parsedBody.length; i++) {
                                crateDetails[i] = parsedBody[i];
                            }
                            console.log('crateDetails in request is: ' + crateDetails);
                            resolve(thisPromiseCount);
                        });
                    }
                );
                p1.then(
                        function (val) {
                            self.waterCrate = new Crate(self.socket, self.token, self.assetUrl, crateDetails[0]);
                            self.fireCrate = new Crate(self.socket, self.token, self.assetUrl, crateDetails[1]);
                            self.earthCrate = new Crate(self.socket, self.token, self.assetUrl, crateDetails[2]);
                            self.windCrate = new Crate(self.socket, self.token, self.assetUrl, crateDetails[3]);

                            var waterDetails = self.waterCrate.getDetails(),
                                fireDetails = self.fireCrate.getDetails(),
                                earthDetails = self.earthCrate.getDetails(),
                                windDetails = self.windCrate.getDetails();

                            // console.log('water details are'+waterDetails);

                            console.log('self.token is: ' + self.token);

                            self.waterBorder = game.add.image(firstColumn - 18, firstRow - 20, 'play_crateborder');
                            self.enterWater = game.add.button(firstColumn, firstRow, 'play_water', self.waterCrate.enterDraw, self.waterCrate);
                            self.waterInfo = game.add.bitmapText(0, 20, 'pixi_font', waterDetails.name + ' - ' + waterDetails._id, 32);
                            self.waterInfo.visible = false;

                            self.fireBorder = game.add.image(secondColumn - 20, firstRow - 20, 'play_crateborder');
                            self.enterFire = game.add.button(secondColumn, firstRow, 'play_water', self.fireCrate.enterDraw, self.fireCrate);
                            self.fireInfo = game.add.bitmapText(0, 20, 'pixi_font', fireDetails.name + ' - ' + fireDetails._id, 32);
                            self.fireInfo.visible = false;

                            self.earthBorder = game.add.image(firstColumn - 18, secondRow - 20, 'play_crateborder');
                            self.enterEarth = game.add.button(firstColumn, secondRow, 'play_water', self.earthCrate.enterDraw, self.earthCrate);
                            self.earthInfo = game.add.bitmapText(0, 20, 'pixi_font', earthDetails.name + ' - ' + earthDetails._id, 32);
                            self.earthInfo.visible = false;

                            self.windBorder = game.add.image(secondColumn - 18, secondRow - 20, 'play_crateborder');
                            self.enterWind = game.add.button(secondColumn, secondRow, 'play_water', self.windCrate.enterDraw, self.windCrate);
                            self.windInfo = game.add.bitmapText(0, 20, 'pixi_font', windDetails.name + ' - ' + windDetails._id, 32);
                            self.windInfo.visible = false;

                            console.log('Promise fulfilled.' + val);

                            self.enterWater.alpha = 0.7;
                            self.enterFire.alpha = 0.7;
                            self.enterEarth.alpha = 0.7;
                            self.enterWind.alpha = 0.7;

                            // console.log('self infunction is'+self);

                            self.enterWater.inputEnabled = true;
                            self.enterFire.inputEnabled = true;
                            self.enterEarth.inputEnabled = true;
                            self.enterWind.inputEnabled = true;
                        })
                    .catch(function (reason) {
                        console.log('rejected promise is: ' + reason + '. ' + self.token);

                    });
            }
            testPromise();

            // var style = {
            //         font: 'bold 20pt Arial',
            //         fill: 'white'
            //     },
            //     skins = {};

            // this.instructionText = game.add.text(20, 20, 'Click on your crate', style);
            this.instructionText.visible = true;

        },
        update: function () {
            if (this.enterWater) {
                if (this.enterWater.input.pointerOver()) {
                    this.enterWater.alpha = 1;
                    this.waterInfo.visible = true;
                } else {
                    this.enterWater.alpha = 0.7;
                    this.waterInfo.visible = false;
                }
            }

            if (this.enterEarth) {
                if (this.enterEarth.input.pointerOver()) {
                    this.enterEarth.alpha = 1;
                    this.earthInfo.visible = true;
                } else {
                    this.enterEarth.alpha = 0.7;
                    this.earthInfo.visible = false;
                }
            }
            if (this.enterFire) {
                if (this.enterFire.input.pointerOver()) {
                    this.enterFire.alpha = 1;
                    this.fireInfo.visible = true;
                } else {
                    this.enterFire.alpha = 0.7;
                    this.fireInfo.visible = false;
                }
            }
            if (this.enterWind) {
                if (this.enterWind.input.pointerOver()) {
                    this.enterWind.alpha = 1;
                    this.windInfo.visible = true;
                } else {
                    this.enterWind.alpha = 0.7;
                    this.windInfo.visible = false;
                }
            }
        },
        preload: function () {
            // game.load.image('falcon-crate', 'src/images/crate/crate/falcon/falcon-crate.jpg');
            game.load.image('alpha_crate', assetUrl + '/play/crate1.png');
            // request

            //  ROOT_DOMAIN/api/crates

            // var options = {
            //   url: 'http://localhost:9000/api/crates',
            //   method: 'GET'
            // };
            //
            // request(options, function(response) {
            //   console.log('The response from GET is: ' + response);
            // });

        },
        enterDraw: function (playerName, numberOfPPs) {
            var player = {
                name: playerName,
                points: numberOfPPs
            };

            if (this.players.length < 20) {
                this.players.push(player);
            } else {
                console.log('20 player limit already reached');
            }
        },
        selectWinner: function () {}
    };
};
