/*globals module, game */
module.exports = {
    //We track the offset of each button
    pos: [-50, 50, 150],
    //We track which callback each button has
    callbacks: ['playState', 'playState', 'playState'],
    draw: function () {
        'use strict';

        //We now create our buttons using a constructor function, YAY!
        this.button1 = this.addButton(1, this.playState);
        // this.button1.inputEnabled = true;
        this.button1.anchor.setTo(0.5, 0.5);
        // this.button1.events.onInputUp.add(displaySomething, this);

        this.button2 = this.addButton(2, this.playState);
        this.button2.anchor.setTo(0.5, 0.5);

        this.button3 = this.addButton(3, this.playState);
        this.button3.anchor.setTo(0.5, 0.5);
    },

    addButton: function (button, func) {
        'use strict';

        return game.add.button(game.world.centerX,
            game.world.centerY + this.pos[button - 1],
            'menu_button' + button, func);
    },

    playState: function () {
        'use strict';

        game.state.start('play');
    }

};