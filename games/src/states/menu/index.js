/*globals module, require, game */
var buttons = require('./menu_buttons'),
  arrow = require('./menu_arrow');

module.exports = function() {
  'use strict';

  return {
    create: function() {

      this.cursors = game.input.keyboard.createCursorKeys();
      this.gameTitle = game.add.image(game.world.centerX, game.world.centerY - 200, 'menu_title');
      this.gameTitle.anchor.setTo(0.5, 0.5);
      this.gameBackground = game.add.image(0, 0, 'menu_background');
      this.bmpText = game.add.bitmapText(10, 10, 'ugly_font', 'Testing testing', 32);
      buttons.draw();
      arrow.draw();
    },

    update: function() {
      arrow.move(this.cursors, buttons);
    }
  };
};
