/*globals module, game, Phaser */
module.exports = {
    draw: function () {
        'use strict';
        
        //Add it with initial position at first button
        this.arrow = game.add.image(game.world.centerX - 100,
            game.world.centerY - 50, 'menu_arrow');
        this.arrow.anchor.setTo(0.5, 0.5);

        //Arrow will take 200ms to go up/down the menu
        this.arrow.moveDelay = 200;

        //Control if the arrow should keep moving or not
        this.arrow.canMove = true;

        //Keep track of the current button the pointer is at
        this.arrow.currentButton = 1;

        //We add an horizontal tween so that the arrow feels nicer
        game.add.tween(this.arrow)
            .to({
                x: this.arrow.x - 10
            }, 700, Phaser.Easing.Quadratic.Out)
            .to({
                x: this.arrow.x
            }, 400, Phaser.Easing.Quadratic.In)
            .loop()
            .start();
    },

    //Here we will set the rules for how it moves
    //We need to pass the variable holding the cursor keys
    //and the object that holds the buttons.
    move: function (cursors, buttons) {
        'use strict';
        
        if (cursors.down.isDown && this.arrow.canMove) {
            //This stops the arrow from traveling way too fast
            this.arrow.canMove = false;

            //Which is reset to true after a 255ms delay
            this.allowMovement();

            if (this.arrow.currentButton === 1) {
                //I made a custom tween function for this
                this.tween(buttons, 2);
            } else if (this.arrow.currentButton === 2) {
                this.tween(buttons, 3);
            } else {
                this.tween(buttons, 1);
            }
        }

        if (cursors.up.isDown && this.arrow.canMove) {
            this.arrow.canMove = false;
            this.allowMovement();
            if (this.arrow.currentButton === 1) {
                this.tween(buttons, 3);
            } else if (this.arrow.currentButton === 2) {
                this.tween(buttons, 1);
            } else {
                this.tween(buttons, 2);
            }
        }

        if (game.input.keyboard.isDown(Phaser.Keyboard.ENTER)) {
            //This will activate the button that the pointer is at
            this.activateButton(buttons, this.arrow.currentButton);
        }
    },

    tween: function (buttons, buttonNum) {
        'use strict';
        
        game.add.tween(this.arrow)
            .to({
                y: game.world.centerY + buttons.pos[buttonNum - 1]
            }, this.arrow.moveDelay, Phaser.Easing.Quadratic.In)
            .start();
        this.arrow.currentButton = buttonNum;
    },

    allowMovement: function () {
        'use strict';
        
        game.time.events.add(255, function () {
            this.arrow.canMove = true;
        }, this);
    },

    activateButton: function (buttons, currentButton) {
        'use strict';
        
        buttons[buttons.callbacks[currentButton - 1]]();
    }
};