/*globals module, game */
module.exports = function () {
    'use strict';

    return {
        init: function () {
            // add here your scaling options
        },

        preload: function () {},

        create: function () {
            game.state.start('load');
        }
    };
};