module JackpotGame.Prefabs {
	export class WinnerFactory {
		private avatarLoader: Phaser.Loader;
		private game: Phaser.Game;
		private avatarSpriteLoader: AvatarSpriteLoader;
		private group: Phaser.Group;

		constructor(game: Phaser.Game, avatarSpriteLoader: AvatarSpriteLoader, group: Phaser.Group) {
			this.game = game;
            this.avatarLoader = new Phaser.Loader(game);
			this.avatarSpriteLoader = avatarSpriteLoader;
			this.group = group;
		}

		newInstance(key: string, callback: (winner: Winner) => void) {
			this.avatarSpriteLoader.loadAvatar(key, () => {
				var winner = new Winner(this.game, 100, 100, key);
				this.group.add(winner);
				callback(winner);
			});
		}
	}
}