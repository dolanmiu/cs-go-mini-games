module JackpotGame.Models {
    export class JackpotEntry {
        public user: any;
        public item: any;
        public assetId: String;
        public inJackpot: Boolean;
        public bot: any;
        public crate: any;
    }
}