/*globals require, module */

module.exports = function (grunt) {
    'use strict';

    require('load-grunt-tasks')(grunt);

    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),

        //jshint: {
        //    options: {
        //        reporter: require('jshint-stylish')
        //    },
        //    build: ['gruntfile.js', 'src/**/*.js']
        //},

        open: {
            dev: {
                path: 'http://localhost:9001/index.html'
                    //app: 'Google Chrome'
            },
            file: {
                path: '/etc/hosts'
            },
            custom: {
                path: function () {
                    return grunt.option('path');
                }
            }
        },

        watch: {
            express: {
                files: ['**/*.js'],
                tasks: ['express:dev'],
                options: {
                    spawn: false
                }
            },
            build: {
                files: ['**/*'],
                tasks: ['build'],
                options: {
                    spawn: false,
                    event: ['all']
                }
            }
        },

        browserify: {
            standalone: {
                //src: ['<%= pkg.name %>.js'],
                src: ['./src/main.js'],
                dest: './browser/dist/standalone.js',
                options: {
                    browserifyOptions: {
                        standalone: '<%= pkg.name %>'
                    }
                }
            },
            build: {
                src: ['./src/main.js'],
                dest: '../website/client/components/crate/game/standalone.js',
                options: {
                    browserifyOptions: {
                        standalone: '<%= pkg.name %>'
                    }
                }
            }
        },

        express: {
            options: {
                // Override defaults here
            },
            dev: {
                options: {
                    script: './src/server/index.js'
                }
            },
            prod: {
                options: {
                    script: 'path/to/prod/server.js',
                    node_env: 'production'
                }
            },
            test: {
                options: {
                    script: 'path/to/test/server.js'
                }
            }
        },

        serve: {
            options: {
                port: 9001
            }
        }
    });

    // Define tasks
    grunt.registerTask('default', [
        'serve',
        'open:dev'
    ]);

    grunt.registerTask('localhost', [
        'browserify:standalone',
        'open:dev',
        'serve'
    ]);

    grunt.registerTask('server', [
        'express:dev',
        'watch:express'
    ]);

    grunt.registerTask('build', [
        'browserify:build'
    ]);

    grunt.registerTask('watch', [
        'watch:build'
    ]);
};