var CrateGame;
(function (CrateGame) {
    var Game = (function () {
        function Game(width, height) {
            this.renderer = new THREE.WebGLRenderer({ alpha: true });
            this.renderer.setSize(width, height);
            this.renderer.setClearColor(0xFFFFFF, 1);
            this.scene = new THREE.Scene();
            this.camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 20000);
            this.clock = new THREE.Clock();
            this.airCrateFactory = new CrateGame.AirCrateFactory(new THREE.ObjectLoader(), new THREE.TextureLoader());
            this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
        }
        Game.prototype.run = function (container, loadedCallback) {
            var _this = this;
            document.getElementById(container).appendChild(this.renderer.domElement);
            this.camera.position.set(0, 600, 0);
            this.scene.add(this.camera);
            this.camera.lookAt(new THREE.Vector3(0, 0, 0));
            this.airCrateFactory.create("assets/crate/air/lid.png", function (crate) {
                _this.crate = crate;
                _this.scene.add(crate.obj);
            });
            this.windowResizeHandler();
            var light = new THREE.PointLight(0xffffff);
            light.position.set(-100, 200, 100);
            this.scene.add(light);
            this.render();
        };
        Game.prototype.windowResizeHandler = function () {
            var _this = this;
            window.addEventListener('resize', function () {
                var newWidth = window.innerWidth;
                var newHeight = window.innerHeight;
                _this.renderer.setSize(newWidth, newHeight);
                _this.camera.aspect = newWidth / newHeight;
                _this.camera.updateProjectionMatrix();
            });
        };
        Game.prototype.render = function () {
            var _this = this;
            requestAnimationFrame(function () { return _this.render(); });
            this.renderer.render(this.scene, this.camera);
            this.controls.update();
            if (this.crate) {
                this.crate.update(this.clock.getDelta());
            }
        };
        return Game;
    })();
    CrateGame.Game = Game;
})(CrateGame || (CrateGame = {}));
var CrateGame;
(function (CrateGame) {
    var AirCrateFactory = (function () {
        function AirCrateFactory(modelLoader, textureLoader) {
            this.modelLoader = modelLoader;
            this.textureLoader = textureLoader;
        }
        AirCrateFactory.prototype.create = function (lidTexturePath, callback) {
            var _this = this;
            this.modelLoader.load("assets/crate/air/crate.json", function (obj) {
                var animationMixer = new THREE.AnimationMixer(obj);
                _this.textureLoader.load("assets/crate/air/container.png", function (texture) {
                    var containerMaterial = new THREE.MeshLambertMaterial({
                        map: texture
                    });
                    texture.magFilter = THREE.NearestFilter;
                    texture.minFilter = THREE.LinearMipMapLinearFilter;
                    _this.textureLoader.load(lidTexturePath, function (texture) {
                        var lidMaterial = new THREE.MeshLambertMaterial({
                            map: texture
                        });
                        texture.magFilter = THREE.NearestFilter;
                        texture.minFilter = THREE.LinearMipMapLinearFilter;
                        obj.traverse(function (child) {
                            if (child instanceof THREE.Mesh) {
                                console.log(child.name);
                                switch (child.name) {
                                    case "Container":
                                        child.material = containerMaterial;
                                        break;
                                    case "Lid":
                                        child.material = lidMaterial;
                                        break;
                                }
                            }
                        });
                        var animation = new THREE.AnimationAction(obj.animations[0]);
                        animation.clipTime = 0;
                        animationMixer.addAction(animation);
                        //animationMixer.removeAllActions();
                        callback(new CrateGame.Crate(obj, animationMixer));
                    });
                });
            });
        };
        return AirCrateFactory;
    })();
    CrateGame.AirCrateFactory = AirCrateFactory;
})(CrateGame || (CrateGame = {}));
var CrateGame;
(function (CrateGame) {
    var Crate = (function () {
        function Crate(obj, animationMixer) {
            this.animationMixer = animationMixer;
            this.obj = obj;
        }
        Crate.prototype.update = function (delta) {
            this.animationMixer.update(delta * 0.75);
            console.log(this.animationMixer.time);
        };
        return Crate;
    })();
    CrateGame.Crate = Crate;
})(CrateGame || (CrateGame = {}));
