module CrateGame {
    export class Crate {

        private animationMixer: THREE.AnimationMixer;
        public obj: THREE.Object3D;

        constructor(obj: THREE.Object3D, animationMixer: THREE.AnimationMixer) {
            this.animationMixer = animationMixer;
            this.obj = obj;

            this.animationMixer.update(0);
            this.animationMixer.actions[0].enabled = false;
            this.animationMixer.actions[0].actionTime = 0;
        }

        update(delta: number) {
            this.animationMixer.update(delta * 0.75);
        }

        open() {
            this.animationMixer.actions[0].enabled = true;
            this.animationMixer.actions[0].actionTime = 0;
        }

        reset() {
            this.animationMixer.actions[0].actionTime = 0;
            this.animationMixer.update(0);
            this.animationMixer.actions[0].enabled = false;
        }
    }
}