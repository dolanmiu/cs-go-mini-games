module CrateGame {
    export class CrateFactory {
        
        private modelLoader: THREE.ObjectLoader;
        private textureLoader: THREE.TextureLoader;

        constructor(modelLoader: THREE.ObjectLoader, textureLoader: THREE.TextureLoader) {
            this.modelLoader = modelLoader;
            this.textureLoader = textureLoader;
        }

        public create(lidTexturePath: string, callback: (obj: Crate) => void) {
            this.modelLoader.load("/assets/crate/crate.json", (obj) => {
                var animationMixer = new THREE.AnimationMixer(obj);

                this.textureLoader.load("/assets/crate/container.png", (texture) => {
                    var containerMaterial = new THREE.MeshBasicMaterial({
                        map: texture
                    });
                    //texture.magFilter = THREE.NearestFilter;
                    //texture.minFilter = THREE.LinearMipMapLinearFilter;
                    this.textureLoader.load(lidTexturePath, (texture) => {
                        var lidMaterial = new THREE.MeshBasicMaterial({
                            map: texture
                        });
                        texture.magFilter = THREE.NearestFilter;
                        texture.minFilter = THREE.LinearMipMapLinearFilter;
                        obj.traverse((child) => {
                            if (child instanceof THREE.Mesh) {
                                switch (child.name) {
                                    case "Container":
                                        child.material = containerMaterial;
                                        break;
                                    case "Lid":
                                        child.material = lidMaterial;
                                        break;
                                }
                            }
                        });

                        var animation = new THREE.AnimationAction(obj.animations[0], undefined, undefined, 1, THREE.LoopOnce);
                        animationMixer.addAction(animation);
                        //animationMixer.removeAllActions();
                        callback(new Crate(obj, animationMixer));
                    });
                });
            });
        }
    }
}