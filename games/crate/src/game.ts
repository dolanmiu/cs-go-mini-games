module CrateGame {
    export class Game {

        private renderer: THREE.WebGLRenderer;
        private scene: THREE.Scene;
        private camera: THREE.PerspectiveCamera;
        private controls: THREE.OrbitControls;
        private clock: THREE.Clock;
        private crateFactory: CrateFactory;
        private floorFactory: FloorFactory;
        private crate: Crate;
        private textureLoader: THREE.TextureLoader;
        private cameraController: CameraController;
        private prize: THREE.Object3D;
        private textFactory: TextFactory;
        private text: THREE.Sprite;
        private prizeFactory: PrizeFactory;

        constructor(width: number, height: number) {
            this.renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
            this.renderer.setSize(width, height);
            this.renderer.setClearColor(0xFFFFFF, 1);
            this.scene = new THREE.Scene();
            this.camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 20000);
            this.clock = new THREE.Clock();
            this.textureLoader = new THREE.TextureLoader();
            this.textureLoader.crossOrigin = 'anonymous';
            this.crateFactory = new CrateFactory(new THREE.ObjectLoader(), this.textureLoader);
            this.floorFactory = new FloorFactory(new THREE.ObjectLoader(), this.textureLoader);
            this.prizeFactory = new PrizeFactory(this.textureLoader);
            this.textFactory = new TextFactory();
            //this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);

            this.cameraController = new CameraController(this.camera);
        }

        run(container: string, crateName: string, loadedCallback: Function) {
            document.getElementById(container).appendChild(this.renderer.domElement);
            this.scene.add(this.camera);

            this.crateFactory.create("/assets/crate/lid/" + crateName.toLowerCase() + ".png", (crate) => {
                this.crate = crate;
                this.scene.add(crate.obj);
            });

            this.floorFactory.create((floor) => {
                this.scene.add(floor);
            });

            this.initListeners();

            var light = new THREE.DirectionalLight(0xffffff);
            light.position.set(-100, 200, 100);
            this.scene.add(light);

            this.textFactory.newInstance("hello world");

            this.render();
        }

        initListeners() {
            window.addEventListener('resize', () => {
                var newWidth = $('body').innerWidth();
                var newHeight = window.innerHeight;
                this.renderer.setSize(newWidth, newHeight);
                this.camera.aspect = newWidth / newHeight;
                this.camera.updateProjectionMatrix();
            });
        }

        render() {
            requestAnimationFrame(() => this.render());
            this.renderer.render(this.scene, this.camera);
            //this.controls.update();
            if (this.crate) {
                this.crate.update(this.clock.getDelta());
            }
            this.cameraController.update(this.clock.getDelta());
            TWEEN.update();
        }

        open(imageUrl: string, text: string) {
            this.prizeFactory.newInstance(imageUrl, prize => {
                this.prize = prize;
                this.scene.add(this.prize);
                this.text = this.textFactory.newInstance(text);
                this.text.position.z = 50;
                this.scene.add(this.text);

                var tween = new TWEEN.Tween(this.prize.position)
                    .to({ y: 150 }, 1000)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .start();

                var tweenRotation = new TWEEN.Tween(this.prize.rotation)
                    .to({ y: -0.349066 }, 8000)
                    .easing(TWEEN.Easing.Linear.None)
                    .repeat(Infinity)
                    .yoyo(true)
                    .start();

                var tweenText = new TWEEN.Tween(this.text.position)
                    .to({ y: 150 }, 1000)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .start();

                this.cameraController.dramaticPanOut();
                this.crate.open();
            });
        }

        reset() {
            this.crate.reset();
            this.cameraController.dramaticPanIn();
            this.prize.position.y = -10;
            this.scene.remove(this.prize);
            this.scene.remove(this.text);
        }

        destroy() {
            this.renderer.forceContextLoss();
        }
    }
}