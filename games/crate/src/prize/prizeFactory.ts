module CrateGame {
    export class PrizeFactory {

        private textureLoader: THREE.TextureLoader;

        constructor(textureLoader: THREE.TextureLoader) {
            this.textureLoader = textureLoader;
        }

        newInstance(imageUrl: string, callback: (obj: THREE.Mesh) => void) {
            var imageTexture = this.textureLoader.load(imageUrl, texture => {
                imageTexture.minFilter = THREE.LinearFilter;
                var material = new THREE.MeshBasicMaterial({ map: imageTexture, color: 0xffffff, fog: true, side: THREE.DoubleSide, transparent: true });
                var geometry = new THREE.PlaneGeometry(150, 84, 1);
                var prize = new THREE.Mesh(geometry, material);
                prize.position.set(0, -10, 0);
                prize.rotation.y = 0.349066;
                
                callback(prize);
            });
        }
    }
}