module CrateGame {
    export class CameraController {

        private camera: THREE.Camera;
        private mousePosition: THREE.Vector2;
        private smoothVector: THREE.Vector2;
        private cameraDefaultZPosition: number;

        constructor(camera: THREE.Camera) {
            this.camera = camera;
            this.mousePosition = new THREE.Vector2(0, 0);
            this.smoothVector = new THREE.Vector2(0, 0);
            this.cameraDefaultZPosition = 400;
            this.camera.position.z = this.cameraDefaultZPosition;

            window.addEventListener('mousemove', (event: MouseEvent) => {
                this.mousePosition.x = (event.clientX / window.innerWidth) * 2 - 1;
                this.mousePosition.y = - (event.clientY / window.innerHeight) * 2 + 1;
            }, false);
        }

        update(delta: number) {
            var xDiff = this.mousePosition.x - this.smoothVector.x;
            var yDiff = this.mousePosition.y - this.smoothVector.y;
            this.smoothVector.x += 0.1 * xDiff;
            this.smoothVector.y += 0.1 * yDiff;

            this.camera.position.x = 248 * -this.smoothVector.x;
            this.camera.position.y = 200 + 100 * (1 + -this.smoothVector.y);

            this.camera.lookAt(new THREE.Vector3(0, 0, 0));
        }

        dramaticPanOut() {
            var tweenOut = new TWEEN.Tween(this.camera.position)
                .to({ z: 600 }, 1000)
                .easing(TWEEN.Easing.Quadratic.Out)
                .start();
        }

        dramaticPanIn() {
            var tweenIn = new TWEEN.Tween(this.camera.position)
                .to({ z: this.cameraDefaultZPosition }, 1000)
                .easing(TWEEN.Easing.Quadratic.Out)
                .start();
        }
    }
}