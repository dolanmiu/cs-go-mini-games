module CrateGame {
    export class FloorFactory {
        private modelLoader: THREE.ObjectLoader;
        private textureLoader: THREE.TextureLoader;

        constructor(modelLoader: THREE.ObjectLoader, textureLoader: THREE.TextureLoader) {
            this.modelLoader = modelLoader;
            this.textureLoader = textureLoader;
        }

        public create(callback: (floor: THREE.Object3D) => void) {
            this.modelLoader.load("/assets/crate/floor.json", (obj) => {
                this.textureLoader.load("/assets/crate/floor-texture.png", (texture) => {
                    var floorMaterial = new THREE.MeshBasicMaterial({
                        map: texture
                    });
                    obj.traverse((child) => {
                        if (child instanceof THREE.Mesh) {
                            child.material = floorMaterial;
                        }
                    });
                    callback(obj);
                });
            });
        }
    }
}