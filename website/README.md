# CS:GO Wild
> A CS:GO spin off website for entertainment.

# Install
1. Clone this project
2. Go into the ```website``` directory
3. Install the ```npm``` and ```bower``` dependencies
4. Run an instance of ```mongodb```. A convenience ```.bat``` file is included if you are lazy.
5. Run ```grunt serve```

```js
$ npm install
$ bower install
```

```js
$ mongod
```

```js
$ grunt serve
```

# API
Please note, in all these examples, ROOT_DOMAIN is your obviously your root domain (duh).

For running on local machine, please make your ROOT_DOMAIN ```http://localhost:9000```, for again, obvious reasons.
## Crate Game
### Get a list of all Crates
```js
GET: ROOT_DOMAIN/api/crates
```

### Get a single Crate
```js
GET: ROOT_DOMAIN/api/crates/CRATE_ID
```
```CRATE_ID``` is the Crate's MongoDB [Object ID](http://docs.mongodb.org/manual/reference/object-id/). For example, ```507f191e810c19729de860ea```.

### Buy slots from Crate
```js
PUT: ROOT_DOMAIN/api/crates/CRATE_ID/buy
```
```CRATE_ID``` is obtained from getting a list of all the crates. ```quantity``` is however many slots you want. ```quantity``` is a body parameter, please pass this as a BODY PARAMETER NOT A URL PARAM. Please note, you may need to specify the user who is buying it. But it seems like it gets it automatically.

When the max slots for the Crate is reached, it will automatically deal with everything, adds an item to the winning user automatically. When that happens, a socket event is emitted. You will need to listen to this.

### Undo crate buy
For administrators only
```js
PUT: ROOT_DOMAIN/api/crates/CRATE_ID/unBuy
```

# URLS
## Crate game
The Crate game will have a special URL system where when no parameters specified, it will load all the Crates, for example:

```js
GET: ROOT_DOMAIN/game/crate
```

However, if the URL has a name flag attatched to it, it will load the game with the Crate specified. This will enable us to pass URLs with the game preloaded:

```js
GET: ROOT_DOMAIN/game/crate?name=fire
```

If a name parameter is invalid, for example ```name=nonsense_ghdfghdfg```, then it should load the game as normal, as above.
