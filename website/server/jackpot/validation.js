/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

var utility = require('./utility');
var win = require('./win');

var config = require('../config/environment');

function validateTotalValue(inventoryItems) {
    var sum = 0;

    inventoryItems.forEach(function (inventoryItem) {
        sum += inventoryItem.item.price.value;
    });

    if (config.jackpot.valueThreshold > sum) {
        return false;
    }
    return true;
}

function validateFull(jackpotEntriesLength) {
    if (jackpotEntriesLength >= config.jackpot.itemLimit) {
        return false;
    } else {
        return true;
    }
}

function validateItemExist(inventoryItemIdsLength, inventoryItems) {
    if (inventoryItems.length === inventoryItemIdsLength) {
        return true;

    }
    return false;
}

function validateIsCurrentlyRunningWin() {
    return !win.getIsSelecting();
}

function validateNotRequestedWithdrawnItem(inventoryItems) {
    var failed = false;

    inventoryItems.forEach(function (inventoryItem) {
        if (inventoryItem.requestedWithdraw) {
            failed = true;
        }
    });

    if (failed) {
        return false;
    } else {
        return true;
    }
}

function validateAddedEnoughItems(jackpotEntries, inventoryItems, userId) {
    var filteredItems = _.filter(jackpotEntries, function (i) {
        return String(i.user) === String(userId);
    });
    
    return filteredItems.length + inventoryItems.length < 10;
}

exports.validate = function (inventoryItemIdsLength, inventoryItems, jackpotEntries, userId) {
    if (!validateItemExist(inventoryItemIdsLength, inventoryItems)) {
        return new Error('Item passing into pot is invalid or does not exist');
    }

    if (!validateTotalValue(inventoryItems)) {
        return new Error('The items must have value greater than: $' + config.jackpot.valueThreshold);
    }

    if (!validateFull(jackpotEntries.length)) {
        return new Error('Pot is full! Try again next round');
    }

    if (!validateIsCurrentlyRunningWin()) {
        return new Error('Currently selecting winner');
    }

    if (!validateNotRequestedWithdrawnItem(inventoryItems)) {
        return new Error('Cannot deposit an item which is being withdrawn');
    }

    if (!validateAddedEnoughItems(jackpotEntries, inventoryItems, userId)) {
        return new Error('Put too many items');
    }

    return true;
};