/*jslint node: true, nomen: true */
/*globals Promise */
'use strict';

var _ = require('lodash');
var Q = require('q');

var socketio;

var InventoryItem = require('../api/models').InventoryItem;
var utility = require('./utility');
var win = require('./win');
var validation = require('./validation');
var timer = require('./timer');

var config = require('../config/environment');

function handleError(reject) {
    return function (err) {
        return reject(err);
    };
}

function moveItemToJackpot(inventoryItemIds) {
    return new Promise(function (resolve, reject) {
        InventoryItem.find({
            '_id': {
                $in: inventoryItemIds
            }
        }).then(function (inventoryItems) {
            var promises = [];

            inventoryItems.forEach(function (inventoryItem) {
                inventoryItem.inJackpot = true;
                promises.push(inventoryItem.save());
            });

            Promise.all(promises).then(function (inventoryItems) {
                resolve(inventoryItems);
            }, function (err) {
                reject(err);
            });
        }, function (err) {
            reject(err);
        });
    });
}

function tryWinningSequence(jackpotEntriesLength, inventoryItemIdsLength) {
    var deferred = Q.defer();

    if (jackpotEntriesLength + inventoryItemIdsLength >= config.jackpot.itemLimit) {
        console.log('Jackpot is now full, will commence winning sequence');
        timer.stop();
        win.handleWin().then(function () {
            return deferred.resolve();
        }, function (err) {
            return deferred.reject(err);
        });
    } else {
        deferred.resolve();
    }
    return deferred.promise;
}

exports.addToPool = function (inventoryItemIds, userId) {
    return new Promise(function (resolve, reject) {
        InventoryItem.findInJackpot().then(function (jackpotEntries) {
            InventoryItem.find({
                '_id': {
                    $in: inventoryItemIds
                }
            }).populate('item').then(function (inventoryItems) {
                var promises = [];

                inventoryItems.forEach(function (inventoryItem) {
                    promises.push(inventoryItem.populateTradeOffer());
                });

                Promise.all(promises).then(function (inventoryItems) {
                    var validationResult = validation.validate(inventoryItemIds.length, inventoryItems, jackpotEntries, userId);

                    if (validationResult !== true) {
                        reject(validationResult);
                    } else {
                        if (utility.getNumberOfOtherPlayers(jackpotEntries, userId) > 0 && !timer.isRunning()) {
                            console.log('timer starting');
                            timer.start();
                        }

                        console.log('passed validation');

                        moveItemToJackpot(inventoryItemIds).then(function () {
                            tryWinningSequence(jackpotEntries.length, inventoryItemIds.length).then(function () {
                                console.log('succesfully finished depositing item(s)');
                                resolve();
                            }, handleError(reject));
                        }, handleError(reject));
                    }
                });
            }, handleError(reject));
        }, handleError(reject));
    });
};

exports.register = function (socket) {
    win.register(socket);
    timer.register(socket);
    socketio = socket;
};

exports.timer = timer;