/*jslint node: true, nomen: true */
'use strict';

var Q = require('q');
var Picker = require('random-picker').Picker;
var _ = require('lodash');

var User = require('../api/models').User;
var InventoryItem = require('../api/models').InventoryItem;
var logger = require('./logger');
var cutter = require('./cutter');
var utility = require('./utility');
var trade = require('../trade');

var socketio,
    isSelecting = false;

function calculateWeights(jackpotEntries) {
    var weights = [];

    jackpotEntries.forEach(function (jackpotEntry) {
        var weight = _.find(weights, function (w) {
            return String(w.user._id) === String(jackpotEntry.user._id);
        });

        if (weight) {
            weight.price += jackpotEntry.item.price.value;
        } else {
            weights.push({
                user: jackpotEntry.user,
                price: jackpotEntry.item.price.value
            });
        }
    });

    return weights;
}

function pickWinner() {
    var deferred = Q.defer();

    InventoryItem.findInJackpot().populate('item user').then(function (jackpotEntries) {
        var picker = new Picker(),
            winner,
            winnerPrice,
            weights = calculateWeights(jackpotEntries);

        weights.forEach(function (weight) {
            picker.option(weight.user, weight.price);
        });

        winner = picker.pick();
        winnerPrice = _.result(_.find(weights, {
            'user': winner
        }), 'price');

        return deferred.resolve({
            user: winner,
            chance: winnerPrice / picker.totalScore()
        });
    }, function (err) {
        deferred.reject(err);
    });

    return deferred.promise;
}

function transferItemsToWinner(userId) {
    setTimeout(function () {
        InventoryItem.findInJackpot().populate('item').then(function (jackpotEntries) {
            console.log('finally made it to get jackpot logs');
            var promises = [],
                entries = cutter.cut(jackpotEntries, userId, 0.05);

            logger.log(userId, jackpotEntries, entries.toBank);
            
            console.log('cutting soon...');
            setTimeout(function () {
                console.log('cutting now...');
                var inventoryItemIds = _.pluck(entries.toBank, '_id');
                //trade.depositToBank(inventoryItemIds);
            }, 60000);

            entries.toBank.forEach(function (jackpotEntry) {
                jackpotEntry.inJackpot = false;
                jackpotEntry.user = undefined;
                jackpotEntry.save();
            });

            entries.toUser.forEach(function (jackpotEntry) {
                jackpotEntry.inJackpot = false;
                jackpotEntry.user = userId;
                promises.push(jackpotEntry.save());
            });

            Q.all(promises).then(function (jackpotEntries) {
                isSelecting = false;
                socketio.emit('jackpotEntry:removeAll', {});
            });
        }, function (err) {
            isSelecting = false;
        });
    }, 10000);
}

exports.handleWin = function () {
    var deferred = Q.defer();

    if (isSelecting) {
        console.warn('TRYING TO RUN HANDLEWIN WHEN ALREADY HANDLING');
        return deferred.reject(new Error('TRYING TO RUN HANDLEWIN WHEN ALREADY HANDLING'));
    }
    isSelecting = true;

    pickWinner().then(function (winner) {
        console.log('Winner won: ' + winner.user.name);
        socketio.emit('jackpotEntry:winner', {
            user: {
                _id: winner.user._id,
                name: winner.user.name,
                avatar: winner.user.avatar,
                steamId: winner.user.steamId
            },
            chance: winner.chance
        });

        transferItemsToWinner(winner.user._id);
        deferred.resolve();
    }, function (err) {
        deferred.reject(new Error('TRYING TO RUN HANDLEWIN WHEN ALREADY HANDLING'));
    });

    return deferred.promise;
};

exports.register = function (socket) {
    socketio = socket;
};

exports.getIsSelecting = function () {
    return isSelecting;
};