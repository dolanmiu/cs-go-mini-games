/*jslint node: true */
'use strict';

var Stopwatch = require('timer-stopwatch');

var win = require('./win');
var config = require('../config/environment');

var stopWatchTime = config.jackpot.timeLimit;
var socketio;
var doneCallback;
var timer;

function createStopWatch() {
    timer = new Stopwatch();

    timer.on('done', function () {
        win.handleWin();
    });
}

createStopWatch();

exports.getTime = function () {
    return {
        remaining: timer.ms,
        start: stopWatchTime
    };
};

exports.start = function () {
    timer.reset(stopWatchTime);
    timer.start();
    socketio.emit('jackpotEntry:timer', {
        remaining: timer.ms,
        start: stopWatchTime
    });
};

exports.stop = function () {
    timer.stop();
    createStopWatch();
};

exports.register = function (socket) {
    socketio = socket;
};

exports.isRunning = function () {
    return timer.ms > 0;
};