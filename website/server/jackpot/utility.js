/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

exports.getItemsFromItemIds = function (inventory, itemIds) {
    return _.filter(inventory, function (item) {
        return _.includes(itemIds, String(item._id));
    });
};

exports.getIndexOfItem = function (inventory, inventoryItem) {
    return _.findIndex(inventory, function (item) {
        return String(item._id) === String(inventoryItem._id);
    });
};

exports.getNumberOfOtherPlayers = function (jackpotEntries, currentUserId) {
    var userIds = _.pluck(jackpotEntries, 'user'),
        uniqueUserIds = _.uniq(userIds, function (u) {
            return String(u);
        }),
        otherPlayers = _.filter(uniqueUserIds, function (u) {
            return String(u) !== String(currentUserId);
        });

    return _.size(otherPlayers);
};

exports.getIndexOfItemById = function (inventory, inventoryItemId) {
    return _.findIndex(inventory, function (item) {
        return String(item._id) === String(inventoryItemId);
    });
};