/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

var JackpotLog = require('../api/models').JackpotLog;

function getValueFromUserId(userId, jackpotEntries) {
    var usersEntries = _.filter(jackpotEntries, 'user', userId),
        sum = 0;

    usersEntries.forEach(function (usersEntry) {
        sum += usersEntry.item.price.value;
    });

    return sum.toFixed(2);
}

function getValueOfPot(jackpotEntries) {
    var sum = 0;

    jackpotEntries.forEach(function (entry) {
        sum += entry.item.price.value;
    });

    return sum.toFixed(2);
}

exports.log = function (userId, jackpotEntries, cutJackpotEntries) {
    var entries = _.map(jackpotEntries, function (e) {
            return {
                user: e.user,
                item: e.item,
                date: e.date
            };
        }),

        cutEntries = _.map(cutJackpotEntries, function (e) {
            return e.item;
        });

    return JackpotLog.create({
        winnerInfo: {
            user: userId,
            value: getValueFromUserId(userId, jackpotEntries)
        },
        pot: {
            entries: entries,
            value: getValueOfPot(jackpotEntries)
        },
        cut: cutEntries
    });
};

exports.getChance = function (userId, jackpotEntries) {
    return getValueFromUserId(userId, jackpotEntries) / getValueOfPot(jackpotEntries);
};