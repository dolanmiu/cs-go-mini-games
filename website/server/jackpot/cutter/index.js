/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

var logger = require('../logger');

function removeCutItems(jackpotEntries, cutValue) {
    var sortedEntries = _.sortBy(jackpotEntries, function (j) {
            return j.item.price.value;
        }).reverse(),
        i,
        bankEntries = [],
        userEntryIndexes = [],
        userEntries = [],
        currentValue = 0;

    console.log('items in pot are all sorted');

    for (i = 0; i < sortedEntries.length; i += 1) {
        if (sortedEntries[i].item.price.value < cutValue) {
            if (sortedEntries[i].item.price.value + currentValue < cutValue) {

                bankEntries.push(sortedEntries[i]);
                userEntryIndexes.push(i);

                currentValue += sortedEntries[i].item.price.value;
            }
        }
    }

    _.pullAt(sortedEntries, userEntryIndexes); // returns bankentries

    return {
        toUser: sortedEntries,
        toBank: bankEntries
    };
}

exports.cut = function (jackpotEntries, userId, cutRatio) {
    var chance = logger.getChance(userId, jackpotEntries),
        items,
        cutValue = _.sum(jackpotEntries, function (j) {
            return j.item.price.value;
        }) * 0.05;

    console.log('cutting pot at: ' + cutValue + ', with chance: ' + chance);

    if (chance >= 0.95) {
        return {
            toUser: jackpotEntries,
            toBank: []
        };
    }

    return removeCutItems(jackpotEntries, cutValue);
};