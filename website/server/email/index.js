/*jslint node: true */
'use strict';

var Q = require('q');
var nodemailer = require('nodemailer');

var transport = require('./transport');

function sendEmail(fromEmail, toEmail, subject, text) {
    var deferred = Q.defer(),
        transporter = transport.hotmail();

    console.info('sending email to: ' + toEmail);
    transporter.sendMail({
        from: fromEmail,
        to: toEmail,
        subject: subject,
        text: text
    }, function (err, info) {
        if (err) {
            console.error(err);
            return deferred.reject(err);
        }
        deferred.resolve(info);
    });

    transporter.close();
    return deferred.promise;
}

exports.sendEmailFromUs = function (toEmail, subject, text) {
    return sendEmail('noreply@opcrates.com', toEmail, subject, text);
};

exports.sendEmailToUs = function (fromEmail, subject, text, firstName, lastName) {
    text = text + '\n\nFrom: ' + fromEmail + '\nName: ' + firstName + ' ' + lastName;
    return sendEmail(fromEmail, 'info@opcrates.com', subject, text);
};