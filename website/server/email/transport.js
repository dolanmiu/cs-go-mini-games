/*jslint node: true */
'use strict';

var nodemailer = require('nodemailer');
var ses = require('nodemailer-ses-transport');
var smtpPool = require('nodemailer-smtp-pool');

exports.amazonSes = function () {
    var transporter = nodemailer.createTransport(ses({
        accessKeyId: process.env.AWS_ACCESS_KEY_ID,
        secretAccessKey: process.env.AWS_SECRET_ACCESS_KEY
    }));

    return transporter;
};

exports.gmail = function () {
    var transporter = nodemailer.createTransport({
        service: 'gmail',
        auth: {
            user: 'dolanmiu@gmail.com',
            pass: 'csgowild'
        }
    });

    return transporter;
};

exports.hotmail = function () {
    var transporter = nodemailer.createTransport({
        service: 'hotmail',
        auth: {
            user: 'csgowinbot1@hotmail.com',
            pass: 'GHwJ}k@5?qWs{CJ)'
        }
    });

    return transporter;
};

exports.pool = function () {
    var transporter = nodemailer.createTransport(smtpPool({
        service: 'gmail',
        auth: {
            user: 'username',
            pass: 'password'
        },
        maxConnections: 5,
        maxMessages: 10,
        rateLimit: 5
    }));
};