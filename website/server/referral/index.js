/*jslint node: true, nomen: true */
'use strict';

var Referral = require('../api/models').Referral;

exports.addSpent = function (user, coins) {
    Referral.findOne({
        referredUser: user
    }).then(function (referral) {
        if (!referral) {
            return;
        }

        referral.coin.available += coins;
        referral.coin.totalSpent += coins;
        referral.save();
    });
};

exports.getAvailableCoins = function (referralCount, coins) {
    var percentage = 0;

    if (referralCount >= 0 && referralCount < 50) {
        percentage = 0.33;
    }

    if (referralCount >= 50 && referralCount < 100) {
        percentage = 0.66;
    }

    if (referralCount >= 100) {
        percentage = 1;
    }
    
    return Math.round((percentage / 100) * coins);
};