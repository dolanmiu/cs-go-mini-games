/**
 * New Relic agent configuration.
 *
 * See lib/config.defaults.js in the agent distribution for a more complete
 * description of configuration variables and their potential values.
 */
exports.config = {
    /**
     * Array of application names.
     */
    app_name: ['OPCrates'],
    /**
     * Your New Relic license key.
     */
    license_key: '8b8a8c10e34854d283c8d753b31afa5e1f98b154',
    logging: {
        /**
         * Level at which to log. 'trace' is most useful to New Relic when diagnosing
         * issues with the agent, 'info' and higher will impose the least overhead on
         * production applications.
         */
        level: 'info'
    },
    rules: {
        ignore: [
            '^/socket.io-client/.*'
        ]
    }
};