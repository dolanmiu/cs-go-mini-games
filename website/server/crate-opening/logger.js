/*jslint nomen: true, node: true */
'use strict';

var Promise = require('bluebird');

var CrateGameLog = require('../api/models').CrateGameLog;

exports.writeLogToServer = function (crate, user, prize) {
    return CrateGameLog.create({
        crate: crate,
        user: user,
        prize: (function () {
            if (prize.type === 'coin') {
                return {
                    coin: prize.amount,
                    rarity: prize.rarity
                };
            }

            if (prize.type === 'item') {
                return {
                    item: prize.item,
                    rarity: prize.rarity
                };
            }
        }())
    });
};

exports.getRecentHistory = function (crateId, amount) {
    var newAmount = amount || 10;
    return new Promise(function (resolve, reject) {
        CrateGameLog.find({
            crate: crateId
        }).populate('prize.item user').sort({
            date: 'desc'
        }).then(function (logs) {
            logs.forEach(function (log) {
                if (!log.user) {
                    return;
                }
                var name = log.user.name;
                log.user.user = name;
            });
        }, function (err) {
            reject(err);
        });
    });
};