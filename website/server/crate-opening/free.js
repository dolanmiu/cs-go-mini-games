/*jslint node: true, nomen: true */
/*globals Promise */
'use strict';

var _ = require('lodash');

var CrateGameLog = require('../api/models').CrateGameLog;
var config = require('../config/environment');
var crateOpening = require('./');

exports.canOpenFreeCrate = function (user) {
    return new Promise(function (resolve, reject) {
        var crate = _.find(crateOpening.allCrates, function (c) {
            return c.name === 'free';
        });

        CrateGameLog.find({
            user: user,
            crate: crate
        }).sort('-date').limit(1).lean().then(function (logs) {
            var regex = /opcrates/ig,
                match = user.name.match(regex);

            if (logs.length === 0) {
                if (match) {
                    return resolve({
                        canOpen: true
                    });
                } else {
                    return resolve({
                        canOpen: false
                    });
                }
            }

            var log = logs[0],
                dateDiff = new Date().getTime() - log.date.getTime(),
                diff = Math.abs(dateDiff) / 3600000;

            if (diff > config.crate.free.time) {
                if (match) {
                    resolve({
                        canOpen: true,
                        time: dateDiff
                    });
                } else {
                    resolve({
                        canOpen: false,
                        time: dateDiff
                    });
                }
            } else {
                resolve({
                    canOpen: false,
                    time: dateDiff
                });
            }
            CrateGameLog.clearMemory();
        });
    });
};