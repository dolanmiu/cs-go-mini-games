/*jslint nomen: true, node: true */
'use strict';

var Promise = require('bluebird');
//var botManager = require('@dolanmiu/bot-manager')(undefined, process.env);

var User = require('../api/models').User;
var InventoryItem = require('../api/models').InventoryItem;
var referral = require('../referral');
var Crate = require('../api/models').Crate;

var socketio;

exports.allCrates = [];

function getAllCrates() {
    Crate.find({}).populate('inventory.item').then(function (crates) {
        exports.allCrates = crates;
    });
}

getAllCrates();

exports.processWin = function (prize, user, cost) {
    return new Promise(function (resolve, reject) {
        if (cost > user.balance.coin) {
            return reject(new Error('Insufficient funds'));
        }

        if (prize.type === 'coin') {
            user.balance.coin += (prize.amount - cost);
            user.save().then(function (user) {
                referral.addSpent(user, cost);
                setTimeout(function () {
                    socketio.emit('user:refresh:' + user._id, {});
                }, 10000);
                resolve(user);
            }, function (err) {
                reject(err);
            });
        }

        if (prize.type === 'item') {
            InventoryItem.findOne({
                assetId: prize.assetId
            }).then(function (inventoryItem) {
                inventoryItem.user = user;
                inventoryItem.crate = undefined;
                inventoryItem.save().then(function () {
                    resolve(user);
                }, function (err) {
                    reject(err);
                });
            });
        }
    });
};

exports.register = function (io) {
    socketio = io;
};