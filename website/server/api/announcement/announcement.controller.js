/*jslint es5: true, nomen: true, node: true  */
'use strict';

var _ = require('lodash');

var Announcement = require('../models').Announcement;
var common = require('../common');

var allAnnouncements = [];

function getAnnouncements() {
    Announcement.find({
        turnedOn: true
    }).lean().then(function (announcements) {
        allAnnouncements = announcements;
    });
}

getAnnouncements();

// Gets a list of Announcements
exports.index = function (req, res) {
    res.status(200).send(allAnnouncements);
};