/*jslint es5: true, node: true */
var express = require('express');

var controller = require('./announcement.controller');

var router = express.Router();

router.get('/', controller.index);

module.exports = router;