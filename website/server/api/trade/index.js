/*jslint es5: true, node: true */
var express = require('express');

var controller = require('./trade.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.post('/withdraw', auth.isAuthenticated(), controller.withdraw);
//router.post('/deposit/inventory', auth.isAuthenticated(), controller.depositInventory);
//router.post('/deposit/coin', auth.isAuthenticated(), controller.depositCoin);

module.exports = router;