/*jslint node: true, nomen: true */
/*globals Promise */
'use strict';

var InboxMessage = require('../models').InboxMessage;
var common = require('../common');
var trade = require('../../trade');

exports.withdraw = function (req, res, next) {
    trade.withdraw(req.body.itemIds, req.user._id)
        .then(function (tradeDetails) {
            var promises = [];
        
            tradeDetails.forEach(function (tradeDetail) {
                var promise = InboxMessage.create({
                    user: req.user._id,
                    title: 'NOTIFICATION.WITHDRAW_READY.TITLE',
                    message: '<a href="https://steamcommunity.com/tradeoffer/' + tradeDetail.tradeOfferId + '" target="_blank">Click here </a> to view the trade offer. Your security code is: ' + tradeDetail.securityCode,
                    link: 'https://steamcommunity.com/tradeoffer/' + tradeDetail.tradeOfferId
                });
                promises.push(promise);
            });
            return Promise.all(promises);
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.depositCoin = function (req, res, next) {
    trade.depositCoin(req.body.assetIds, req.user._id, req.body.items)
        .then(function (tradeDetail) {
            return InboxMessage.create({
                user: req.user._id,
                title: 'NOTIFICATION.DEPOSIT_READY.TITLE',
                message: '<a href="https://steamcommunity.com/tradeoffer/' + tradeDetail.tradeOfferId + '" target="_blank">Click here </a> to view the trade offer. Your security code is: ' + tradeDetail.securityCode,
                link: 'https://steamcommunity.com/tradeoffer/' + tradeDetail.tradeOfferId
            });
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.depositInventory = function (req, res, next) {
    trade.deposit(req.body.assetIds, req.user._id, req.body.items)
        .then(function (tradeDetail) {
            return InboxMessage.create({
                user: req.user._id,
                title: 'NOTIFICATION.DEPOSIT_READY.TITLE',
                message: '<a href="https://steamcommunity.com/tradeoffer/' + tradeDetail.tradeOfferId + '" target="_blank">Click here </a> to view the trade offer. Your security code is: ' + tradeDetail.securityCode,
                link: 'https://steamcommunity.com/tradeoffer/' + tradeDetail.tradeOfferId
            });
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};