/*jslint node: true */
'use strict';

var JackpotLog = require('../models').JackpotLog;

function onSave(socket, doc, cb) {
    JackpotLog.populate(doc, {
        path: 'winnerInfo.user pot.entries.item',
        select: 'name avatar steamId item iconUrl price'
    }, function (err, log) {
        socket.emit('jackpotLog:save', log);
    });
}

function onRemove(socket, doc, cb) {
    socket.emit('jackpotLog:remove', doc);
}

exports.register = function (socket) {
    JackpotLog.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    JackpotLog.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};