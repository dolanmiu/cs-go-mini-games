/*jslint node: true */
'use strict';

var express = require('express');

var auth = require('../../auth/auth.service');
var controller = require('./jackpotLog.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/me/balance', auth.isAuthenticated(), controller.balance);

module.exports = router;