/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

var common = require('../common');
var JackpotLog = require('../models').JackpotLog;

function handleError(res, err) {
    return res.status(500).send(err);
}

var allJackpotLogs = [];

function getJackpotLogs() {
    JackpotLog.find({}).sort('-date').limit(10).populate('winnerInfo.user', 'name avatar steamId').populate('pot.entries.item', 'item iconUrl price').then(function (jackpotLogs) {
        allJackpotLogs = jackpotLogs;
    });
}

getJackpotLogs();
setInterval(function () {
    getJackpotLogs();
}, 60000);

// Get list of jackpotLogs
exports.index = function (req, res) {
    res.status(200).send(allJackpotLogs);
};

exports.balance = function (req, res) {
    JackpotLog.find({
        'pot.entries.user': req.user._id
    }).populate('pot.entries.item').then(function (jackpotLogs) {
        var totalSpent = _.sum(jackpotLogs, function (jackpotLog) {
            return _.sum(jackpotLog.pot.entries, function (e) {
                if (!e.item || String(e.user) !== String(req.user._id)) {
                    return 0;
                }
                return e.item.price.value;
            });
        });

        JackpotLog.aggregate([{
            $match: {
                'winnerInfo.user': req.user._id
            }
        }, {
            $group: {
                _id: null,
                totalWon: {
                    $sum: '$pot.value'
                }
            }
        }], function (err, jackpotLogs) {
            res.status(200).send({
                totalWon: (function () {
                    if (jackpotLogs.length > 0) {
                        return jackpotLogs[0].totalWon.toFixed(2);
                    } else {
                        return 0;
                    }
                }()),
                totalSpent: totalSpent.toFixed(2)
            });
        });
    });
};