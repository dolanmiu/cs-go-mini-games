/*jslint node: true */
var express = require('express');

var controller = require('./loyalty-points.controller');

var router = express.Router();

router.get('/nextgiveaway', controller.getNextLpGiveaway);
router.get('/lpLeaderboard', controller.lpLeaderboard);

module.exports = router;
