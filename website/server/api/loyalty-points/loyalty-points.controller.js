/*jslint node: true, es5: true */
'use strict';

var User = require('../models').User;
var config = require('../../config/environment');
var common = require('../common');

var lpGiveawayDates = config.loyaltyPoints.giveaway.times;

function createDateFromLpGiveawayIndex(index) {
    var giveawayDate = new Date();

    giveawayDate.setHours(lpGiveawayDates[index].hours);
    giveawayDate.setMinutes(lpGiveawayDates[index].minutes);
    return giveawayDate;
}

function getNextLpGiveawayTime() {
    var currentDate = new Date(),
        i,
        currentGiveawayDate;

    for (i = 0; i < lpGiveawayDates.length; i += 1) {
        currentGiveawayDate = createDateFromLpGiveawayIndex(i);

        if (currentGiveawayDate > currentDate) {
            break;
        }

        if (i === lpGiveawayDates.length - 1) {
            currentGiveawayDate = createDateFromLpGiveawayIndex(0);
        }
    }

    return currentGiveawayDate;
}

exports.getNextLpGiveaway = function (req, res) {
    var date = getNextLpGiveawayTime();
    return res.send(200, {
        date: date,
        epoch: date.getTime()
    });
};

exports.lpLeaderboard = function (req, res) {
    User.find({}, 'name balance.loyalty avatar.small').sort('-balance.loyalty').limit(config.loyaltyPoints.leaderboard.count)
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};