/*jslint node: true, nomen: true */
/*globals Promise */
'use strict';

var request = require('request');
var _ = require('lodash');

var User = require('../models').User;
var Item = require('../models').Item;
var common = require('../common');

exports.getSteamImage = function (req, res) {
    request('https://steamcommunity-a.akamaihd.net/economy/image/' + req.params.id, function (error, response, body) {}).pipe(res);
};

exports.getSteamImageWithSize = function (req, res) {
    request('https://steamcommunity-a.akamaihd.net/economy/image/' + req.params.id + '/' + req.params.size + 'fx' + req.params.size + 'f', function (error, response, body) {}).pipe(res);
};

exports.getSteamAvatar = function (req, res) {
    User.findById(req.params.id, function (err, user) {
        request(user.avatar.large, function (error, response, body) {}).pipe(res);
    });
};


function formatSteamJson(body) {
    var result = JSON.parse(body),
        aggregate = [];

    _.forEach(result.rgInventory, function (rgInventoryItem) {
        var item = {
            assetId: rgInventoryItem.id,
            classId: rgInventoryItem.classid
        };
        _.forEach(result.rgDescriptions, function (rgDescription) {
            if (rgInventoryItem.classid === rgDescription.classid) {
                item.rgDescription = rgDescription;
            }
        });
        aggregate.push(item);
    });

    return aggregate;
}

function getItem(marketHash, assetId) {
    return new Promise(function (resolve, reject) {
        Item.findOneByName(marketHash, function (err, item) {
            if (err) {
                return reject(err);
            }
            if (!item) {
                return resolve();
            }
            resolve({
                assetId: assetId,
                item: item
            });
        });
    });
}

function getSteamIventory(steamId) {
    return new Promise(function (resolve, reject) {
        request('http://steamcommunity.com/profiles/' + steamId + '/inventory/json/730/2', function (err, response, body) {
            if (err) {
                return reject(err);
            }
            if (response.statusCode !== 200) {
                return reject(response);
            }

            var formattedJson = formatSteamJson(body),
                promises = [];

            formattedJson.forEach(function (steamItem) {
                promises.push(getItem(steamItem.rgDescription.market_hash_name, steamItem.assetId));
            });

            Promise.all(promises).then(function (result) {
                return resolve(_.compact(result));
            });
        });
    });
}

exports.steamInventory = function (req, res, next) {
    User.findById(req.user._id)
        .then(common.handleEntityNotFound(res))
        .then(function (user) {
            getSteamIventory(user.steamId).then(function (result) {
                res.status(200).send(result);
            }, common.handleError(res));
            return user;
        })
        .then(null, common.handleError(res));
};