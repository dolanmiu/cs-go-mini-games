/*jslint node: true */
var express = require('express');

var controller = require('./proxy.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/steamInventory', auth.isAuthenticated(), controller.steamInventory);

router.get('/:id/image', controller.getSteamImage);
router.get('/:id/image/:size', controller.getSteamImageWithSize);
router.get('/:id/avatar', controller.getSteamAvatar);

module.exports = router;
