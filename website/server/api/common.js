/*jslint node: true, nomen: true */
'use strict';

var _ = require('lodash');

exports.handleError = function (res, statusCode) {
    statusCode = statusCode || 500;
    return function (err) {
        console.log(err);
        res.status(statusCode).send(err);
    };
};

exports.responseWithResult = function (res, statusCode) {
    statusCode = statusCode || 200;
    return function (entity) {
        if (entity) {
            res.status(statusCode).json(entity);
        }
    };
};

exports.handleEntityNotFound = function (res) {
    return function (entity) {
        if (!entity) {
            res.status(404).end();
            return null;
        }
        return entity;
    };
};

exports.handleEntitiesNotFound = function (res) {
    return function (entity) {
        if (entity.length === 0) {
            res.status(404).end();
            return null;
        }
        return entity;
    };
};

exports.saveUpdates = function (updates) {
    return function (entity) {
        var updated = _.merge(entity, updates);
        return updated.save().then(function (updated) {
            return updated;
        });
    };
};

exports.removeEntity = function (res) {
    return function (entity) {
        if (entity) {
            return entity.remove().then(function () {
                res.status(204).end();
            });
        }
    };
};

exports.saveEntity = function (res) {
    return function (entity) {
        if (entity) {
            return entity.save().then(function () {
                return entity;
            });
        }
    };
};