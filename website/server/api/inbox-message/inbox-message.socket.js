/*jslint node: true */
'use strict';

var InboxMessage = require('../models').InboxMessage;

function onEvent(socket, doc, cb) {
    socket.emit('user:notification:' + doc.user, {});
}

exports.register = function (socket) {
    InboxMessage.schema.post('save', function (doc) {
        onEvent(socket, doc);
    });
    InboxMessage.schema.post('remove', function (doc) {
        onEvent(socket, doc);
    });
};