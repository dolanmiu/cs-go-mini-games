/*jslint nomen: true, node: true */
'use strict';

var _ = require('lodash');

var InboxMessage = require('../models').InboxMessage;
var common = require('../common');

function handleError(res, err) {
    return res.send(500, err);
}

// Get list of notifications
exports.index = function (req, res) {
    var pageNumber = req.query.page || 1,
        limit = req.query.limit || 3;

    InboxMessage.paginate({
        user: req.user._id
    }, {
        page: pageNumber,
        limit: limit,
        sort: '-date'
    }).then(function (messages) {
        if (req.query.read) {
            messages.docs.forEach(function (message) {
                message.read = true;
                message.save();
            });
        }
        return messages;
    }).then(common.responseWithResult(res)).then(null, common.handleError(res));
};

// Deletes a notification from the DB.
exports.destroy = function (req, res) {
    InboxMessage.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.removeEntity(res))
        .then(null, common.handleError(res));
};