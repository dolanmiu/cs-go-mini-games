/*jslint es5: true, node: true */
var express = require('express');

var controller = require('./inbox-message.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);

router.delete('/:id', auth.isAuthenticated(), controller.destroy);

module.exports = router;