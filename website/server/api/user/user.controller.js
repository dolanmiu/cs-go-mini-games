/*jslint nomen: true, node: true */
/*globals Promise */
'use strict';

var User = require('../models').User;
var InventoryItem = require('../models').InventoryItem;
var common = require('../common');

var validationError = function (res, err) {
    return res.status(422).send(err);
};

function handleError(res, err, statusCode) {
    var status = statusCode || 500;
    return res.status(status).send(err);
}

exports.changeEmail = function (req, res, next) {
    User.findById(req.user._id)
        .then(common.handleEntityNotFound(res))
        .then(function (user) {
            user.email = req.body.email;
            return user;
        })
        .then(common.saveUpdates(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.changeTradeUrl = function (req, res, next) {
    User.findById(req.user._id)
        .then(common.handleEntityNotFound(res))
        .then(function (user) {
            user.tradeUrl = req.body.tradeUrl;
            return user;
        })
        .then(common.saveUpdates(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.changeName = function (req, res) {
    User.findById(req.user._id)
        .then(common.handleEntityNotFound(res))
        .then(function (user) {
            if (req.body.firstName !== undefined) {
                user.firstName = req.body.firstName;
            }

            if (req.body.lastName !== undefined) {
                user.lastName = req.body.lastName;
            }
            return user;
        })
        .then(common.saveUpdates(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

/**
 * Get my info
 */
exports.me = function (req, res, next) {
    res.status(200).send(req.user);
};

exports.myInventory = function (req, res, next) {
    InventoryItem.findMyInventory(req.user._id).populate('item')
        .then(function (inventoryItems) {
            var promises = [];

            inventoryItems.forEach(function (inventoryItem) {
                promises.push(inventoryItem.populateTradeOffer());
            });

            return Promise.all(promises);
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.changeReferral = function (req, res) {
    var user = req.user,
        promise = new Promise(function (resolve, reject) {
            User.findOne({
                    'referral.code': req.body.referralCode.toLowerCase()
                })
                .then(function (existingUser) {
                    if (!existingUser) {
                        user.referral.code = req.body.referralCode;
                        user.save().then(function (user) {
                            resolve(user);
                        });
                    } else {
                        reject(null);
                    }
                });
        }).then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

/**
 * Authentication callback
 */
exports.authCallback = function (req, res, next) {
    res.redirect('/');
};