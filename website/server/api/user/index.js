/*jslint es5: true, node: true */
var express = require('express');
var controller = require('./user.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/me', auth.isAuthenticated(), controller.me);
router.get('/me/inventory', auth.isAuthenticated(), controller.myInventory);

router.put('/me/email', auth.isAuthenticated(), controller.changeEmail);
router.put('/me/tradeUrl', auth.isAuthenticated(), controller.changeTradeUrl);
router.put('/me/name', auth.isAuthenticated(), controller.changeName);
router.put('/me/referral', auth.isAuthenticated(), controller.changeReferral)
module.exports = router;