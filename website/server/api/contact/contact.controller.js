/*jslint nomen: true, node: true */
'use strict';

var _ = require('lodash');
var email = require('../../email');

function handleError(res, err) {
    return res.send(500, err);
}

exports.contact = function (req, res) {
    email.sendEmailToUs(req.body.from, req.body.subject, req.body.message, req.body.firstName, req.body.lastName).then(function (response) {
        return res.send(200, response);
    }, function (err) {
        return res.send(err.responseCode, err);
    });
};