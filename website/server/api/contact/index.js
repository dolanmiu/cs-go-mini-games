/*jslint node: true */
var express = require('express');
var controller = require('./contact.controller');

var router = express.Router();

router.post('/', controller.contact);

module.exports = router;