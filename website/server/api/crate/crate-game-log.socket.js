/*jslint node: true */
'use strict';

var CrateGameLog = require('../models').CrateGameLog;
var config = require('../../config/environment');

function onSave(socket, doc, cb) {
    setTimeout(function () {
        CrateGameLog.populate(doc, {
            path: 'user crate'
        }, function (err, crateGameLog) {
            if (crateGameLog.prize.rarity > config.crate.rarityThreshold) {
                socket.emit('crateGameLog:save', crateGameLog);
            }
        });
    }, config.crate.cooldown);
}

function onRemove(socket, doc, cb) {
    socket.emit('crateGameLog:remove', doc);
}

exports.register = function (socket) {
    CrateGameLog.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    CrateGameLog.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};