/*jslint nomen: true, node: true */
/*globals Promise */
'use strict';

var _ = require('lodash');

var Crate = require('../models').Crate;
var CrateGameLog = require('../models').CrateGameLog;
var CrateGameChat = require('../models').CrateGameChat;
var crateOpening = require('../../crate-opening');
var crateOpeningFree = require('../../crate-opening/free');
var logger = require('../../crate-opening/logger');
var common = require('../common');
var config = require('../../config/environment');
var chat = require('../../chat');

function checkIfCanOpenFree(user) {
    return crateOpeningFree.canOpenFreeCrate(user);
}

// Get list of crates
exports.index = function (req, res) {
    Crate.find({}, 'name cost').populate('inventory.item').then(function (crates) {
        var promises = [];

        crates.forEach(function (crate) {
            var promise = new Promise(function (resolve, reject) {
                logger.getRecentHistory(crate._id, req.body.historyAmount).then(function (history) {
                    crate.recentRounds = history;
                    resolve(crate);
                }, function (err) {
                    reject(err);
                });
            });
            promises.push(promise);
        });

        Promise.all(promises).then(function (resolvedCrates) {
            return res.status(200).send(resolvedCrates);
        });
    }, common.handleError(res));
};

// Get single Crate from name
exports.getFromName = function (req, res) {
    var i;

    for (i = 0; i < crateOpening.allCrates.length; i += 1) {
        if (crateOpening.allCrates[i].name.toLowerCase() === req.params.name.toLowerCase()) {
            return res.status(200).send(crateOpening.allCrates[i]);
        }
    }
    return res.status(404).send('Crate not found');
};

function openCrate(crate, user) {
    return new Promise(function (resolve, reject) {
        var prize = crate.getAndRemoveRandomPrize();

        if (!prize) {
            console.error('No prize for grabs');
            return reject(new Error('No Prize Avaliable'));
        }
        prize.chance = undefined;
        prize.maxStock = undefined;

        return crateOpening.processWin(prize, user, crate.cost).then(function () {
            console.log('processed');
            logger.writeLogToServer(crate, user, prize);

            crate.save().then(function () {
                resolve(prize);
            }, function (err) {
                reject(err);
            });
        }, function (err) {
            reject(err);
        });
    });
}

exports.openFree = function (req, res) {
    var crate = _.find(crateOpening.allCrates, function (c) {
        return c.name === 'free';
    });

    crateOpeningFree.canOpenFreeCrate(req.user).then(function (result) {
        if (result.canOpen) {
            openCrate(crate, req.user).then(function (prize) {
                res.status(200).send(prize);
            }, function (err) {
                res.status(500).send(err);
            });
        } else {
            res.status(403).send({
                message: 'You need to wait 24 hours to open another Daily Free Crate. Please try again later. Please ensure you have opcrates.com in your name.'
            });
        }
    });
};

exports.open = function (req, res) {
    var crate = _.find(crateOpening.allCrates, function (c) {
        return String(c.id) === String(req.params.id);
    });

    if (!crate) {
        return res.status(404).send('Crate not found');
    }

    openCrate(crate, req.user).then(function (prize) {
        res.status(200).send(prize);
    }, function (err) {
        res.status(500).send(err);
    });
};

// Check if can open free
exports.canOpenFree = function (req, res) {
    crateOpeningFree.canOpenFreeCrate(req.user)
        .then(function (result) {
            return {
                canOpen: result.canOpen,
                time: Math.max(0, config.crate.free.time * 3600000 - result.time)
            };
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Send crateGameChat message
exports.chat = function (req, res) {
    chat.createPayload(req.user._id, req.body.message).then(function (payload) {
        CrateGameChat.createWithLimit(payload, 50);
        res.status(200).send(payload);
    }, function (err) {
        res.status(403).send(err);
    });
};

// Get list of crateGameChats
exports.getChat = function (req, res) {
    CrateGameChat.find().sort('date').lean().populate('user', 'name avatar steamId')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

var allCrateGameLogs = [];

function getAllCrateGameLogs() {
    CrateGameLog.find({
            'prize.rarity': {
                $gt: config.crate.rarityThreshold
            }
        }).sort('-date').limit(10).lean().populate('user', 'name avatar.small').populate('crate', 'name').populate('prize.item')
        .then(function (crateGameLogs) {
            allCrateGameLogs = crateGameLogs;
        });
}
getAllCrateGameLogs();
setInterval(function () {
    getAllCrateGameLogs();
}, 5 * 60 * 1000);

exports.getNotice = function (req, res) {
    res.status(200).send(allCrateGameLogs);
};