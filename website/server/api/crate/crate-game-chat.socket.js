/*jslint node: true */
'use strict';

var CrateGameChat = require('../models').CrateGameChat;

function onSave(socket, doc, cb) {
    CrateGameChat.populate(doc, {
        path: 'user'
    }, function (err, crateGameChat) {
        socket.emit('crateGameChat:save', crateGameChat);
    });
}

function onRemove(socket, doc, cb) {
    socket.emit('crateGameChat:remove', doc);
}

exports.register = function (socket) {
    CrateGameChat.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    CrateGameChat.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};