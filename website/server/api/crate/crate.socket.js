/*jslint nomen: true, node: true */
'use strict';

//var Crate = require('./crate.model');
var Crate = require('../models').Crate;

var logger = require('../../crate-opening/logger');

function onSave(socket, doc, cb) {
    Crate.populate(doc, {
        path: 'inventory.item'
    }, function (err, crate) {
        logger.getRecentHistory(crate._id, 3).then(function (history) {
            crate.recentRounds = history;
            socket.emit('crate:save', crate);
        }, function (err) {
            console.error(err);
        });
    });
}

function onRemove(socket, doc, cb) {
    socket.emit('crate:remove', doc);
}

exports.register = function (socket) {
    Crate.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    Crate.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });

    socket.on('connection', function (newSocket) {
        console.log('a user connected: ' + newSocket.id);

        //socket.on('disconnect', socketEvents.disconnect);
        newSocket.on('chat message', function (msg) {
            console.log('message: ' + msg);
            socket.emit('chat message', newSocket.id + ': ' + msg);
        });
        newSocket.on('room join', function () {
            socket.emit('room join', newSocket.id + ' has connected');
        });
        newSocket.on('typing', function () {
            socket.emit('typing', newSocket.id);
        });
        newSocket.on('not-typing', function () {
            socket.emit('not-typing', newSocket.id);
        });

    });

    socket.on('buy-in', function () {
    });
};