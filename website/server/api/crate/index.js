/*jslint es5: true, node: true */
'use strict';

var express = require('express');
var ExpressBrute = require('express-brute');
var MongoStore = require('express-brute-mongo');
var MongoClient = require('mongodb').MongoClient;

var controller = require('./crate.controller');
var auth = require('../../auth/auth.service');
var config = require('../../config/environment');

var router = express.Router();
var store;
if (process.env.NODE_ENV === 'development') {
    store = new ExpressBrute.MemoryStore(); // stores state locally, don't use this in production 
} else {
    store = new MongoStore(function (ready) {
        MongoClient.connect(config.mongo.uri, function (err, db) {
            if (err) {
                throw err;
            }
            ready(db.collection('bruteforce-store'));
        });
    });
}
var bruteforce = new ExpressBrute(store);
var crateOpenBruteforce = new ExpressBrute(store, {
    freeRetries: 0,
    proxyDepth: 1,
    minWait: 5 * 1000,
    maxWait: 5 * 1000
});

//router.get('/', controller.index);
//router.get('/canOpenFree', auth.isAuthenticated(), controller.canOpenFree);
router.get('/chat', controller.getChat);
router.get('/notice', controller.getNotice);
router.get('/:name', controller.getFromName);

//router.put('/:id/open', auth.isAuthenticated(), controller.open);
//router.put('/openFree', auth.isAuthenticated(), crateOpenBruteforce.prevent, controller.openFree);
router.post('/chat', auth.isAuthenticated(), controller.chat);

module.exports = router;