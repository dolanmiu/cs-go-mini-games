/*jslint node: true */
'use strict';

var Twitter = require('twitter');

var client = new Twitter({
    consumer_key: process.env.TWITTER_CONSUMER_KEY,
    consumer_secret: process.env.TWITTER_CONSUMER_SECRET,
    access_token_key: process.env.TWITTER_ACCESS_TOKEN_KEY,
    access_token_secret: process.env.TWITTER_ACCESS_TOKEN_SECRET
});

var twitterCache = [];

function getTwitter() {
    client.get('statuses/user_timeline', {
        'count': 3
    }, function (error, tweets, response) {
        if (error) {
            console.error(error);
        }
        twitterCache = tweets;
    });
}

getTwitter();

exports.getPosts = function (req, res) {
    res.status(200).send(twitterCache);
};