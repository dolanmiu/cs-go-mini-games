/*jslint node: true */
var express = require('express');
var controller = require('./twitter.controller');

var router = express.Router();

router.get('/', controller.getPosts);

module.exports = router;