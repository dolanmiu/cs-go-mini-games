/*jslint node: true */
var express = require('express');

var controller = require('./loginLog.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/me', auth.isAuthenticated(), controller.me);

module.exports = router;