/*jslint nomen: true, node: true */
'use strict';

var LoginLog = require('../models').LoginLog;

function handleError(res, err) {
    return res.send(500, err);
}

exports.me = function (req, res) {
    LoginLog.getLogsForUser(req.user._id, function (err, loginLog) {
        if (err) {
            return handleError(res, err);
        }
        return res.json(loginLog);
    });
};