/*jslint node: true */
'use strict';

var LoginLog = require('../models').LoginLog;

function onSave(socket, doc, cb) {
    socket.emit('loginLog:save', doc);
}

function onRemove(socket, doc, cb) {
    socket.emit('loginLog:remove', doc);
}

exports.register = function (socket) {
    LoginLog.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    LoginLog.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};

