/*jslint nomen: true, node: true */
/*globals Promise */
'use strict';

var _ = require('lodash');
var SteamApi = require('steam-api');
var player = new SteamApi.Player(process.env.STEAM_API_KEY);

var Referral = require('../models').Referral;
var User = require('../models').User;
var common = require('../common');
var referralUtility = require('../../referral');

function queryBuilder(category) {
    if (category) {
        return {
            category: category
        };
    } else {
        return {};
    }
}

function hasRedeemedFree(user) {
    return new Promise(function (resolve, reject) {
        Referral.findOne({
                referredUser: user
            })
            .then(function (referral) {
                if (referral) {
                    resolve(true);
                } else {
                    resolve(false);
                }
            }, function (err) {
                reject(err);
            });
    });
}

// Get list of referrals
exports.index = function (req, res) {
    var pageNumber = req.query.page || 1,
        limit = req.query.limit || 10;

    Referral.paginate({
            user: req.user.id
        }, {
            page: pageNumber,
            limit: limit,
            sort: '-date',
            populate: {
                path: 'referredUser',
                select: 'name'
            }
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single referral
exports.show = function (req, res) {
    Referral.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.stats = function (req, res) {
    Referral.find({
            user: req.user
        })
        .then(function (referrals) {
            var available = _.sum(referrals, 'coin.available'),
                totalSpent = _.sum(referrals, 'coin.totalSpent'),
                totalEarned = _.sum(referrals, 'coin.totalEarned'),
                spenders = _.filter(referrals, function (referral) {
                    return referral.coin.totalSpent > 0;
                });

            return new Promise(function (resolve, reject) {
                hasRedeemedFree(req.user).then(function (hasRedeemed) {
                    resolve({
                        totalSpenders: spenders.length,
                        totalSpent: totalSpent,
                        totalEarned: totalEarned,
                        available: referralUtility.getAvailableCoins(spenders.length, available),
                        redeemedFree: hasRedeemed
                    });
                });
            });
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.redeem = function (req, res) {
    hasRedeemedFree(req.user)
        .then(function (hasRedeemed) {
            return new Promise(function (resolve, reject) {
                if (hasRedeemed) {
                    return reject({
                        message: 'already claimed'
                    });
                }

                player.GetOwnedGames(req.user.steamId).done(function (result) {
                    var containsCsgo = _.filter(result, function (game) {
                        return game.appId === 730;
                    });
                    resolve(containsCsgo.length > 0);
                });
            });
        }).then(function (hasCsgo) {
            return new Promise(function (resolve, reject) {
                if (!hasCsgo) {
                    return reject({
                        message: 'you need CS:GO to claim free coins'
                    });
                }
                User.findOne({
                        'referral.code': req.body.code.toLowerCase()
                    })
                    .then(function (user) {
                        if (!user || user.id === req.user.id) {
                            return reject({
                                message: 'code does not exist'
                            });
                        }

                        Referral.create({
                            user: user,
                            referredUser: req.user.id
                        }).then(function () {
                            req.user.balance.coin += 500;
                            req.user.save().then(function (user) {
                                resolve({
                                    message: 'succesfully claimed Coins'
                                });
                            });
                        });
                    });
            });
        }).then(common.responseWithResult(res)).then(null, common.handleError(res));
};

exports.collect = function (req, res) {
    Referral.find({
            user: req.user
        }).then(function (referrals) {
            var available = _.sum(referrals, 'coin.available'),
                spenders = _.filter(referrals, function (referral) {
                    return referral.coin.totalSpent > 0;
                }),
                coinsToGain = referralUtility.getAvailableCoins(spenders.length, available),
                user = req.user,
                promises = [];

            user.balance.coin += coinsToGain;
            promises.push(user.save());
            referrals.forEach(function (referral) {
                referral.coin.totalEarned += referralUtility.getAvailableCoins(spenders.length, referral.coin.available);
                referral.coin.available = 0;
                promises.push(referral.save());
            });

            Promise.all(promises).then(function () {
                res.status(201).send();
            });
        })
        .then(null, common.handleError(res));
};