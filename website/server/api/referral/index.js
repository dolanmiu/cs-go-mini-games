/*jslint node: true */
'use strict';

var express = require('express');

var controller = require('./referral.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', auth.isAuthenticated(), controller.index);
//router.get('/stats', auth.isAuthenticated(), controller.stats);
//router.get('/:id', auth.isAuthenticated(), controller.show);

//router.post('/redeem', auth.isAuthenticated(), controller.redeem);
//router.post('/collect', auth.isAuthenticated(), controller.collect);

module.exports = router;