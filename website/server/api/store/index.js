'use strict';

var express = require('express');

var controller = require('./store.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.post('/buy', auth.isAuthenticated(), controller.buy);

module.exports = router;