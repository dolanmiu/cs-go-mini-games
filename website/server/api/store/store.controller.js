/*jslint nomen: true, node: true */
/*globals Promise */
'use strict';

var _ = require('lodash');

var InventoryItem = require('../models').InventoryItem;
var Bot = require('../models').Bot;
var User = require('../models').User;
var common = require('../common');
var trade = require('../../trade');

function createSearchQuery(searchTerm) {
    var query = {
        $or: [{
            user: {
                $exists: false
            }
        }, {
            user: null
        }],
        'metadata.name': {
            $not: /sticker/i
        },
        'metadata.price': { $gt: 1 }
    };

    if (searchTerm) {
        searchTerm = '"' + searchTerm.replace(' ', '\" \"') + '"';

        query.$text = {
            $search: searchTerm
        };
    }

    return query;
}

function createSortOptions(sortCriteria) {
    if (sortCriteria === 'price') {
        return 'metadata.price';
    }

    if (sortCriteria === '-price') {
        return '-metadata.price';
    }
}

// Get list of items in store
exports.index = function (req, res) {
    var pageNumber = req.query.page || 1,
        limit = req.query.limit || 5;

    InventoryItem.paginate(createSearchQuery(req.query.search), {
            page: pageNumber,
            limit: limit,
            sort: createSortOptions(req.query.sort),
            populate: {
                path: 'item'
            }
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));

};

exports.buy = function (req, res) {
    if (req.body.inventoryItemIds.length > 8) {
        return res.status(500).send(new Error('Items must be less than 8'));
    }

    InventoryItem.find({
        '_id': {
            $in: req.body.inventoryItemIds
        },
        user: {
            $exists: false
        }
    }).populate('item').then(function (entity) {
        if (entity.length === 0) {
            res.status(404).end();
            return null;
        }
        return entity;
    }).then(function (inventoryItems) {
        User.findById(req.user.id)
            .then(common.handleEntityNotFound(res))
            .then(function (user) {
                return new Promise(function (resolve, reject) {
                    var totalCost = 0;

                    inventoryItems.forEach(function (inventoryItem) {
                        totalCost += inventoryItem.item.price.coin;
                    });

                    if (totalCost > user.balance.coin) {
                        throw new Error('Insufficient Funds');
                    }

                    console.log('trade from bank to deposit bot successful');

                    inventoryItems.forEach(function (inventoryItem) {
                        inventoryItem.user = req.user.id;
                        inventoryItem.save();
                    });
                    user.balance.coin -= totalCost;
                    resolve(user);
                });
            })
            .then(common.saveUpdates(req.body))
            .then(common.responseWithResult(res))
            .then(null, common.handleError(res));
    }).then(null, common.handleError(res));
};