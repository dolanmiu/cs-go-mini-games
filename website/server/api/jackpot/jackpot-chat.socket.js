/*jslint node: true */
'use strict';

var JackpotChat = require('../models').JackpotChat;

function onSave(socket, doc, cb) {
    JackpotChat.populate(doc, {
        path: 'user'
    }, function (err, jackpotEntry) {
        socket.emit('jackpotChat:save', jackpotEntry);
    });
}

function onRemove(socket, doc, cb) {
    socket.emit('jackpotChat:remove', doc);
}

exports.register = function (socket) {
    JackpotChat.schema.post('save', function (doc) {
        onSave(socket, doc);
    });
    JackpotChat.schema.post('remove', function (doc) {
        onRemove(socket, doc);
    });
};