/*jslint node: true */
'use strict';

var InventoryItem = require('../models').InventoryItem;

function onSave(socket, doc, cb) {
    InventoryItem.populate(doc, {
        path: 'user item'
    }, function (err, jackpotEntry) {
        socket.emit('jackpotEntry:save', jackpotEntry);
    });
}

function onRemove(socket, doc, cb) {
    socket.emit('jackpotEntry:remove', doc);
}

exports.register = function (socket) {
    InventoryItem.schema.post('save', function (doc) {
        if (doc.inJackpot) {
            onSave(socket, doc);
        }
    });
    InventoryItem.schema.post('remove', function (doc) {
        if (doc.inJackpot) {
            onRemove(socket, doc);
        }
    });
};