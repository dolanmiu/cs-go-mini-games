/*jslint nomen: true, node: true, regexp: true */
'use strict';

var _ = require('lodash');

var common = require('../common');
var jackpot = require('../../jackpot');
var Item = require('../models').Item;
var InventoryItem = require('../models').InventoryItem;
var JackpotChat = require('../models').JackpotChat;
var config = require('../../config/environment');
var chat = require('../../chat');

var socketio;

function handleError(res, err) {
    return res.status(500).send(err);
}

exports.index = function (req, res) {
    InventoryItem.findInJackpot().select('-bot -location').populate('item', 'price iconUrl item').populate('user', 'avatar.large name steamId')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

exports.deposit = function (req, res) {
    var inventoryItemIds = req.body.inventoryItemIds,
        totalCost = 0;

    if (!inventoryItemIds) {
        return handleError(res, 'No items sent');
    }

    jackpot.addToPool(inventoryItemIds, req.user._id).then(function (response) {
        return res.status(200).send(response);
    }, function (err) {
        return handleError(res, {
            title: 'JACKPOT.ERROR.TITLE',
            //message: 'JACKPOT.ERROR.DESCRIPTION'
            message: err.message
        });
    });
};

exports.config = function (req, res) {
    return res.status(200).send({
        max: config.jackpot.itemLimit,
        timeLimit: config.jackpot.timeLimit
    });
};

exports.time = function (req, res) {
    return res.status(200).send(jackpot.timer.getTime());
};

exports.chat = function (req, res) {
    chat.createPayload(req.user._id, req.body.message).then(function (payload) {
        JackpotChat.createWithLimit(payload, 50);
        res.status(200).send(payload);
    }, function (err) {
        res.status(403).send(err);
    });
};

exports.register = function (socket) {
    socketio = socket;
};

// Get list of jackpotChats
exports.getChat = function (req, res) {
    JackpotChat.find().sort('date').lean().populate('user', 'name avatar steamId')
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};