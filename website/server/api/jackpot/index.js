/*jslint es5: true, node: true */
var express = require('express');

var controller = require('./jackpot.controller');
var auth = require('../../auth/auth.service');

var router = express.Router();

router.get('/', controller.index);
router.get('/config', controller.config);
router.get('/time', controller.time);
router.get('/chat', controller.getChat);

router.post('/deposit', auth.isAuthenticated(), controller.deposit);
router.post('/chat', auth.isAuthenticated(), controller.chat);

module.exports = router;