/*jslint node: true */
var express = require('express');
var controller = require('./app.controller');

var router = express.Router();

router.get('/version', controller.version);
router.get('/users', controller.users);

module.exports = router;