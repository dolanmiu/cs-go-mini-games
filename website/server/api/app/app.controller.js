/*jslint nomen: true, node: true */
'use strict';

var pJson = require('../../../package.json');

var socketio;

function handleError(res, err) {
    return res.status(500).send(err);
}

exports.version = function (req, res) {
    res.status(200).send({
        version: pJson.version
    });
};

exports.users = function (req, res) {
    var count = 0;
    if (socketio) {
        count = socketio.engine.clientsCount;
    }
    res.status(200).send({
        count: count
    });
};

exports.register = function (socket) {
    socketio = socket;
    console.log('REGISTERED SOCKETIO');
};