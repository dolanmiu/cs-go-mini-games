/*jslint nomen: true, node: true */
'use strict';

var News = require('../models').News;
var common = require('../common');

function queryBuilder(category) {
    if (category) {
        return {
            category: category
        };
    } else {
        return {};
    }
}

var latest = [];

function getLatest() {
    News.find({}).sort('-date').lean().limit(4)
        .then(function (entities) {
            latest = entities;
        });
}

getLatest();

// Get list of newss
exports.index = function (req, res) {
    var pageNumber = req.query.page || 1,
        limit = req.query.limit || 5;
    
    News.paginate(queryBuilder(req.query.category), { page: pageNumber, limit: limit, sort: '-date' })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get a single news
exports.show = function (req, res) {
    News.findById(req.params.id)
        .then(common.handleEntityNotFound(res))
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};

// Get latest news
exports.getLatest = function (req, res) {
    res.status(200).send(latest);
};

// Get a single news
exports.count = function (req, res) {
    News.count(queryBuilder(req.query.category))
        .then(function (count) {
            return {
                count: count
            };
        })
        .then(common.responseWithResult(res))
        .then(null, common.handleError(res));
};