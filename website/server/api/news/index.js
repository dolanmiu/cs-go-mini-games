/*jslint node: true */
'use strict';

var express = require('express');

var controller = require('./news.controller');

var router = express.Router();

router.get('/', controller.index);
router.get('/count', controller.count);
router.get('/latest', controller.getLatest);
router.get('/:id', controller.show);

module.exports = router;