'use strict';

// Use local.env.js for environment variables that grunt will set when the server starts locally.
// Use for your api keys, secrets, etc. This file should not be tracked by git.
//
// You will need to set these on the server you deploy to.

module.exports = {
  DOMAIN: 'http://localhost:9000',
  API_DOMAIN: 'http://api.localhost:9000',
  SESSION_SECRET: '',

  // Control debug level for modules using visionmedia/debug
  DEBUG: '',
  STEAM_API_KEY: '',

  TWITTER_CONSUMER_KEY: '',
  TWITTER_CONSUMER_SECRET: '',
  TWITTER_ACCESS_TOKEN_KEY: '',
  TWITTER_ACCESS_TOKEN_SECRET: '',

  DISQUS_API_SECRET: '',
  DISQUS_API_KEY: '',
  DISQUS_ACCESS_TOKEN: '',

  AWS_ACCESS_KEY_ID: '',
  AWS_SECRET_ACCESS_KEY: '',
  AWS_S3_NEWS_BUCKET: '',
  AWS_S3_SENTRYFILE_BUCKET: ''
};
