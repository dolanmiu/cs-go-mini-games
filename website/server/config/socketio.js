/**
 * Socket.io configuration
 */
/*jslint node: true */
'use strict';

var config = require('./environment');

// When the user disconnects.. perform this
function onDisconnect(socket) {}

// When the user connects.. perform this
function onConnect(socket, socketio) {
    // When the client emits 'info', this listens and executes
    socket.on('info', function (data) {
        console.info('[%s] %s', socket.address, JSON.stringify(data, null, 2));
    });

    // Insert sockets below
    require('../api/jackpotLog/jackpotLog.socket').register(socket);
    require('../api/jackpot/jackpot.socket').register(socket);
    require('../api/inbox-message/inbox-message.socket').register(socketio);
    require('../api/loginLog/loginLog.socket').register(socket);
    require('../api/news/news.socket').register(socket);
    require('../api/crate/crate.socket').register(socket);
    require('../api/jackpot/jackpot-chat.socket').register(socket);
    require('../api/crate/crate-game-chat.socket').register(socket);
    require('../api/crate/crate-game-log.socket').register(socket);
    require('../api/user/user.socket').register(socket);
    require('../api/app/app.controller').register(socketio);
    require('../api/jackpot/jackpot.controller').register(socketio);
    require('../jackpot').register(socketio);
    require('../crate-opening').register(socketio);
}

module.exports = function (socketio) {
    // socket.io (v1.x.x) is powered by debug.
    // In order to see all the debug output, set DEBUG (in server/config/local.env.js) to including the desired scope.
    //
    // ex: DEBUG: "http*,socket.io:socket"

    // We can authenticate socket.io users and access their token through socket.handshake.decoded_token
    //
    // 1. You will need to send the token in `client/components/socket/socket.service.js`
    //
    // 2. Require authentication here:
    // socketio.use(require('socketio-jwt').authorize({
    //   secret: config.secrets.session,
    //   handshake: true
    // }));

    socketio.on('connection', function (socket) {
        socket.address = socket.handshake.address !== null ? socket.handshake.address.address + ':' + socket.handshake.address.port : process.env.DOMAIN;

        socket.connectedAt = new Date();

        // Call onDisconnect.
        socket.on('disconnect', function () {
            onDisconnect(socket);
            console.info('[%s] DISCONNECTED', socket.address);
        });

        // Call onConnect.
        onConnect(socket, socketio);
        console.info('[%s] CONNECTED', socket.address);
    });
};