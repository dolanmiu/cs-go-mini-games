/*jslint node: true */

// Development specific configuration
// ==================================
module.exports = {
    // MongoDB connection options
    mongo: {
        uri: 'mongodb://localhost/csgominigames-dev'
    },

    seedDB: true,

    item: {
        priceThreshold: 0,
        coinRatio: {
            between1and5: 800,
            between5and10: 900,
            above10: 1000
        }
    },

    game: {
        crate: {
            anounceWinningItemTime: 10000,
            anounceWinnerTime: 10000
        }
    },

    loyaltyPoints: {
        leaderboard: {
            count: 50
        },
        giveaway: {
            amount: 250,
            times: [{
                hours: 0,
                minutes: 0
            }, {
                hours: 8,
                minutes: 0
            }, {
                hours: 16,
                minutes: 0
            }]
        }
    },

    jackpot: {
        itemLimit: 5,
        valueThreshold: 0,
        timeLimit: 10000
    },

    chat: {
        limit: {
            message: 2,
            time: 4
        }
    },

    crate: {
        free: {
            time: 0.017
        },
        cooldown: 10000,
        rarityThreshold: 2
    }
};