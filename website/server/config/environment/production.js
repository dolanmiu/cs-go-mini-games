/*jslint node: true */

// Production specific configuration
// =================================
module.exports = {
    // Server IP
    ip: process.env.OPENSHIFT_NODEJS_IP ||
        process.env.IP ||
        undefined,

    // Server port
    port: process.env.OPENSHIFT_NODEJS_PORT ||
        process.env.PORT ||
        8080,

    // MongoDB connection options
    mongo: {
        uri: process.env.MONGOLAB_URI ||
            process.env.MONGOHQ_URL ||
            process.env.OPENSHIFT_MONGODB_DB_URL + process.env.OPENSHIFT_APP_NAME ||
            'mongodb://localhost/csgominigames'
    },

    item: {
        priceThreshold: 0,
        coinRatio: {
            between1and5: 800,
            between5and10: 900,
            above10: 1000
        }
    },

    game: {
        crate: {
            anounceWinningItemTime: 10000,
            anounceWinnerTime: 10000
        }
    },

    loyaltyPoints: {
        leaderboard: {
            count: 50
        },
        giveaway: {
            amount: 250,
            times: [{
                hours: 0,
                minutes: 0
            }, {
                hours: 8,
                minutes: 0
            }, {
                hours: 16,
                minutes: 0
            }]
        }
    },

    jackpot: {
        itemLimit: 50,
        valueThreshold: 0,
        timeLimit: 90000
    },

    chat: {
        limit: {
            message: 2,
            time: 2
        }
    },

    crate: {
        free: {
            time: 24
        },
        cooldown: 10000,
        rarityThreshold: 2
    }
};