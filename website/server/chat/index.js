/*jslint nomen: true, node: true, regexp: true */
/*globals Promise */
'use strict';

var _ = require('lodash');

var config = require('../config/environment');
var User = require('../api/models').User;

var blacklist = [
    'csgojackpot',
    'csgoegg',
    'csgoshuffle',
    'skinarena',
    'csgobig',
    'csgopoor',
    'csgodouble',
    'csgofast',
    'csgorumble',
    'csgospeed',
    'csgofirewheel',
    'csgoscope',
    'ciwox',
    'csgo-easy',
    'csgoeasy',
    'eqraffle',
    'csgo2x',
    'csgofresh',
    'csgodark',
    'csgokiller',
    'royalcspot',
    'csgopolygon',
    'csgosick',
    'csgohouse',
    'csgored',
    'csgo360',
    'csgomassive',
    'csgohill',
    'skinrate',
    'csgocircle',
    'csg0'
];

var chatDictionary = {};

function createPayload(message, user) {
    return {
        message: message.substring(0, 85),
        user: user
    };
}

exports.createPayload = function (user, message) {
    return new Promise(function (resolve, reject) {
        User.findById(user, 'name avatar steamId').then(function (user) {
            if (!user) {
                return reject(new Error('No user found'));
            }

            var regex = /(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z][a-z\/\-]*/ig,
                match = message.match(regex),
                payload;

            if (match) {
                return reject(createPayload(message, user));
            }

            var normalisedMessage = message.replace(/[\W_]+/g, '').toLowerCase(),
                failed = false;
            blacklist.forEach(function (word) {
                if (_.includes(normalisedMessage, word)) {
                    failed = true;
                }
            });

            if (failed) {
                return reject(createPayload(message, user));
            }

            if (chatDictionary[user._id]) {
                if (new Date() > chatDictionary[user._id].timeoutDate) {
                    chatDictionary[user._id].count = 0;
                    chatDictionary[user._id].timeoutDate = new Date((new Date()).getTime() + (config.chat.limit.time * 1000));
                }
                chatDictionary[user._id].count += 1;
            } else {
                chatDictionary[user._id] = {
                    timeoutDate: new Date((new Date()).getTime() + (config.chat.limit.time * 1000)),
                    count: 1
                };
            }

            if (chatDictionary[user._id].count > config.chat.limit.message) {
                return reject({
                    error: 'CHAT.ERROR.SPAM'
                });
            }

            resolve(createPayload(message, user));
        }, function (err) {
            reject(err);
        });
    });
};