/*jslint node: true */
/*globals Promise */
'use strict';

var request = require('request');

var config = require('./config/environment');

exports.deposit = function (assetIds, userId, itemIds) {
    return new Promise(function (resolve, reject) {
        request.post({
            url: process.env.BOT_SERVER_URL + '/api/trades/deposit',
            json: true,
            body: {
                assetIds: assetIds,
                user: userId,
                items: itemIds
            }
        }, function (err, response, body) {
            if (err || response.statusCode !== 200) {
                reject(err);
            }
            resolve(body);
        });
    });
};

exports.withdraw = function (itemIds, userId) {
    return new Promise(function (resolve, reject) {
        request.post({
            url: process.env.BOT_SERVER_URL + '/api/trades/withdraw',
            json: true,
            body: {
                itemIds: itemIds,
                user: userId
            }
        }, function (err, response, body) {
            if (err || response.statusCode !== 200) {
                reject(err);
            }
            console.log(body);
            resolve(body);
        });
    });
};

exports.depositCoin = function (assetIds, userId, itemIds) {
    return new Promise(function (resolve, reject) {
        request.post({
            url: process.env.BOT_SERVER_URL + '/api/trades/depositCoin',
            json: true,
            body: {
                assetIds: assetIds,
                user: userId,
                items: itemIds
            }
        }, function (err, response, body) {
            if (err || response.statusCode !== 200) {
                reject(err);
            }
            resolve(body);
        });
    });
};