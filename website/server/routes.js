/**
 * Main application routes
 */
/*jslint node: true */
'use strict';

var passport = require('passport');
var path = require('path');
//var cors = require('cors');

var config = require('./config/environment');
var errors = require('./components/errors');

module.exports = function (app) {

    app.use(function (req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, DELETE, OPTIONS');
        res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept, Authorization");
        next();
    });
    //app.use(cors());

    // Insert routes below
    app.use('/api/jackpotLogs', require('./api/jackpotLog'));
    app.use('/api/jackpot', require('./api/jackpot'));
    app.use('/api/app', require('./api/app'));
    //app.use('/api/inboxMessages', require('./api/inbox-message'));
    app.use('/api/loginLogs', require('./api/loginLog'));
    app.use('/api/news', require('./api/news'));
    app.use('/api/crates', require('./api/crate'));
    app.use('/api/users', require('./api/user'));
    app.use('/api/contact', require('./api/contact'));
    app.use('/api/twitter', require('./api/twitter'));
    app.use('/api/proxy', require('./api/proxy'));
    app.use('/api/loyaltyPoints', require('./api/loyalty-points'));
    app.use('/api/trade', require('./api/trade'));
    app.use('/api/announcements', require('./api/announcement'));
    app.use('/api/store', require('./api/store'));
    app.use('/api/referrals', require('./api/referral'));

    app.use('/auth', require('./auth'));

    /*app.get('/account', ensureAuthenticated, function (req, res) {
        res.json(req.user);
    });*/

    // All undefined asset or api routes should return a 404
    app.route('/:url(api|auth|components|app|bower_components|assets)/*').get(errors[404]);

    // All other routes should redirect to the index.html
    if ('development' === process.env.NODE_ENV || 'test' === process.env.NODE_ENV) {
        app.route('/*').get(function (req, res) {
            res.sendFile(path.resolve(app.get('appPath') + '/index.html'));
        });
    }

    function ensureAuthenticated(req, res, next) {
        if (req.isAuthenticated()) {
            return next();
        }
        res.redirect('/');
    }
};