/*jslint nomen: true, node: true */
'use strict';

var express = require('express');
var passport = require('passport');

var auth = require('../auth.service');
var LoginLog = require('../../api/models').LoginLog;
var CrateGameLog = require('../../api/models').CrateGameLog;
var InboxMessage = require('../../api/models').InboxMessage;
var crateOpeningFree = require('../../crate-opening/free');

var router = express.Router();

function createLog(user) {
    LoginLog.create({
        user: user,
        ip: '',
        location: {
            x: 0,
            y: 0
        }
    });
}

function sendFreeCrateNotification(user) {
    crateOpeningFree.canOpenFreeCrate(user)
        .then(function (result) {
            if (result) {
                console.log('can open free crate: ' + result);

                var cutoff = new Date();

                cutoff.setDate(cutoff.getDate() - 1);

                InboxMessage.find({
                    user: user,
                    link: '/crate/free',
                    date: {
                        $lt: cutoff
                    }
                }).then(function (messages) {
                    if (messages) {
                        return;
                    }
                    InboxMessage.create({
                        user: user,
                        title: 'NOTIFICATION.FREE_CRATE.TITLE',
                        message: 'Your daily Free Crate is ready to be opened',
                        link: '/crate/free'
                    });
                });
            }
        });
}

router.get('/', passport.authenticate('steam'));

router.get('/return', function (req, res, next) {
    //return res.status(301).redirect('http://www.google.com');
    passport.authenticate('steam', function (err, user, newUser) {
        if (err) {
            return next(err);
        }
        req.logIn(user, function (err) {
            if (err) {
                return next(err);
            }

            var token = auth.signToken(user._id, user.role);
            if (process.env.NODE_ENV === 'production') {
                res.cookie('token', '"' + token + '"', {
                    domain: process.env.DOMAIN.replace('http://www', ''),
                    maxAge: 7 * 24 * 60 * 60 * 1000
                });
            } else {
                res.cookie('token', '"' + token + '"', {
                    maxAge: 7 * 24 * 60 * 60 * 1000
                });
            }

            if (newUser) {
                return res.status(301).redirect(process.env.DOMAIN);
            } else {
                createLog(user);
                sendFreeCrateNotification(user);
                return res.status(301).redirect(process.env.DOMAIN + '/');
            }
        });
    })(req, res, next);
});

passport.serializeUser(function (user, done) {
    done(null, user);
});

passport.deserializeUser(function (obj, done) {
    done(null, obj);
});

module.exports = router;