/*jslint nomen: true, node: true */
'use strict';

var passport = require('passport');
var SteamStrategy = require('passport-steam').Strategy;

var User = require('../../api/models').User;

exports.setup = function (User) {
    passport.use(new SteamStrategy({
        returnURL: process.env.API_DOMAIN + '/auth/steam/return',
        realm: process.env.API_DOMAIN + '/',
        apiKey: process.env.STEAM_API_KEY
    }, function (identifier, profile, done) {
        User.findOne({
            steamId: profile._json.steamid
        }, function (err, user) {
            if (err) {
                return done(err, null, false);
            }
            if (!user) {
                User.create({
                    name: profile._json.personaname,
                    steamId: profile._json.steamid,
                    avatar: {
                        small: profile._json.avatar,
                        large: profile._json.avatarfull
                    }
                }, function (err, newUser) {
                    if (err) {
                        return done(err, null, false);
                    }
                    return done(err, newUser, true);
                });
            } else {
                // If old user, then just update these minor details
                user.name = profile._json.personaname; //same as profile.displayName
                user.avatar = {
                    small: profile._json.avatar,
                    large: profile._json.avatarfull
                };
                user.save().then(function (user) {
                    done(err, user, false);
                });
            }
        });
    }));
};