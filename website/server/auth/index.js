/*jslint node: true */
var express = require('express');
var passport = require('passport');

var User = require('../api/models').User;

// Passport Configuration
require('./steam/passport').setup(User);

var router = express.Router();

router.use('/steam', require('./steam'));

module.exports = router;