/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('jackpot', {
        url: '/jackpot',
        templateUrl: 'app/jackpot/jackpot.html',
        controller: 'JackpotController',
        resolve: {
            $title: function ($translate) {
                return $translate('NAVIGATION.JACKPOT');
            }
        }
    });
});