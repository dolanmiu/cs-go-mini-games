/*jslint nomen: true */
/*globals angular, _, JackpotGame, $ */
angular.module('csGoMiniGamesApp').controller('JackpotController', function ($scope, $filter, $window, $timeout, socket, Auth, Jackpot, LocaleService, ItemUtilityService, JackpotLog, Notification, User, DomainService) {
    'use strict';

    $scope.user = Auth.getCurrentUser();
    $scope.itemUtilityService = ItemUtilityService;
    $scope.rightSideTab = 0;
    $scope.jackpotModel = Jackpot;

    Auth.isLoggedInAsync(function (response) {
        $scope.isLoggedIn = response;
        if (response) {
            $scope.inventory = User.geMyInventory();
        }
    });

    $scope.jackpotLogs = JackpotLog.query();
    socket.syncUpdates('jackpotLog', $scope.jackpotLogs);

    var game;

    function createGame() {
        Jackpot.query().$promise.then(function (response) {
            $scope.jackpotEntries = response;

            socket.syncUpdates('jackpotEntry', $scope.jackpotEntries, function (event, item) {
                if (event === 'deletedAll') {
                    Auth.isLoggedInAsync(function (response) {
                        if (response) {
                            User.geMyInventory().$promise.then(function (response) {
                                $scope.inventory = response;
                            });
                        }
                    });
                }
            });

            socket.socket.on('jackpotEntry:winner', function (winner) {
                game.setWinner(winner);
                game.setWaterLevel(1);
                $timeout(function () {
                    game.setWaterLevel(0, 5000);
                }, 2000);

                $scope.hideMouse = true;
            });

            socket.socket.on('jackpotEntry:timer', function (time) {
                game.setTime(time.remaining);
            });

            game = new JackpotGame.Game($('body').innerWidth(), $window.innerHeight - 50);
            Jackpot.config().$promise.then(function (config) {
                $scope.maxEntries = config.max;

                game.run('game', DomainService.apiDomain, function () {
                    game.setJackpotItemsArray($scope.jackpotEntries, $scope.maxEntries);
                    Jackpot.time().$promise.then(function (response) {
                        game.setTime(response.remaining);
                    });
                });
            });
        });
    }

    $(function () {
        $(window).focus(function () {
            Jackpot.time().$promise.then(function (response) {
                game.setTime(response.remaining);
            });
            game.setWaterLevel();
        });
    });

    createGame();

    $scope.deposit = function () {
        var selectedAssetIds = ItemUtilityService.getSelectedAssetIds($scope.inventory, '_id');

        if (selectedAssetIds.length === 0) {
            return;
        }

        $scope.depositing = true;

        Jackpot.deposit({}, {
            inventoryItemIds: selectedAssetIds
        }).$promise.then(function (reset) {
            User.geMyInventory().$promise.then(function (response) {
                $scope.inventory = response;
            }, function (err) {

            });
        }, function (err) {
            Notification.error({
                message: err.data.message || 'You cannot play Jackpot at this time',
                title: 'An error has occurred',
                delay: 3000
            });
        });
    };

    $scope.sortJackpotLogs = function (logs) {
        return _.sortBy(logs, function (n) {
            return new Date(n.date);
        }).reverse();
    };

    function calculateCurrentRoundStats() {
        var itemsFromUsers = _.groupBy($scope.jackpotEntries, function (j) {
            return j.user.name;
        });

        var counts = _.transform(_.countBy($scope.jackpotEntries, function (j) {
            return j.user.name;
        }), function (result, n, key) {
            result[key] = {
                count: n
            };
        });

        var values = _.transform(_.mapValues(itemsFromUsers, function (i) {
            return _.sum(i, function (j) {
                return j.item.price.value;
            });
        }), function (result, n, key) {
            result[key] = {
                value: n
            };
        });

        var chances = _.transform(_.mapValues(values, function (v) {
            var totalSum = _.sum($scope.jackpotEntries, function (j) {
                return j.item.price.value;
            });
            return v.value / totalSum;
        }), function (result, n, key) {
            result[key] = {
                chance: n
            };
        });

        var names = _.transform(itemsFromUsers, function (result, n, key) {
            result[key] = {
                name: key
            };
        });

        return _.sortBy(_.merge(_.merge(_.merge(counts, values), chances), names), function (j) {
            return j.chance;
        }).reverse();
    }

    $scope.$watch('jackpotEntries', function () {
        $scope.entrySummary = calculateCurrentRoundStats();
    }, true);

    $scope.$watch(function () {
        return $window.scrollY;
    }, function (scrollY) {
        if (scrollY > $window.innerHeight / 2) {
            $scope.hideMouse = true;
        }
    });

    $scope.$on('$destroy', function () {
        socket.unsyncUpdates('jackpotLog');
        socket.unsync('jackpotEntry:winner');
        socket.unsync('jackpotChat:message');
        socket.unsync('jackpotEntry:timer');
        game.destroy();
        $(window).unbind('focus');
    });

});