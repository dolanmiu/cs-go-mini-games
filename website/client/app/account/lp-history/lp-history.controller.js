/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').controller('LpHistoryController', function ($scope, $interval, LoyaltyPoints, LoginLog) {
    'use strict';
    
    $scope.loginLogs = LoginLog.get();

    var countdownTime;

    function calculateTimeFromEpoch(epoch) {
        var date = {
            seconds: Math.floor((epoch / 1000) % 60),
            minutes: Math.floor(((epoch / (1000 * 60)) % 60)),
            hours: Math.floor(((epoch / (1000 * 60 * 60)) % 24))
        };


        return date;
    }

    LoyaltyPoints.getNextGiveaway().$promise.then(function (giveaway) {
        $scope.nextGiveaway = giveaway;
        var currentDate = new Date(),
            nextGiveawayDate = new Date(giveaway.date);

        countdownTime = nextGiveawayDate - currentDate;
        
        $scope.displayCountdownTime = calculateTimeFromEpoch(countdownTime);
        $interval(function () {
            countdownTime -= 1000;
            $scope.displayCountdownTime = calculateTimeFromEpoch(countdownTime);
        }, 1000, 0);
    });
});