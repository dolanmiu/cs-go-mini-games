/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').controller('InboxController', function ($scope, $interval, InboxMessage) {
    'use strict';

    $scope.postLimit = 10;

    $scope.changePage = function (page) {
        InboxMessage.query({
            page: page,
            limit: $scope.postLimit,
            read: true
        }).$promise.then(function (response) {
            $scope.inboxMessages = response.docs;
            $scope.totalPosts = response.total;
        });
    };

    $scope.changePage(1);
});