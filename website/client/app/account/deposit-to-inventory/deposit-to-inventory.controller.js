/*jslint nomen: true, es5: true */
/*globals angular */
angular.module('csGoMiniGamesApp').controller('DepositToInventoryController', function ($scope, $translate, Auth, User, socket, Trade, Notification, ItemUtilityService, Proxy) {
    'use strict';

    Proxy.getSteamInventory({}, function (steamInventory) {
        $scope.steamInventory = ItemUtilityService.sortInventory(steamInventory);
    });

    Auth.isLoggedInAsync(function (response) {
        if (response) {
            socket.socket.on('user:refresh:' + Auth.getCurrentUser()._id, function () {
                User.getSteamInventory({}, function (steamInventory) {
                    $scope.steamInventory = ItemUtilityService.sortInventory(steamInventory);
                });
            });
        }
    });

    $scope.itemUtilityService = ItemUtilityService;

    $scope.deposit = function () {
        var selectedAssetIds = ItemUtilityService.getSelectedAssetIds($scope.steamInventory),
            selectedItemIds = ItemUtilityService.getSelectedItemIds($scope.steamInventory);
        
        $scope.depositing = true;

        Trade.depositToInventory({}, {
            assetIds: selectedAssetIds,
            items: selectedItemIds
        }).$promise.then(function (notification) {
            $translate([notification.message, notification.title]).then(function (translations) {
                Notification.success({
                    message: translations[notification.message],
                    title: translations[notification.title],
                    delay: 30000
                });
            });
            $scope.depositing = false;
        }, function (err) {
            $translate([err.data.message, err.data.title]).then(function (translations) {
                Notification.error({
                    message: translations[err.data.message] || 'You cannot deposit at this time, please make sure you select less than 10 items',
                    title: translations[err.data.title] || 'An error has occurred.',
                    delay: 30000
                });
            });
            $scope.depositing = false;
        });
    };

});