/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('account', {
            abstract: true,
            url: '/account',
            templateUrl: 'app/account/account.html',
            controller: 'AccountController',
            resolve: {
                $title: function ($translate) {
                    return $translate('NAVIGATION.ACCOUNT');
                }
            }
        })
        .state('account.profile', {
            url: '/profile',
            templateUrl: 'app/account/profile/profile.html',
            controller: 'ProfileController',
            authenticate: true
        })
        .state('account.getcoin', {
            url: '/getcoin',
            templateUrl: 'app/account/deposit/deposit.html',
            controller: 'DepositController',
            authenticate: true
        })
        .state('account.deposit', {
            url: '/deposit',
            templateUrl: 'app/account/deposit-to-inventory/deposit-to-inventory.html',
            controller: 'DepositToInventoryController',
            authenticate: true
        })
        .state('account.inventory', {
            url: '/inventory',
            templateUrl: 'app/account/withdraw/withdraw.html',
            controller: 'WithdrawController',
            authenticate: true
        })
        .state('account.balancehistory', {
            url: '/balancehistory',
            templateUrl: 'app/account/balance-history/balance-history.html',
            controller: 'BalanceHistoryController',
            authenticate: true
        })
        .state('account.lphistory', {
            url: '/lphistory',
            templateUrl: 'app/account/lp-history/lp-history.html',
            controller: 'LpHistoryController',
            authenticate: true
        })
        .state('account.inbox', {
            url: '/inbox',
            templateUrl: 'app/account/inbox/inbox.html',
            controller: 'InboxController',
            authenticate: true
        })
        .state('account.referral', {
            url: '/referral',
            templateUrl: 'app/account/referral/referral.html',
            controller: 'ReferralController',
            authenticate: true
        });
});