/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').controller('ReferralController', function ($scope, User, Referral, Auth) {
    'use strict';

    $scope.referralPage = 1;
    $scope.referralLimit = 10;

    function getStats() {
        Referral.getStats().$promise.then(function (response) {
            $scope.stats = response;
        });
    }

    $scope.changeReferralCode = function () {
        $scope.user.referral.success = undefined;
        $scope.user.referral.error = undefined;

        User.changeReferral({}, {
            referralCode: $scope.user.referral.code
        }).$promise.then(function (response) {
            $scope.user.referral.success = 'Referral Code succesfully added';
        }, function (err) {
            $scope.user.referral.error = 'Referral Code is taken. Please choose another.';
        });
    };

    $scope.changeReferralPage = function () {
        Referral.query({
            page: $scope.referralPage,
            limit: $scope.referralLimit
        }).$promise.then(function (response) {
            $scope.referrals = response;
        });
    };

    $scope.redeem = function () {
        $scope.redeem.success = undefined;
        $scope.redeem.error = undefined;
        $scope.redeem.busy = true;

        Referral.redeem({}, {
            code: $scope.redeem.code
        }).$promise.then(function (response) {
            $scope.redeem.success = response;
            $scope.redeem.busy = false;
            Auth.refreshCurrentUser();
        }, function (err) {
            $scope.redeem.busy = false;
            $scope.redeem.error = err;
        });
    };

    $scope.collect = function () {
        Referral.collect({}, {}).$promise.then(function () {
            $scope.changeReferralPage();
            getStats();
            Auth.refreshCurrentUser();
        });
    };

    $scope.changeReferralPage();
    getStats();
});