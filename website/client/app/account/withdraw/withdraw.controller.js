/*jslint nomen: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').controller('WithdrawController', function ($scope, socket, Auth, Trade, $translate, Notification, ItemUtilityService, User) {
    'use strict';

    $scope.itemUtilityService = ItemUtilityService;

    socket.socket.on('inventory:refresh:' + Auth.getCurrentUser()._id, function () {
        $scope.myInventory = User.geMyInventory();
    });

    function getUserInventory() {
        User.geMyInventory().$promise.then(function (response) {
            $scope.myInventory = response;
        });
    }

    $scope.withdrawItems = function () {
        var itemIds = ItemUtilityService.getSelectedAssetIds($scope.myInventory, '_id');

        $scope.withdrawing = true;

        Trade.withdraw({}, {
            itemIds: itemIds
        }).$promise.then(function (notifications) {
            notifications.forEach(function (notification) {
                $translate([notification.message, notification.title]).then(function (translations) {
                    Notification.success({
                        message: translations[notification.message],
                        title: translations[notification.title],
                        delay: 30000
                    });
                });
            });
            getUserInventory();
            $scope.withdrawing = false;
        }, function (err) {
            Notification.error({
                message: err.data.message || 'You cannot withdraw at this time, please make sure you select less than 10 items',
                title: err.data.title || 'An error has occurred',
                delay: 30000
            });
            $scope.withdrawing = false;
        });
    };

    $scope.$on('$destroy', function () {
        socket.unsync('inventory:refresh:' + Auth.getCurrentUser()._id);
    });
    
    getUserInventory();
});