/*jslint nomen: true, es5: true */
/*globals angular */
angular.module('csGoMiniGamesApp').controller('BalanceHistoryController', function ($scope, JackpotLog) {
    'use strict';

    $scope.balance = JackpotLog.getBalance();
});