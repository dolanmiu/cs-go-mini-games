/*jslint nomen: true, es5: true */
/*globals angular */
angular.module('csGoMiniGamesApp').controller('DepositController', function ($scope, $translate, User, Trade, Notification, ItemUtilityService, Proxy) {
    'use strict';

    Proxy.getSteamInventory({}, function (steamInventory) {
        var sortedItems = ItemUtilityService.sortInventory(steamInventory);
        sortedItems = ItemUtilityService.removeZeroCoinItems(sortedItems);
        sortedItems = ItemUtilityService.removedStickers(sortedItems);
        $scope.steamInventory = ItemUtilityService.removeBlacklistedItems(sortedItems);
    });

    $scope.itemUtilityService = ItemUtilityService;

    $scope.deposit = function () {
        var selectedAssetIds = ItemUtilityService.getSelectedAssetIds($scope.steamInventory),
            selectedItemIds = ItemUtilityService.getSelectedItemIds($scope.steamInventory);

        $scope.depositing = true;

        Trade.depositToCoin({}, {
            assetIds: selectedAssetIds,
            items: selectedItemIds
        }).$promise.then(function (notification) {
            $translate([notification.message, notification.title]).then(function (translations) {
                Notification.success({
                    message: translations[notification.message],
                    title: translations[notification.title],
                    delay: 30000
                });
            });
            $scope.depositing = false;
        }, function (err) {
            Notification.error({
                message: err.data.message || 'You cannot deposit at this time, please make sure you select less than 10 items',
                title: err.data.title || 'An error has occurred',
                delay: 30000
            });
            $scope.depositing = false;
        });
    };

});