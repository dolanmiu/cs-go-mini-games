/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').controller('AccountController', function ($scope, Auth) {
    'use strict';

    $scope.user = Auth.getCurrentUser();
    $scope.getCurrentUser = Auth.getCurrentUser;

    $scope.menu = [{
        title: 'ACCOUNT.NAVIGATION.PROFILE',
        link: 'account.profile'
    }, {
        title: 'ACCOUNT.NAVIGATION.NOTIFICATIONS',
        link: 'account.inbox'
    }, {
        title: 'ACCOUNT.NAVIGATION.GET_COIN',
        link: 'account.getcoin'
    }, {
        title: 'ACCOUNT.NAVIGATION.DEPOSIT_TO_INVENTORY',
        link: 'account.deposit'
    }, {
        title: 'ACCOUNT.NAVIGATION.INVENTORY',
        link: 'account.inventory'
    }, {
        title: 'ACCOUNT.NAVIGATION.REFERRAL',
        link: 'account.referral'    
    }/*, {
        title: 'ACCOUNT.NAVIGATION.BALANCE_HISTORY',
        link: 'account.balancehistory'
    }, {
        title: 'ACCOUNT.NAVIGATION.LP_HISTORY',
        link: 'account.lphistory'
    }*/];
});