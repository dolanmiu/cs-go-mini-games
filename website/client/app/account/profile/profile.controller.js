/*jslint nomen: true, es5: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').controller('ProfileController', function ($scope, Auth) {
    'use strict';

    $scope.errors = {};
    $scope.user = Auth.getCurrentUser();

    $scope.changeEmail = function (form) {
        if (!form.$valid) {
            return;
        }

        Auth.changeEmail($scope.user.email).then(function () {}).catch(function () {
            $scope.errors.push('E-mail already taken');
        });
    };

    $scope.changeTradeUrl = function (form) {
        if (!form.$valid) {
            return;
        }

        Auth.changeTradeUrl($scope.user.tradeUrl).then(function () {}).catch(function () {
            $scope.errors.push('TradeURL already taken');
        });
    };

    $scope.changeName = function (form) {
        if (!form.$valid) {
            return;
        }

        Auth.changeName({
            firstName: $scope.user.firstName,
            lastName: $scope.user.lastName
        }).catch(function () {
            $scope.errors.push('Name is invalid');
        });
    };

    $scope.$watch('user', function (user) {
        var completionCount = 0;
        if (user.email) {
            completionCount += 1;
        }

        if (user.tradeUrl) {
            completionCount += 1;
        }

        if (user.firstName) {
            completionCount += 1;
        }

        if (user.lastName) {
            completionCount += 1;
        }

        $scope.completionPercent = Math.floor(completionCount / 4 * 100);
    }, true);
});