/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('tos', {
        url: '/terms-of-service',
        templateUrl: 'app/tos/tos.html',
        resolve: {
            $title: function ($translate) {
                return $translate('NAVIGATION.TOS');
            }
        }
    });
});