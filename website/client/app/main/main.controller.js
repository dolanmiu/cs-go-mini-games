/*jslint nomen: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').controller('MainController', function ($scope, $http, socket, Crate, $q, $translate, SteamService) {
    'use strict';

    /*Crate.query().$promise.then(function (crates) {
        $scope.crates = crates;
        socket.syncUpdates('crate', $scope.crates);
    });*/

    $scope.options = {
        thickness: 20,
        mode: "gauge",
        total: 100
    };

    $translate('HOME.JUMBOTRON.CRATE').then(function (translation) {
        $scope.crateTranslation = translation;
    });

    $scope.pieChartData = {};

    function generateCratePieData(crate) {
        var data = [{
            label: crate.name + ' ' + $scope.crateTranslation,
            value: crate.slotsCount,
            suffix: '/' + crate.maxSlots,
            color: 'steelblue'
        }];
        return data;
    }

    $scope.$watch('crates', function (newValue) {
        if (angular.isDefined(newValue)) {
            angular.forEach($scope.crates, function (crate) {
                var pieChartData = generateCratePieData(crate);
                if (angular.isUndefined($scope.pieChartData[crate._id])) {
                    $scope.pieChartData[crate._id] = pieChartData;
                }
                $scope.pieChartData[crate._id][0].value = Math.floor(crate.slots.length / crate.maxSlots * 100);
                //_.merge($scope.pieChartData[crate._id], pieChartData);
            });
        }

    }, true);

    $scope.$on('$destroy', function () {
        socket.unsyncUpdates('crate');
    });

    $scope.redirectToSteam = function () {
        SteamService.redirectToSteamOAuth();
    };
});