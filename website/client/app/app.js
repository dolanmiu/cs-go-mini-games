/*jslint nomen: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp', [
    'csGoMiniGamesShared',
    'ngCookies',
    'ngResource',
    'ngSanitize',
    'btford.socket-io',
    'ui.router',
    'ui.bootstrap',
    'slick',
    'n3-pie-chart',
    'akoenig.deckgrid',
    'pascalprecht.translate',
    'yaru22.angular-timeago',
    'ui-notification',
    'ncy-angular-breadcrumb',
    'angulartics',
    'angulartics.google.analytics',
    'mgcrea.ngStrap',
    'angular-perfect-scrollbar',
    'luegg.directives',
    'ui.router.title',
    'ngFitText',
    'cgNotify',
    'ui.bootstrap-slider'
]);

angular.module('csGoMiniGamesApp').config(function ($stateProvider, $urlRouterProvider, $locationProvider, $httpProvider, $translateProvider) {
    'use strict';

    $urlRouterProvider.otherwise('/');

    $locationProvider.html5Mode(true);
    $httpProvider.interceptors.push('authInterceptor');
});

angular.module('csGoMiniGamesApp').config(function ($translateProvider) {
    'use strict';

    $translateProvider.useStaticFilesLoader({
        prefix: 'i18n/',
        suffix: '.json'
    });
    $translateProvider.preferredLanguage('en');
    $translateProvider.useSanitizeValueStrategy('sanitizeParameters');
});

angular.module('csGoMiniGamesApp').config(function ($breadcrumbProvider) {
    'use strict';

    $breadcrumbProvider.setOptions({
        prefixStateName: 'main',
        includeAbstract: true
    });
});

angular.module('csGoMiniGamesApp').config(function ($urlRouterProvider) {
    'use strict';

    $urlRouterProvider.rule(function ($injector, $location) {
        //what this function returns will be set as the $location.url
        var path = $location.path(),
            normalized = path.toLowerCase();
        if (path !== normalized) {
            //instead of returning a new url string, I'll just change the $location.path directly so I don't have to worry about constructing a new url string and so a new state change is not triggered
            $location.replace().path(normalized);
        }
    });
});

angular.module('csGoMiniGamesApp').factory('authInterceptor', function ($rootScope, $q, $cookieStore, $location) {
    'use strict';

    return {
        // Add authorization token to headers
        request: function (config) {
            config.headers = config.headers || {};
            if ($cookieStore.get('token')) {
                config.headers.Authorization = 'Bearer ' + $cookieStore.get('token');
            }
            return config;
        },

        // Intercept 401s and redirect you to login
        responseError: function (response) {
            if (response.status === 401) {
                $location.path('/');
                // remove any stale tokens
                $cookieStore.remove('token');
                return $q.reject(response);
            } else {
                return $q.reject(response);
            }
        }
    };
});

angular.module('csGoMiniGamesApp').run(function ($rootScope, $location, Auth, $state, notify, Announcement, socket) {
    'use strict';

    Announcement.query().$promise.then(function (announcements) {
        announcements.forEach(function (announcement) {
            notify({
                messageTemplate: announcement.message
            });
        });
    });

    notify.config({
        startTop: 60
    });

    //temporary before moving into new admin server
    if ($state.includes('admin')) {} else {
        Auth.setCurrentUserToUser();
    }

    Auth.isLoggedInAsync(function (response) {
        if (response) {
            socket.socket.on('user:refresh:' + Auth.getCurrentUser()._id, function () {
                Auth.refreshCurrentUser();
            });
        }
    });


    // Redirect to login if route requires auth and you're not logged in
    // Anauthorize if not admin
    $rootScope.$on('$stateChangeStart', function (event, next) {
        Auth.isLoggedInAsync(function (loggedIn) {
            if (next.authenticate && !loggedIn) {
                event.preventDefault();
                $state.go('main');
            }

            if (loggedIn) {
                //Auth.refreshCurrentUser();
                Auth.assignSocket(Auth.refreshCurrentUser);
            }
        });
    });
});