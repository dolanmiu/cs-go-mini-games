/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('giveaway', {
        url: '/giveaway',
        templateUrl: 'app/giveaway/giveaway.html',
        controller: 'GiveawayController',
        resolve: {
            $title: function ($translate) {
                return $translate('NAVIGATION.GIVEAWAY');
            }
        }
    });
});