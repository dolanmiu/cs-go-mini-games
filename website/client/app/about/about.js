/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('about', {
        url: '/about',
        templateUrl: 'app/about/about.html',
        controller: 'AboutController',
        resolve: {
            $title: function ($translate) {
                return $translate('NAVIGATION.ABOUT');
            }
        }
    });
});