/*globals angular */
angular.module('csGoMiniGamesApp').controller('AboutController', function ($scope) {
    'use strict';

    $scope.tiles = [{
        title: 'ABOUT.MYSTERIOUS_CRATE_OPENING.CRATES.WATER.TITLE',
        price: 'ABOUT.MYSTERIOUS_CRATE_OPENING.CRATES.WATER.PRICE',
        image: '/assets/images/about/water-crate.png'
    }, {
        title: 'ABOUT.MYSTERIOUS_CRATE_OPENING.CRATES.EARTH.TITLE',
        price: 'ABOUT.MYSTERIOUS_CRATE_OPENING.CRATES.EARTH.PRICE',
        image: '/assets/images/about/earth-crate.png'
    }, {
        title: 'ABOUT.MYSTERIOUS_CRATE_OPENING.CRATES.AIR.TITLE',
        price: 'ABOUT.MYSTERIOUS_CRATE_OPENING.CRATES.AIR.PRICE',
        image: '/assets/images/about/air-crate.png'
    }, {
        title: 'ABOUT.MYSTERIOUS_CRATE_OPENING.CRATES.FIRE.TITLE',
        price: 'ABOUT.MYSTERIOUS_CRATE_OPENING.CRATES.FIRE.PRICE',
        image: '/assets/images/about/fire-crate.png'
    }]
});