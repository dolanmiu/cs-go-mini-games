/*globals angular */
angular.module('csGoMiniGamesApp').controller('CategoryController', function ($scope, News, $location, $stateParams) {
    'use strict';
    
    $scope.disqusIdentifier = $location.path();
    $scope.postLimit = 8;

    $scope.categoryPosts = News.query({
        category: $stateParams.category,
        page: 1,
        limit: $scope.postLimit
    });

    $scope.changePage = function (page) {
        News.query({
            category: $stateParams.category,
            page: page,
            limit: $scope.postLimit
        }).$promise.then(function (response) {
            $scope.categoryPosts = response;
        });
    };
});