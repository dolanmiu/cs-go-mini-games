/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('news', {
            url: '/news',
            templateUrl: 'app/news/news.html',
            controller: 'NewsController',
            ncyBreadcrumb: {
                label: 'News'
            },
            resolve: {
                $title: function ($translate) {
                    return $translate('NAVIGATION.NEWS');
                }
            }
        })
        .state('news.article', {
            abstract: true,
            url: '/article',
            template: '<ui-view />',
            ncyBreadcrumb: {
                label: 'Article'
            }
        });

    var categories = [{
        lowercase: 'general',
        display: 'General',
        model: 'general'
    }, {
        lowercase: 'patchnotes',
        display: 'Patch Notes',
        model: 'patch notes'
    }, {
        lowercase: 'giveaways',
        display: 'Giveaways',
        model: 'giveaways'
    }];

    angular.forEach(categories, function (category) {
        $stateProvider
            .state('news.' + category.lowercase, {
                url: '/' + category.lowercase,
                templateUrl: 'app/news/category/category.html',
                controller: 'CategoryController',
                ncyBreadcrumb: {
                    label: category.display
                },
                params: {
                    category: category.model
                }
            })
            .state('news.' + category.lowercase + '.post', {
                url: '/:id',
                templateUrl: 'app/news/article/article.html',
                controller: 'ArticleController',
                ncyBreadcrumb: {
                    label: ' '
                },
                params: {
                    category: category.model
                }
            });
    });
});