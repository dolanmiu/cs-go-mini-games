/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('news.article.post', {
            url: '/:id',
            templateUrl: 'app/news/article/article.html',
            controller: 'ArticleController',
            ncyBreadcrumb: {
                label: 'Article Post'
            }
        });
});