/*globals angular */
angular.module('csGoMiniGamesApp').controller('ArticleController', function ($scope, News, $location, $stateParams) {
    'use strict';

    $scope.disqusIdentifier = $location.path();
    $scope.currentNewsId = $stateParams.id;
    
    $scope.post = News.get({
        id: $stateParams.id
    });
});