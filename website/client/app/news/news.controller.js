/*globals angular */
angular.module('csGoMiniGamesApp').controller('NewsController', function ($scope, News) {
    'use strict';

    $scope.postLimit = 8;

    $scope.changePage = function (page) {
        News.query({
            page: page,
            limit: $scope.postLimit
        }).$promise.then(function (response) {
            $scope.news = response;
        });
    };
    
    $scope.changePage(1);
});