/*globals angular */
angular.module('csGoMiniGamesApp').controller('HowToController', function ($scope, $window) {
    'use strict';

    $scope.howTos = [{
        title: 'HOW_TO.JACKPOT.HEADING',
        id: 'jackpot',
        steps: [{
            heading: 'HOW_TO.JACKPOT.STEP_1.HEADING',
            description: 'HOW_TO.JACKPOT.STEP_1.DESCRIPTION',
            image: '/assets/images/how-to/jackpot-step-one.png'
        }, {
            heading: 'HOW_TO.JACKPOT.STEP_2.HEADING',
            description: 'HOW_TO.JACKPOT.STEP_2.DESCRIPTION',
            image: '/assets/images/how-to/jackpot-step-two.png'
        }, {
            heading: 'HOW_TO.JACKPOT.STEP_3.HEADING',
            description: 'HOW_TO.JACKPOT.STEP_3.DESCRIPTION',
            image: '/assets/images/how-to/jackpot-step-three.png'
        }, {
            heading: 'HOW_TO.JACKPOT.STEP_4.HEADING',
            description: 'HOW_TO.JACKPOT.STEP_4.DESCRIPTION',
            image: '/assets/images/how-to/jackpot-step-four.png'
        }]
    }, {
        title: 'HOW_TO.CRATE.HEADING',
        id: 'crate',
        steps: [{
            heading: 'HOW_TO.CRATE.STEP_1.HEADING',
            description: 'HOW_TO.CRATE.STEP_1.DESCRIPTION',
            image: '/assets/images/how-to/crate-step-one.png'
        }, {
            heading: 'HOW_TO.CRATE.STEP_2.HEADING',
            description: 'HOW_TO.CRATE.STEP_2.DESCRIPTION',
            image: '/assets/images/how-to/crate-step-two.png'
        }, {
            heading: 'HOW_TO.CRATE.STEP_3.HEADING',
            description: 'HOW_TO.CRATE.STEP_3.DESCRIPTION',
            image: '/assets/images/how-to/crate-step-three.png'
        }, {
            heading: 'HOW_TO.CRATE.STEP_4.HEADING',
            description: 'HOW_TO.CRATE.STEP_4.DESCRIPTION',
            image: '/assets/images/how-to/crate-step-four.png'
        }]
    }];
    
    $scope.getLocationHash = function () {
        return $window.location.hash;
    };
});