/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('howto', {
        url: '/how-to',
        templateUrl: 'app/how-to/how-to.html',
        controller: 'HowToController',
                resolve: {
            $title: function ($translate) {
                return $translate('NAVIGATION.HOW_TO');
            }
        }
    });
});