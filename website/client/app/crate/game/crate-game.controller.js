/*jslint nomen: true */
/*globals angular, CrateGame, CrateSlot, $, _ */
angular.module('csGoMiniGamesApp').controller('CrateGameController', function ($scope, $stateParams, $window, Auth, Crate, Notification, socket, $timeout, $translate) {
    'use strict';

    Auth.isLoggedInAsync(function (response) {
        if (response) {
            if ($stateParams.name === 'free') {
                Crate.canOpenFree().$promise.then(function (response) {
                    $scope.crateTime = response.time;
                });
            }
        }
        $scope.loggedIn = response;
    });

    $scope.crateModel = Crate;

    $scope.notices = Crate.getNotices();
    socket.syncUpdates('crateGameLog', $scope.notices);

    function getImageFromRarity(rarity) {
        var imageUrl;
        if (rarity === 1) {
            imageUrl = "/assets/images/coin/one.png";
        } else if (rarity === 2) {
            imageUrl = "/assets/images/coin/two.png";
        } else if (rarity === 3) {
            imageUrl = "/assets/images/coin/three.png";
        } else if (rarity === 4) {
            imageUrl = "/assets/images/coin/four.png";
        } else if (rarity === 5) {
            imageUrl = "/assets/images/coin/five.png";
        } else if (rarity === 6) {
            imageUrl = "/assets/images/coin/six.png";
        }

        return imageUrl;
    }

    var game, slots;

    Crate.getFromName({
        id: $stateParams.name
    }).$promise.then(function (response) {
        $scope.crate = response;

        game = new CrateGame.Game($('body').innerWidth(), $window.innerHeight - 50);
        game.run('game', $stateParams.name);

        slots = new CrateSlot.Game(400, 160);
        slots.run('slots', function () {
            slots.setCoinData(response.coins);
        });
    });

    function crateOpenSuccess(response) {
        $scope.isCrateOpening = true;
        //if (response.type === 'coin') {}
        slots.rollSlots(response._id, function () {
            game.open(getImageFromRarity(response.rarity), '+' + response.amount + ' Coins');
            $timeout(function () {
                game.reset();
                $scope.isCrateOpening = false;
                $scope.tryingToOpen = false;
                if ($stateParams.name === 'free') {
                    $scope.crateTime = 86400000;
                }
            }, 3000);
        });
    }

    function crateOpenFailure(err) {
        console.log(err);
        $translate([err.data.message, err.data.title]).then(function (translations) {
            Notification.error({
                message: translations[err.data.message] || 'You cannot open a Crate at this time',
                title: translations[err.data.title] || 'An error has occurred.',
                delay: 30000
            });
        });
        $scope.isCrateOpening = false;
        $scope.tryingToOpen = false;
    }

    $scope.openCrate = function () {
        $scope.tryingToOpen = true;

        if ($stateParams.name === 'free') {
            Crate.openFree({}, {}).$promise.then(crateOpenSuccess, crateOpenFailure);
        } else {
            Crate.open({
                id: $scope.crate._id
            }, {}).$promise.then(crateOpenSuccess, crateOpenFailure);
        }
    };

    $scope.closeNotification = function (index) {
        $scope.notifications.splice(index, 1);
    };

    $scope.$watch(function () {
        var w = angular.element($window);
        return {
            h: w.height(),
            w: w.width()
        };
    }, function (newValue, oldValue) {
        console.log(newValue);
        if (!game) {
            return console.log('Game not loaded yet, cannot resize');
        }
    }, true);

    $scope.$on('$destroy', function () {
        socket.unsyncUpdates($scope.notices);
        slots.destroy();
        game.destroy();
    });


    $scope.sortCrateGameLogs = function (logs) {
        return _.sortBy(logs, function (n) {
            return new Date(n.date);
        }).reverse();
    };
});