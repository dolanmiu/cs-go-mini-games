/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('crate', {
            url: '/crate',
            templateUrl: 'app/crate/crate.html',
            controller: 'CrateController',
            resolve: {
                $title: function ($translate) {
                    return $translate('NAVIGATION.CRATE');
                }
            }
        })
        .state('crateGame', {
            url: '/crate/:name',
            templateUrl: 'app/crate/game/game.html',
            controller: 'CrateGameController',
            resolve: {
                $title: function ($translate) {
                    return $translate('NAVIGATION.CRATE');
                }
            }
        })

});