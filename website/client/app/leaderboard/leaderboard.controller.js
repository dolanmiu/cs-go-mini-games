/*globals angular */
angular.module('csGoMiniGamesApp').controller('LeaderboardController', function ($scope, socket, LoyaltyPoints) {
    'use strict';

    $scope.leaderboard = LoyaltyPoints.getLpLeaderboard({});
    
    $scope.menu = [{
        title: 'LEADERBOARD.NAVIGATION.MAIN',
        link: 'leaderboard'
    }, {
        title: 'LEADERBOARD.NAVIGATION.HALL_OF_FAME',
        link: 'leaderboard.hallOfFame'
    }];
    
});