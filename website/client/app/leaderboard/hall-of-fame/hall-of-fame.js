/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider
        .state('leaderboard.hallOfFame', {
            url: '/hall-of-fame',
            templateUrl: 'app/leaderboard/hall-of-fame/hall-of-fame.html',
            controller: 'HallOfFameController',
            resolve: {
                $title: function ($translate) {
                    return $translate('NAVIGATION.HALL_OF_FAME');
                }
            }
        });
});