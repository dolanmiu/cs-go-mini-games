/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('support', {
        url: '/support',
        templateUrl: 'app/support/support.html',
        controller: 'SupportController'
    });
});