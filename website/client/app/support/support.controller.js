/*globals angular */
angular.module('csGoMiniGamesApp').controller('SupportController', function ($scope) {
    'use strict';

    $scope.submitMessage = function () {
        $scope.messageSubmitted = true;  
    };
});