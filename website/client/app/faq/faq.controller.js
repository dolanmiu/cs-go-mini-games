/*globals angular */
angular.module('csGoMiniGamesApp').controller('FaqController', function ($scope) {
    'use strict';

    $scope.faqs = {
        deposit: {
            title: 'FAQ.DEPOSIT',
            tag: 'deposit'
        },
        crate: {
            title: 'FAQ.CRATE',
            tag: 'crate'
        },
        referral: {
            title: 'FAQ.REFERRAL',
            tag: 'referral'
        },
        jackpot: {
            title: 'FAQ.JACKPOT',
            tag: 'jackpot'
        }
    };
});