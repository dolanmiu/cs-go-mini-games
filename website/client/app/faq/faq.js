/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('faq', {
        url: '/faq',
        templateUrl: 'app/faq/faq.html',
        controller: 'FaqController',
        resolve: {
            $title: function ($translate) {
                return $translate('NAVIGATION.FAQ');
            }
        }
    });
});