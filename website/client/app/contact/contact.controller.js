/*jslint nomen: true */
/*globals angular */
angular.module('csGoMiniGamesApp').controller('ContactController', function ($scope, Contact, Auth) {
    'use strict';
    
    $scope.user = Auth.getCurrentUser();
    $scope.email = $scope.user.email;

    $scope.submitContactForm = function (formValid) {
        $scope.submitted = true;

        if (!formValid) {
            return;
        }

        Contact.contact({}, {
            firstName: $scope.firstName,
            lastName: $scope.lastName,
            from: $scope.email,
            subject: $scope.subject,
            message: $scope.message
        }).$promise.then(function () {
            $scope.hideContactForm = true;
        }, function (err) {
            $scope.error = err;
        });
    };
});