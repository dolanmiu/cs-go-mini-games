/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('contact', {
        url: '/contact',
        templateUrl: 'app/contact/contact.html',
        controller: 'ContactController',
        resolve: {
            $title: function ($translate) {
                return $translate('NAVIGATION.CONTACT');
            }
        }
    });

});