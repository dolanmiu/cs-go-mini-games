/*globals angular */
angular.module('csGoMiniGamesApp').config(function ($stateProvider) {
    'use strict';

    $stateProvider.state('store', {
        url: '/store',
        templateUrl: 'app/store/store.html',
        controller: 'StoreController',
        resolve: {
            $title: function ($translate) {
                return $translate('NAVIGATION.STORE');
            }
        }
    });
});