/*globals angular */
angular.module('csGoMiniGamesApp').controller('StoreController', function ($scope, Store, ItemUtilityService) {
    'use strict';

    $scope.itemsLimit = 30;
    $scope.itemUtilityService = ItemUtilityService;
    $scope.sortCriteria = '-price';
    
    $scope.changePage = function (page) {
        Store.query({
            page: page,
            limit: $scope.itemsLimit,
            sort: $scope.sortCriteria,
            search: $scope.searchCriteria
        }).$promise.then(function (response) {
            $scope.storeItems = response.docs;
            $scope.itemsTotal = response.total;
        });
    };

    $scope.buy = function () {
        $scope.buying = true;

        var inventoryItemIds = ItemUtilityService.getSelectedAssetIds($scope.storeItems, '_id');

        Store.buy({}, {
            inventoryItemIds: inventoryItemIds
        }).$promise.then(function () {
            $scope.changePage($scope.currentPage);
            $scope.buying = false;
        }, function () {
            $scope.buying = false;
        });
    };

    $scope.getItems = function () {
        $scope.changePage(1);
    };

    $scope.getItems();
});