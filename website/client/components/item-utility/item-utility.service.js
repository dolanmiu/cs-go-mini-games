/*jslint nomen: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').service('ItemUtilityService', function ($filter, LocaleService) {
    'use strict';

    var self = this;

    function getSelectedItems(steamInventory) {
        return _.filter(steamInventory, {
            'isSelected': true
        });
    }

    this.getSelectedAssetIds = function (steamInventory, identifier) {
        identifier = identifier || 'assetId';
        var itemIds = _.pluck(getSelectedItems(steamInventory), identifier);
        return itemIds;
    };

    this.getTotalCost = function (steamInventory) {
        var value = _.sum(getSelectedItems(steamInventory), function (i) {
            return i.item.price.value * LocaleService.currentLanguage.rate;
        });

        return $filter('currency')(value, LocaleService.currentLanguage.symbol);
    };

    this.getTotalCostInDollar = function (steamInventory) {
        var value = _.sum(getSelectedItems(steamInventory), function (i) {
            return i.item.price.value;
        });

        return $filter('currency')(value);
    };

    this.getTotalCostInCoins = function (steamInventory) {
        var value = _.sum(getSelectedItems(steamInventory), function (i) {
            return i.item.price.coin;
        });

        return value;
    };

    this.getCount = function (steamInventory) {
        return self.getSelectedAssetIds(steamInventory).length;
    };

    this.sortInventory = function (inventory, itemKey) {
        if (itemKey) {
            inventory = _.transform(inventory, function (result, n) {
                result.push(_.result(n, itemKey));
                return n;
            });
        }
        return _.sortBy(inventory, function (n) {
            if (!n.item) {
                return 0;
            }
            return n.item.price.value;
        }).reverse();
    };

    this.removeZeroCoinItems = function (steamInventory) {
        return _.filter(steamInventory, function (steamItem) {
            return steamItem.item.price.coin > 0;
        });
    };

    this.removedStickers = function (steamInventory) {
        return _.filter(steamInventory, function (steamItem) {
            var regex = /sticker/ig,
                match = steamItem.item.item.marketName.match(regex);
            return !match;
        });
    };

    this.getSelectedItemIds = function (steamInventory) {
        var itemIds = _.pluck(getSelectedItems(steamInventory), 'item._id');
        return itemIds;
    };

    this.removeBlacklistedItems = function (steamInventory) {
        var blacklist = [
            'AK-47 | Fuel Injector',
            'M4A4 | The Battlestar',
            'AWP | Elite Build',
            'Desert Eagle | Kumicho Dragon',
            'Nova | Hyper Beast',
            'FAMAS | Valence',
            'Five-SeveN | Triumvirate',
            'Glock-18 | Royal Legion',
            'MAG-7 | Praetorian',
            'MP7 | Impire',
            'PP-Bizon | Photic Zone',
            'Dual Berettas | Cartel',
            'MAC-10 | Lapis Gator',
            'SSG 08 | Necropos',
            'Tec-9 | Jambiya',
            'USP-S | Lead Conduit',
            'Bowie'
        ];

        return _.filter(steamInventory, function (steamItem) {
            var match = false;
            
            blacklist.forEach(function (word) {
                if (_.includes(steamItem.item.item.marketName.toLowerCase(), word.toLowerCase())) {
                    match = true;
                }
            });
            return !match;
        });
    };
});