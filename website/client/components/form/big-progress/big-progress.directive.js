/*globals angular */
angular.module('csGoMiniGamesApp').directive('bigProgress', function ($window) {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            value: '=',
            max: '='
        },
        templateUrl: 'components/form/big-progress/big-progress.html',
        link: function ($scope, $element, $attrs) {
            $scope.$watch(function () {
                return $element[0].firstChild.offsetWidth;
            }, function (value) {
                $scope.barWidth = $element[0].firstChild.offsetWidth;
            });
        }
    };
});