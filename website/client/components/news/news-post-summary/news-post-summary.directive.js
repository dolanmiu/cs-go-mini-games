/*globals angular, regexp: true */
angular.module('csGoMiniGamesApp').directive('newsPostSummary', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            post: '='
        },
        controller: function ($scope) {
        },
        templateUrl: 'components/news/news-post-summary/news-post-summary.html',
        link: function ($scope, $element, $attrs) {}
    };
});