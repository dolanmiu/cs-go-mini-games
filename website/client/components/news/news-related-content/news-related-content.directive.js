/*globals angular, regexp: true */
angular.module('csGoMiniGamesApp').directive('newsRelatedContent', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            post: '='
        },
        controller: function ($scope) {
        },
        templateUrl: 'components/news/news-related-content/news-related-content.html',
        link: function ($scope, $element, $attrs) {}
    };
});