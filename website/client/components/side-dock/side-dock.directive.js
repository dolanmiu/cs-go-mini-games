/*globals angular */
angular.module('csGoMiniGamesApp').directive('sideDock', function () {
    'use strict';

    return {
        restrict: 'E',
        transclude: true,
        scope: {
            side: '@'
        },
        templateUrl: 'components/side-dock/side-dock.html',
        link: function ($scope, $element, $attrs) {}
    };
});