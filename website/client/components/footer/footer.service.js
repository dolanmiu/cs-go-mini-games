/*jslint nomen: true */
/*globals angular */
angular.module('csGoMiniGamesApp').service('FooterService', function (Twitter, News, App) {
    'use strict';

    this.twitterPosts = Twitter.query();

    this.news = News.latest();
    
    this.version = App.version();
});