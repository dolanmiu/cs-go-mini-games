/*globals angular */
angular.module('csGoMiniGamesApp').directive('footer', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'components/footer/footer.html',
        controller: 'FooterController',
        link: function ($scope, $element, $attrs) {}
    };
});