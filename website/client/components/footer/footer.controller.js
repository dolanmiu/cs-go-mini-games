/*jslint nomen: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').controller('FooterController', function ($scope, $translate, Twitter, App, LocaleService, News, FooterService) {
    'use strict';

    $scope.twitterPosts = FooterService.twitterPosts;

    $scope.news = FooterService.news;

    $scope.getYear = function () {
        return new Date().getFullYear();
    };

    $scope.version = FooterService.version;

    $scope.siteMap = [{
        title: 'FOOTER.SITE_MAP.SECTION_1.HEADING',
        links: [{
            title: 'FOOTER.SITE_MAP.SECTION_1.ITEM_1',
            link: 'about',
            icon: 'users'
        }, {
            title: 'FOOTER.SITE_MAP.SECTION_1.ITEM_2',
            link: 'account.profile',
            icon: 'user'
        }, {
            title: 'FOOTER.SITE_MAP.SECTION_1.ITEM_4',
            link: 'contact',
            icon: 'comments'
        }, {
            title: 'FOOTER.SITE_MAP.SECTION_1.FAQ',
            link: 'faq'
        }, {
            title: 'FOOTER.SITE_MAP.SECTION_1.HOW_TO',
            link: 'howto'
        }]
    }, {
        title: 'FOOTER.SITE_MAP.SECTION_2.HEADING',
        links: [{
            title: 'FOOTER.SITE_MAP.SECTION_2.ITEM_2',
            link: 'jackpot'
        }, {
            title: 'FOOTER.SITE_MAP.SECTION_2.ITEM_1',
            link: 'game.crate'
        }]
    }, {
        title: 'FOOTER.SITE_MAP.SECTION_3.HEADING',
        links: [{
            title: 'FOOTER.SITE_MAP.SECTION_3.GIVEAWAY',
            link: 'giveaway'
        }, {
            title: 'FOOTER.SITE_MAP.SECTION_3.ITEM_1',
            link: 'news'
        }]
    }, {
        title: 'FOOTER.SITE_MAP.SECTION_4.HEADING',
        links: [{
            title: 'FOOTER.SITE_MAP.SECTION_4.ITEM_1',
            link: 'tos'
        }]
    }];

    $scope.localeService = LocaleService;
});