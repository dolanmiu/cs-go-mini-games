/*globals angular */
angular.module('csGoMiniGamesApp').service('SteamService', function ($window, $location, DomainService) {
    'use strict';

    this.redirectToSteamOAuth = function () {
        $window.location.href = DomainService.transformResource('/auth/steam');
    };
});