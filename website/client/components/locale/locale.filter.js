/*globals angular */
angular.module('csGoMiniGamesApp').filter('localeCurrency', function ($filter, LocaleService) {
    'use strict';

    function filterFunction(input) {
        var rate = LocaleService.currentLanguage.rate || 1,
            scaledValue = input * rate;

        return LocaleService.getLanguage();

        //return $filter('currency')(scaledValue, LocaleService.currentLanguage.symbol);
    };
    
    filterFunction.$stateful;

    return filterFunction
});