/*jslint nomen: true */
/*globals angular, _ */
angular.module('csGoMiniGamesApp').service('LocaleService', function ($translate, $http, $q, $rootScope) {
    'use strict';

    var self = this;

    this.languages = [{
        key: 'en',
        display: 'English'
    }, {
        key: 'ru',
        display: 'Русский'
    }, {
        key: 'zh-CN',
        display: '中文(简体)'
    }];

    this.getLanguage = function () {
        var deferred = $q.defer();

        deferred.resolve(self.findLanguage(self.currentLanguage));

        return deferred.promise;
    };



    this.findLanguage = function (languageKey) {
        return _.find(self.languages, function (language) {
            return language.key === languageKey;
        });
    };

    this.changeLanguage = function (languageKey) {
        $translate.use(languageKey);
        self.currentLanguage = this.findLanguage(languageKey);
    };

    this.currentLanguage = this.findLanguage($translate.proposedLanguage() || $translate.use());
});