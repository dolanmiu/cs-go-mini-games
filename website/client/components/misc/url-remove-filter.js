/*globals angular */
angular.module('csGoMiniGamesApp').filter('urlRemove', function ($filter) {
    'use strict';
    
    return function (text) {
        if (text === undefined) {
            return text;
        }
        return text.replace(/(http\:\/\/|https\:\/\/)?([a-z0-9][a-z0-9\-]*\.)+[a-z][a-z\/\-]*/ig, '');
    };
});