/*globals angular */
angular.module('csGoMiniGamesApp').directive('itemThumbnail', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            src: '=',
            alt: '@',
            size: '@'
        },
        templateUrl: 'components/item-thumbnail/skin-thumbnail/item-thumbnail.html',
        link: function (scope, element) {
        }
    };
});