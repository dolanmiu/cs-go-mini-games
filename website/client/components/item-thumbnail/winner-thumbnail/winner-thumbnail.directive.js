/*globals angular */
angular.module('csGoMiniGamesApp').directive('winnerThumbnail', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            src: '=',
            text: '@',
            title: '@'
        },
        templateUrl: 'components/item-thumbnail/winner-thumbnail/winner-thumbnail.html',
        link: function (scope, element) {
        }
    };
});