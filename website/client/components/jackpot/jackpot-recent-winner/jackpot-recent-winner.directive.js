/*globals angular, regexp: true */
angular.module('csGoMiniGamesApp').directive('jackpotRecentWinner', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            jackpotLog: '='
        },
        controller: function ($scope, ItemUtilityService) {
            $scope.itemUtilityService = ItemUtilityService;
        },
        templateUrl: 'components/jackpot/jackpot-recent-winner/jackpot-recent-winner.html',
        link: function ($scope, $element, $attrs) {

        }
    };
});