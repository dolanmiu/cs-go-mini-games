/*globals angular, $, JackpotGame */
angular.module('csGoMiniGamesApp').service('jackpotGame', function ($window, $timeout, Jackpot, LocaleService, ItemUtilityService, JackpotLog, Notification, User) {
    'use strict';

    var game;

    function createGame(container) {
        Jackpot.query().$promise.then(function (jackpotEntries) {
            game = new JackpotGame.Game($('body').innerWidth(), $window.innerHeight - 50);
            Jackpot.config().$promise.then(function (config) {
                game.run(container, function () {
                    game.setJackpotItemsArray(jackpotEntries, config.max);
                    Jackpot.time().$promise.then(function (response) {
                        game.setTime(response.remaining);
                    });
                });
            });
        });
    }

    this.setWinner = function (winner) {
        game.setWinner(winner);
        game.setWaterLevel(1);
        $timeout(function () {
            game.setWaterLevel(0, 5000);
        }, 2000);
    };

    this.setTime = function (time) {
        game.setTime(time.remaining);
    };

    createGame();
});