/*globals angular, regexp: true */
angular.module('csGoMiniGamesApp').directive('jackpotSelectItem', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            inventoryEntry: '='
        },
        controller: function ($scope) {
            $scope.getName = function (name) {
                var re = /([^|]+ \| .+(?!\(\))) \([a-z \-]+\)/i;
                return name.match(re)[1];
            };
            
            $scope.getWearFromName = function (name) {
                var re = /\((.+)\)/i;
                return name.match(re)[1];
            };
        },
        templateUrl: 'components/jackpot/jackpot-select-item/jackpot-select-item.html',
        link: function ($scope, $element, $attrs) {}
    };
});