/*globals angular, regexp: true */
angular.module('csGoMiniGamesApp').directive('jackpotPotItem', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            item: '=',
            count: '@'
        },
        templateUrl: 'components/jackpot/jackpot-pot-item/jackpot-pot-item.html',
        link: function (scope, element, attrs) {
            scope.isStatTrak = scope.item.item.marketName.includes("StatTrak");
        }
    };
});