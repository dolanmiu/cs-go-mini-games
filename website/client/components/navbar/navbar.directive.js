/*globals angular */
angular.module('csGoMiniGamesApp').directive('navbar', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {},
        templateUrl: 'components/navbar/navbar.html',
        controller: 'NavbarController',
        link: function ($scope, $element, $attrs) {}
    };
});