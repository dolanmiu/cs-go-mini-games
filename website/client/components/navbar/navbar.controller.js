/*jslint nomen: true */
/*globals angular */
angular.module('csGoMiniGamesApp').controller('NavbarController', function ($scope, $location, Auth, $window, Bot, InboxMessage, SteamService, NavbarService) {
    'use strict';

    $scope.menu = [{
        title: 'NAVIGATION_BAR.HOW_TO',
        link: 'howto'
    }, {
        title: 'NAVIGATION_BAR.NEWS',
        link: 'news'
    }, {
        title: 'NAVIGATION_BAR.CRATE.HEADING',
        link: 'crate',
        subMenu: [{
            title: 'Free',
            header: true
        }, {
            title: 'NAVIGATION_BAR.CRATE.DAILY',
            link: 'crateGame({name: "free"})'
        }, {
            title: 'Premium',
            header: true
        }, {
            title: 'NAVIGATION_BAR.CRATE.WATER',
            link: 'crateGame({name: "water"})'
        }, {
            title: 'NAVIGATION_BAR.CRATE.AIR',
            link: 'crateGame({name: "air"})'
        }, {
            title: 'NAVIGATION_BAR.CRATE.LEAF',
            link: 'crateGame({name: "leaf"})'
        }, {
            title: 'NAVIGATION_BAR.CRATE.FIRE',
            link: 'crateGame({name: "fire"})'
        }]
    }, {
        title: 'NAVIGATION_BAR.JACKPOT',
        link: 'jackpot'
    }, {
        title: 'NAVIGATION_BAR.STORE',
        link: 'store'
    }, {
        title: 'NAVIGATION_BAR.REFERRAL',
        link: 'account.referral'
    }];

    $scope.userMenu = [{
        title: 'NAVIGATION_BAR.ACCOUNT.MY_ACCOUNT',
        link: 'account.profile',
        icon: 'user'
    }, {
        divider: true
    }, {
        title: 'Deposit',
        header: true
    }, {
        title: 'NAVIGATION_BAR.ACCOUNT.DEPOSIT_COIN',
        link: 'account.getcoin',
        icon: 'life-ring'
    }, {
        title: 'NAVIGATION_BAR.ACCOUNT.DEPOSIT_ITEM',
        link: 'account.deposit',
        icon: 'archive'
    }, {
        divider: true
    }, {
        title: 'Withdraw',
        header: true
    }, {
        title: 'NAVIGATION_BAR.ACCOUNT.INVENTORY',
        link: 'account.inventory',
        icon: 'briefcase'
    }];

    $scope.isCollapsed = true;
    $scope.isLoggedIn = Auth.isLoggedIn;
    $scope.isAdmin = Auth.isAdmin;
    $scope.getCurrentUser = Auth.getCurrentUser;
    $scope.inboxMessages = NavbarService.inboxMessages;

    $scope.logout = function () {
        Auth.logout();
        $location.path('/');
    };

    $scope.redirectToSteam = function () {
        SteamService.redirectToSteamOAuth();
    };

    Auth.isLoggedInAsync(function (result) {
        $scope.isLoggedIn = result;
    });
});