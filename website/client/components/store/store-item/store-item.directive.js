/*globals angular */
angular.module('csGoMiniGamesApp').directive('storeItem', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            item: '=',
            itemsPerRow: '@'
        },
        templateUrl: 'components/store/store-item/store-item.html',
        link: function (scope, element) {
        }
    };
});