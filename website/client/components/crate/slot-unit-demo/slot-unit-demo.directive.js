/*globals angular */
angular.module('csGoMiniGamesApp').directive('slotUnitDemo', function () {
    'use strict';

    return {
        restrict: 'E',
        transclude: true,
        scope: {
            rarity: '@',
            text: '@'
        },
        templateUrl: 'components/crate/slot-unit-demo/slot-unit-demo.html',
        link: function ($scope, $element, $attrs) {
            switch ($scope.rarity) {
            case "1":
                $scope.tier = 'common';
                $scope.image = 'one-small';
                break;
            case "2":
                $scope.tier = 'uncommon';
                $scope.image = 'two-small';
                break;
            case "3":
                $scope.tier = 'rare';
                $scope.image = 'three-small';
                break;
            case "4":
                $scope.tier = 'epic';
                $scope.image = 'four-small';
                break;
            case "5":
                $scope.tier = 'legendary';
                $scope.image = 'five-small';
                break;
            case "6":
                $scope.tier = 'godlike';
                $scope.image = 'six-small';
                break;
            }
        }
    };
});