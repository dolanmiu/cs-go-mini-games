/*globals angular */
angular.module('csGoMiniGamesApp').directive('crateTimer', function () {
    'use strict';

    return {
        restrict: 'E',
        transclude: true,
        scope: {
            time: '@'
        },
        templateUrl: 'components/crate/timer/crate-timer.html',
        controller: function ($scope, $interval) {
            $interval(function () {
                if ($scope.time - 1000 > 0) {
                    $scope.time -= 1000;
                } else {
                    $scope.time = 0;
                }

                $scope.hours = Math.floor($scope.time / 36e5);
                $scope.mins = Math.floor(($scope.time % 36e5) / 6e4);
                $scope.secs = Math.floor(($scope.time % 6e4) / 1000);
            }, 1000);
        },
        link: function ($scope, $element, $attrs) {

        }
    };
});