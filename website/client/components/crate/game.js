var CrateGame;
(function (CrateGame) {
    var CameraController = (function () {
        function CameraController(camera) {
            var _this = this;
            this.camera = camera;
            this.mousePosition = new THREE.Vector2(0, 0);
            this.smoothVector = new THREE.Vector2(0, 0);
            this.cameraDefaultZPosition = 400;
            this.camera.position.z = this.cameraDefaultZPosition;
            window.addEventListener('mousemove', function (event) {
                _this.mousePosition.x = (event.clientX / window.innerWidth) * 2 - 1;
                _this.mousePosition.y = -(event.clientY / window.innerHeight) * 2 + 1;
            }, false);
        }
        CameraController.prototype.update = function (delta) {
            var xDiff = this.mousePosition.x - this.smoothVector.x;
            var yDiff = this.mousePosition.y - this.smoothVector.y;
            this.smoothVector.x += 0.1 * xDiff;
            this.smoothVector.y += 0.1 * yDiff;
            this.camera.position.x = 248 * -this.smoothVector.x;
            this.camera.position.y = 200 + 100 * (1 + -this.smoothVector.y);
            this.camera.lookAt(new THREE.Vector3(0, 0, 0));
        };
        CameraController.prototype.dramaticPanOut = function () {
            var tweenOut = new TWEEN.Tween(this.camera.position)
                .to({ z: 600 }, 1000)
                .easing(TWEEN.Easing.Quadratic.Out)
                .start();
        };
        CameraController.prototype.dramaticPanIn = function () {
            var tweenIn = new TWEEN.Tween(this.camera.position)
                .to({ z: this.cameraDefaultZPosition }, 1000)
                .easing(TWEEN.Easing.Quadratic.Out)
                .start();
        };
        return CameraController;
    })();
    CrateGame.CameraController = CameraController;
})(CrateGame || (CrateGame = {}));
var CrateGame;
(function (CrateGame) {
    var Game = (function () {
        function Game(width, height) {
            this.renderer = new THREE.WebGLRenderer({ alpha: true, antialias: true });
            this.renderer.setSize(width, height);
            this.renderer.setClearColor(0xFFFFFF, 1);
            this.scene = new THREE.Scene();
            this.camera = new THREE.PerspectiveCamera(45, width / height, 0.1, 20000);
            this.clock = new THREE.Clock();
            this.textureLoader = new THREE.TextureLoader();
            this.textureLoader.crossOrigin = 'anonymous';
            this.crateFactory = new CrateGame.CrateFactory(new THREE.ObjectLoader(), this.textureLoader);
            this.floorFactory = new CrateGame.FloorFactory(new THREE.ObjectLoader(), this.textureLoader);
            this.prizeFactory = new CrateGame.PrizeFactory(this.textureLoader);
            this.textFactory = new CrateGame.TextFactory();
            //this.controls = new THREE.OrbitControls(this.camera, this.renderer.domElement);
            this.cameraController = new CrateGame.CameraController(this.camera);
        }
        Game.prototype.run = function (container, crateName, loadedCallback) {
            var _this = this;
            document.getElementById(container).appendChild(this.renderer.domElement);
            this.scene.add(this.camera);
            this.crateFactory.create("/assets/crate/lid/" + crateName.toLowerCase() + ".png", function (crate) {
                _this.crate = crate;
                _this.scene.add(crate.obj);
            });
            this.floorFactory.create(function (floor) {
                _this.scene.add(floor);
            });
            this.initListeners();
            var light = new THREE.DirectionalLight(0xffffff);
            light.position.set(-100, 200, 100);
            this.scene.add(light);
            this.textFactory.newInstance("hello world");
            this.render();
        };
        Game.prototype.initListeners = function () {
            var _this = this;
            window.addEventListener('resize', function () {
                var newWidth = $('body').innerWidth();
                var newHeight = window.innerHeight;
                _this.renderer.setSize(newWidth, newHeight);
                _this.camera.aspect = newWidth / newHeight;
                _this.camera.updateProjectionMatrix();
            });
        };
        Game.prototype.render = function () {
            var _this = this;
            requestAnimationFrame(function () { return _this.render(); });
            this.renderer.render(this.scene, this.camera);
            //this.controls.update();
            if (this.crate) {
                this.crate.update(this.clock.getDelta());
            }
            this.cameraController.update(this.clock.getDelta());
            TWEEN.update();
        };
        Game.prototype.open = function (imageUrl, text) {
            var _this = this;
            this.prizeFactory.newInstance(imageUrl, function (prize) {
                _this.prize = prize;
                _this.scene.add(_this.prize);
                _this.text = _this.textFactory.newInstance(text);
                _this.text.position.z = 50;
                _this.scene.add(_this.text);
                var tween = new TWEEN.Tween(_this.prize.position)
                    .to({ y: 150 }, 1000)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .start();
                var tweenRotation = new TWEEN.Tween(_this.prize.rotation)
                    .to({ y: -0.349066 }, 8000)
                    .easing(TWEEN.Easing.Linear.None)
                    .repeat(Infinity)
                    .yoyo(true)
                    .start();
                var tweenText = new TWEEN.Tween(_this.text.position)
                    .to({ y: 150 }, 1000)
                    .easing(TWEEN.Easing.Quadratic.Out)
                    .start();
                _this.cameraController.dramaticPanOut();
                _this.crate.open();
            });
        };
        Game.prototype.reset = function () {
            this.crate.reset();
            this.cameraController.dramaticPanIn();
            this.prize.position.y = -10;
            this.scene.remove(this.prize);
            this.scene.remove(this.text);
        };
        Game.prototype.destroy = function () {
            this.renderer.forceContextLoss();
        };
        return Game;
    })();
    CrateGame.Game = Game;
})(CrateGame || (CrateGame = {}));
var CrateGame;
(function (CrateGame) {
    var Crate = (function () {
        function Crate(obj, animationMixer) {
            this.animationMixer = animationMixer;
            this.obj = obj;
            this.animationMixer.update(0);
            this.animationMixer.actions[0].enabled = false;
            this.animationMixer.actions[0].actionTime = 0;
        }
        Crate.prototype.update = function (delta) {
            this.animationMixer.update(delta * 0.75);
        };
        Crate.prototype.open = function () {
            this.animationMixer.actions[0].enabled = true;
            this.animationMixer.actions[0].actionTime = 0;
        };
        Crate.prototype.reset = function () {
            this.animationMixer.actions[0].actionTime = 0;
            this.animationMixer.update(0);
            this.animationMixer.actions[0].enabled = false;
        };
        return Crate;
    })();
    CrateGame.Crate = Crate;
})(CrateGame || (CrateGame = {}));
var CrateGame;
(function (CrateGame) {
    var CrateFactory = (function () {
        function CrateFactory(modelLoader, textureLoader) {
            this.modelLoader = modelLoader;
            this.textureLoader = textureLoader;
        }
        CrateFactory.prototype.create = function (lidTexturePath, callback) {
            var _this = this;
            this.modelLoader.load("/assets/crate/crate.json", function (obj) {
                var animationMixer = new THREE.AnimationMixer(obj);
                _this.textureLoader.load("/assets/crate/container.png", function (texture) {
                    var containerMaterial = new THREE.MeshBasicMaterial({
                        map: texture
                    });
                    //texture.magFilter = THREE.NearestFilter;
                    //texture.minFilter = THREE.LinearMipMapLinearFilter;
                    _this.textureLoader.load(lidTexturePath, function (texture) {
                        var lidMaterial = new THREE.MeshBasicMaterial({
                            map: texture
                        });
                        texture.magFilter = THREE.NearestFilter;
                        texture.minFilter = THREE.LinearMipMapLinearFilter;
                        obj.traverse(function (child) {
                            if (child instanceof THREE.Mesh) {
                                switch (child.name) {
                                    case "Container":
                                        child.material = containerMaterial;
                                        break;
                                    case "Lid":
                                        child.material = lidMaterial;
                                        break;
                                }
                            }
                        });
                        var animation = new THREE.AnimationAction(obj.animations[0], undefined, undefined, 1, THREE.LoopOnce);
                        animationMixer.addAction(animation);
                        //animationMixer.removeAllActions();
                        callback(new CrateGame.Crate(obj, animationMixer));
                    });
                });
            });
        };
        return CrateFactory;
    })();
    CrateGame.CrateFactory = CrateFactory;
})(CrateGame || (CrateGame = {}));
var CrateGame;
(function (CrateGame) {
    var FloorFactory = (function () {
        function FloorFactory(modelLoader, textureLoader) {
            this.modelLoader = modelLoader;
            this.textureLoader = textureLoader;
        }
        FloorFactory.prototype.create = function (callback) {
            var _this = this;
            this.modelLoader.load("/assets/crate/floor.json", function (obj) {
                _this.textureLoader.load("/assets/crate/floor-texture.png", function (texture) {
                    var floorMaterial = new THREE.MeshBasicMaterial({
                        map: texture
                    });
                    obj.traverse(function (child) {
                        if (child instanceof THREE.Mesh) {
                            child.material = floorMaterial;
                        }
                    });
                    callback(obj);
                });
            });
        };
        return FloorFactory;
    })();
    CrateGame.FloorFactory = FloorFactory;
})(CrateGame || (CrateGame = {}));
var CrateGame;
(function (CrateGame) {
    var PrizeFactory = (function () {
        function PrizeFactory(textureLoader) {
            this.textureLoader = textureLoader;
        }
        PrizeFactory.prototype.newInstance = function (imageUrl, callback) {
            var imageTexture = this.textureLoader.load(imageUrl, function (texture) {
                imageTexture.minFilter = THREE.LinearFilter;
                var material = new THREE.MeshBasicMaterial({ map: imageTexture, color: 0xffffff, fog: true, side: THREE.DoubleSide, transparent: true });
                var geometry = new THREE.PlaneGeometry(150, 84, 1);
                var prize = new THREE.Mesh(geometry, material);
                prize.position.set(0, -10, 0);
                prize.rotation.y = 0.349066;
                callback(prize);
            });
        };
        return PrizeFactory;
    })();
    CrateGame.PrizeFactory = PrizeFactory;
})(CrateGame || (CrateGame = {}));
var CrateGame;
(function (CrateGame) {
    var TextFactory = (function () {
        function TextFactory() {
        }
        TextFactory.prototype.newInstance = function (message) {
            return this.makeTextSprite(message, { fontsize: 32, fontface: "Source Sans Pro", borderColor: { r: 0, g: 0, b: 0, a: 0.7 } });
        };
        TextFactory.prototype.makeTextSprite = function (message, parameters) {
            if (parameters === undefined)
                parameters = {};
            var fontface = parameters.hasOwnProperty("fontface") ?
                parameters["fontface"] : "Arial";
            var fontsize = parameters.hasOwnProperty("fontsize") ?
                parameters["fontsize"] : 18;
            var borderThickness = parameters.hasOwnProperty("borderThickness") ?
                parameters["borderThickness"] : 4;
            var borderColor = parameters.hasOwnProperty("borderColor") ?
                parameters["borderColor"] : { r: 0, g: 0, b: 0, a: 1.0 };
            var backgroundColor = parameters.hasOwnProperty("backgroundColor") ?
                parameters["backgroundColor"] : { r: 255, g: 255, b: 255, a: 1.0 };
            var canvas = document.createElement('canvas');
            var context = canvas.getContext('2d');
            context.font = "Bold " + fontsize + "px " + fontface;
            // get size data (height depends only on font size)
            var metrics = context.measureText(message);
            var textWidth = metrics.width;
            // background color
            context.fillStyle = "rgba(" + backgroundColor.r + "," + backgroundColor.g + ","
                + backgroundColor.b + "," + backgroundColor.a + ")";
            // border color
            context.strokeStyle = "rgba(" + borderColor.r + "," + borderColor.g + ","
                + borderColor.b + "," + borderColor.a + ")";
            context.lineWidth = borderThickness;
            //this.roundRect(context, borderThickness / 2, borderThickness / 2, textWidth + borderThickness, fontsize * 1.4 + borderThickness, 6);
            // 1.4 is extra height factor for text below baseline: g,j,p,q.
            context.strokeText(message, borderThickness, fontsize + borderThickness);
            // text color
            context.fillStyle = "rgba(247, 235, 2, 1.0)";
            context.fillText(message, borderThickness, fontsize + borderThickness);
            //context.shadowColor = "#000";
            //context.shadowOffsetY = 2;
            // canvas contents will be used for a texture
            var texture = new THREE.Texture(canvas);
            texture.needsUpdate = true;
            var spriteMaterial = new THREE.SpriteMaterial({ map: texture });
            var sprite = new THREE.Sprite(spriteMaterial);
            sprite.scale.set(100, 50, 1.0);
            return sprite;
        };
        // function for drawing rounded rectangles
        TextFactory.prototype.roundRect = function (ctx, x, y, w, h, r) {
            ctx.beginPath();
            ctx.moveTo(x + r, y);
            ctx.lineTo(x + w - r, y);
            ctx.quadraticCurveTo(x + w, y, x + w, y + r);
            ctx.lineTo(x + w, y + h - r);
            ctx.quadraticCurveTo(x + w, y + h, x + w - r, y + h);
            ctx.lineTo(x + r, y + h);
            ctx.quadraticCurveTo(x, y + h, x, y + h - r);
            ctx.lineTo(x, y + r);
            ctx.quadraticCurveTo(x, y, x + r, y);
            ctx.closePath();
            ctx.fill();
            ctx.stroke();
        };
        return TextFactory;
    })();
    CrateGame.TextFactory = TextFactory;
})(CrateGame || (CrateGame = {}));
