/*globals angular */
angular.module('csGoMiniGamesApp').directive('crateNotice', function () {
    'use strict';

    return {
        restrict: 'E',
        transclude: true,
        scope: {
            notice: '='
        },
        templateUrl: 'components/crate/notice/notice.html',
        link: function ($scope, $element, $attrs) {
            if (angular.isUndefined($scope.notice)) {
                return;
            }

            switch ($scope.notice.prize.rarity) {
            case 1:
                $scope.tier = 'common';
                break;
            case 2:
                $scope.tier = 'uncommon';
                break;
            case 3:
                $scope.tier = 'rare';
                break;
            case 4:
                $scope.tier = 'epic';
                break;
            case 5:
                $scope.tier = 'legendary';
                break;
            case 6:
                $scope.tier = 'godlike';
                break;
            }
        }
    };
});