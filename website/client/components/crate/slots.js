var CrateSlot;
(function (CrateSlot) {
    var Game = (function () {
        function Game(width, height) {
            this.width = width;
            this.height = height;
        }
        Game.prototype.run = function (container, loadedCallback) {
            this.game = new Phaser.Game(this.width, this.height, Phaser.AUTO, container, CrateSlot.States.MainState, false);
            this.game.stateLoadedCallback = loadedCallback;
        };
        Game.prototype.setCoinData = function (coinInput) {
            var state = this.game.state.getCurrentState();
            if (state) {
                var coins = new Array();
                coinInput.forEach(function (coin) {
                    coins.push(new CrateSlot.Models.Coin(coin.amount, coin.rarity, coin._id));
                });
                state.setCoinData(_.shuffle(coins));
            }
        };
        Game.prototype.setInventoryData = function (inventoryItems) {
            var state = this.game.state.getCurrentState();
            if (state) {
                state.setInventoryData(inventoryItems);
            }
        };
        Game.prototype.rollSlots = function (uniqueKey, callback) {
            var state = this.game.state.getCurrentState();
            if (state) {
                state.rollSlots(uniqueKey, callback);
            }
        };
        Game.prototype.destroy = function () {
            this.game.destroy();
        };
        return Game;
    })();
    CrateSlot.Game = Game;
})(CrateSlot || (CrateSlot = {}));
var CrateSlot;
(function (CrateSlot) {
    var Models;
    (function (Models) {
        var Coin = (function () {
            function Coin(amount, rarity, _id) {
                this.amount = amount;
                this.rarity = rarity;
                this._id = _id;
                if (this.rarity === 1) {
                    this.imageUrl = "/assets/images/coin/one.png";
                    this.name = "one";
                }
                else if (this.rarity === 2) {
                    this.imageUrl = "/assets/images/coin/two.png";
                    this.name = "two";
                }
                else if (this.rarity === 3) {
                    this.imageUrl = "/assets/images/coin/three.png";
                    this.name = "three";
                }
                else if (this.rarity === 4) {
                    this.imageUrl = "/assets/images/coin/four.png";
                    this.name = "four";
                }
                else if (this.rarity === 5) {
                    this.imageUrl = "/assets/images/coin/five.png";
                    this.name = "five";
                }
                else if (this.rarity === 6) {
                    this.imageUrl = "/assets/images/coin/six.png";
                    this.name = "six";
                }
            }
            return Coin;
        })();
        Models.Coin = Coin;
    })(Models = CrateSlot.Models || (CrateSlot.Models = {}));
})(CrateSlot || (CrateSlot = {}));
var CrateSlot;
(function (CrateSlot) {
    var Models;
    (function (Models) {
        var InventoryItem = (function () {
            function InventoryItem() {
            }
            return InventoryItem;
        })();
        Models.InventoryItem = InventoryItem;
    })(Models = CrateSlot.Models || (CrateSlot.Models = {}));
})(CrateSlot || (CrateSlot = {}));
var CrateSlot;
(function (CrateSlot) {
    var Models;
    (function (Models) {
        var Slot = (function () {
            function Slot() {
            }
            return Slot;
        })();
        Models.Slot = Slot;
    })(Models = CrateSlot.Models || (CrateSlot.Models = {}));
})(CrateSlot || (CrateSlot = {}));
var __extends = (this && this.__extends) || function (d, b) {
    for (var p in b) if (b.hasOwnProperty(p)) d[p] = b[p];
    function __() { this.constructor = d; }
    d.prototype = b === null ? Object.create(b) : (__.prototype = b.prototype, new __());
};
var CrateSlot;
(function (CrateSlot) {
    var Prefabs;
    (function (Prefabs) {
        var Meter = (function (_super) {
            __extends(Meter, _super);
            function Meter() {
                var points = [];
                for (var _i = 0; _i < arguments.length; _i++) {
                    points[_i - 0] = arguments[_i];
                }
                _super.call(this, points);
            }
            Meter.prototype.draw = function (graphics) {
                graphics.beginFill(0xFFFFFF);
                graphics.drawPolygon(this.points);
                graphics.endFill();
            };
            return Meter;
        })(Phaser.Polygon);
        Prefabs.Meter = Meter;
    })(Prefabs = CrateSlot.Prefabs || (CrateSlot.Prefabs = {}));
})(CrateSlot || (CrateSlot = {}));
var CrateSlot;
(function (CrateSlot) {
    var Prefabs;
    (function (Prefabs) {
        var MeterFactory = (function () {
            function MeterFactory(game) {
                this.game = game;
            }
            MeterFactory.prototype.create = function (x, width) {
                var meter = new Prefabs.Meter(new Phaser.Point(x, 0), new Phaser.Point(x + width, 0), new Phaser.Point(x + width, this.game.height), new Phaser.Point(x, this.game.height));
                return meter;
            };
            return MeterFactory;
        })();
        Prefabs.MeterFactory = MeterFactory;
    })(Prefabs = CrateSlot.Prefabs || (CrateSlot.Prefabs = {}));
})(CrateSlot || (CrateSlot = {}));
var CrateSlot;
(function (CrateSlot) {
    var Prefabs;
    (function (Prefabs) {
        var Slot = (function (_super) {
            __extends(Slot, _super);
            function Slot(game, key, uniqueKey, foregroundGroup, backgroundGroup, text) {
                this.background = game.add.image(0, 5, "slot-" + key + "-bg");
                _super.call(this, game, 0, 0, key);
                this.uniqueKey = uniqueKey;
                this.y = game.height / 2 - 150 / 2;
                this.width = 150;
                this.height = 84;
                backgroundGroup.add(this.background);
                foregroundGroup.add(this);
                this.text = new Phaser.Text(game, 0, 0, text, { font: 'Source Sans Pro', fill: '#ffffff', align: 'center', fontSize: 16 });
                this.text.setShadow(0, 1, 'rgba(0,0,0,0.7)', 0);
                this.text.anchor.set(0.5);
                this.text.y = game.height - 30;
                foregroundGroup.add(this.text);
            }
            Slot.prototype.update = function () {
                this.background.x = this.x;
                this.text.x = Math.round(this.x + this.width / 2);
            };
            Slot.prototype.resetPosition = function () {
                this.x = this.originalX;
            };
            return Slot;
        })(Phaser.Sprite);
        Prefabs.Slot = Slot;
    })(Prefabs = CrateSlot.Prefabs || (CrateSlot.Prefabs = {}));
})(CrateSlot || (CrateSlot = {}));
var CrateSlot;
(function (CrateSlot) {
    var Prefabs;
    (function (Prefabs) {
        var SlotFactory = (function () {
            function SlotFactory(game, foregroundGroup, backgroundGroup) {
                this.game = game;
                this.foregroundGroup = foregroundGroup;
                this.backgroundGroup = backgroundGroup;
            }
            SlotFactory.prototype.create = function (key, uniqueId, text) {
                var slot = new Prefabs.Slot(this.game, key, uniqueId, this.foregroundGroup, this.backgroundGroup, text);
                return slot;
            };
            return SlotFactory;
        })();
        Prefabs.SlotFactory = SlotFactory;
    })(Prefabs = CrateSlot.Prefabs || (CrateSlot.Prefabs = {}));
})(CrateSlot || (CrateSlot = {}));
var CrateSlot;
(function (CrateSlot) {
    var Prefabs;
    (function (Prefabs) {
        var SlotLoader = (function (_super) {
            __extends(SlotLoader, _super);
            function SlotLoader(game) {
                _super.call(this, game);
            }
            SlotLoader.prototype.batchLoadCoins = function (coins, callback) {
                var _this = this;
                coins.forEach(function (coin) {
                    _this.image(coin.name, coin.imageUrl, false);
                });
                this.onFileComplete.add(function (progress, cacheKey) {
                }, this);
                this.onLoadComplete.addOnce(function () {
                    callback();
                });
                this.start();
            };
            SlotLoader.prototype.batchLoadInventory = function (inventoryItems, callback) {
                var _this = this;
                inventoryItems.forEach(function (inventoryItem) {
                    _this.image(inventoryItem.assetId, inventoryItem.item.iconUrl.large, false);
                });
                this.onLoadComplete.addOnce(function () {
                    callback();
                });
                this.start();
            };
            return SlotLoader;
        })(Phaser.Loader);
        Prefabs.SlotLoader = SlotLoader;
    })(Prefabs = CrateSlot.Prefabs || (CrateSlot.Prefabs = {}));
})(CrateSlot || (CrateSlot = {}));
var CrateSlot;
(function (CrateSlot) {
    var Prefabs;
    (function (Prefabs) {
        var Wheel = (function () {
            function Wheel(game, imgGroup) {
                this.speed = 0;
                this.game = game;
                this.slots = new Array();
                this.imgGroup = imgGroup;
                this.offset = 0;
                this.slotSpacing = 10;
                this.graphics = game.add.graphics(0, 0);
                this.bgGroup = new Phaser.Group(game);
                this.bgGroup.z = -10;
            }
            Wheel.prototype.findSlot = function (uniqueKey) {
                for (var i = 0; i < this.slots.length; i++) {
                    if (this.slots[i].uniqueKey === uniqueKey) {
                        return this.slots[i];
                    }
                }
            };
            Wheel.prototype.getTimeTaken = function (distancetoStoppingPoint) {
                var time = (2 * distancetoStoppingPoint) / this.speed;
                return time * 1000;
            };
            Wheel.prototype.getDistanceForSlotToCenter = function (slot) {
                var targetPoint = this.game.width / 2;
                var distance = targetPoint - slot.x;
                var targetVariation = Math.floor(Math.random() * slot.width);
                //var targetVariation = 0;
                var nearestDistance = 0;
                if (distance > 0) {
                    nearestDistance = distance - targetVariation;
                }
                else {
                    var remainingDistance = this.game.width - slot.x;
                    var newDistance = this.getSlotsWidth() - targetPoint;
                    nearestDistance = remainingDistance + newDistance - targetVariation;
                }
                return nearestDistance + 5 * this.getSlotsWidth();
            };
            Wheel.prototype.getSlotsWidth = function () {
                return this.slots.length * (this.slots[0].width + this.slotSpacing);
            };
            Wheel.prototype.update = function (delta) {
                var _this = this;
                this.slots.forEach(function (slot) {
                    var addedDelta = _this.speed * delta;
                    slot.travelDistance += addedDelta;
                    slot.x = slot.width + (slot.travelDistance + _this.offset) % _this.getSlotsWidth() - (_this.getSlotsWidth() / 2);
                    slot.update();
                });
            };
            Wheel.prototype.setSpeed = function (speed) {
                this.speed = speed;
            };
            Wheel.prototype.addSlot = function (slot) {
                if (this.slots.length > 0) {
                    var lastSlot = this.slots[this.slots.length - 1];
                    var startX = lastSlot.position.x + lastSlot.width + this.slotSpacing;
                    slot.position.x = startX;
                    slot.originalX = startX;
                    slot.travelDistance = startX;
                }
                else {
                    var startX = 0;
                    slot.position.x = startX;
                    slot.originalX = startX;
                    slot.travelDistance = startX;
                }
                this.slots.push(slot);
                this.imgGroup.add(slot);
            };
            Wheel.prototype.reset = function () {
                this.slots.forEach(function (slot) {
                    slot.resetPosition();
                });
                this.offset = 0;
            };
            Wheel.prototype.stopAt = function (uniqueKey, callback) {
                var slot = this.findSlot(uniqueKey);
                var tween = this.game.add.tween(this);
                var distance = this.getDistanceForSlotToCenter(slot);
                var time = this.getTimeTaken(distance);
                this.speed = 0;
                tween.to({ offset: this.offset + distance }, time, Phaser.Easing.Quadratic.Out, true);
                tween.onComplete.add(callback, this);
            };
            return Wheel;
        })();
        Prefabs.Wheel = Wheel;
    })(Prefabs = CrateSlot.Prefabs || (CrateSlot.Prefabs = {}));
})(CrateSlot || (CrateSlot = {}));
var CrateSlot;
(function (CrateSlot) {
    var States;
    (function (States) {
        var MainState = (function (_super) {
            __extends(MainState, _super);
            function MainState() {
                _super.call(this);
            }
            MainState.prototype.preload = function () {
                this.game.stage.disableVisibilityChange = true;
                this.game.load.crossOrigin = "anonymous";
                this.game.load.image("slot-one-bg", "/assets/images/slot-background/one.png");
                this.game.load.image("slot-two-bg", "/assets/images/slot-background/two.png");
                this.game.load.image("slot-three-bg", "/assets/images/slot-background/three.png");
                this.game.load.image("slot-four-bg", "/assets/images/slot-background/four.png");
                this.game.load.image("slot-five-bg", "/assets/images/slot-background/five.png");
                this.game.load.image("slot-six-bg", "/assets/images/slot-background/six.png");
            };
            MainState.prototype.create = function () {
                var wheelGroup = new Phaser.Group(this.game);
                wheelGroup.z = 10;
                var slotBgGroup = new Phaser.Group(this.game);
                slotBgGroup.z = 0;
                //slotBgGroup.add();
                this.graphics = this.game.add.graphics(0, 0);
                this.wheel = new CrateSlot.Prefabs.Wheel(this.game, wheelGroup);
                this.slotLoader = new CrateSlot.Prefabs.SlotLoader(this.game);
                this.slotLoader.crossOrigin = "anonymous";
                this.slotFactory = new CrateSlot.Prefabs.SlotFactory(this.game, slotBgGroup, wheelGroup);
                var meterFactory = new CrateSlot.Prefabs.MeterFactory(this.game);
                this.meter = meterFactory.create(this.game.width / 2, 1);
                this.meter.draw(this.graphics);
                var group = new Phaser.Group(this.game);
                group.add(this.graphics);
                group.z = 100;
                this.game.stateLoadedCallback();
            };
            MainState.prototype.update = function () {
                this.wheel.update(this.game.time.elapsed / 1000);
            };
            MainState.prototype.rollSlots = function (uniqueKey, callback) {
                var _this = this;
                this.wheel.setSpeed(2500);
                setTimeout(function () {
                    _this.wheel.stopAt(uniqueKey, callback);
                }, 2000);
            };
            MainState.prototype.setCoinData = function (coins) {
                var _this = this;
                this.slotLoader.batchLoadCoins(coins, function () {
                    coins.forEach(function (coin) {
                        var slot = _this.slotFactory.create(coin.name, coin._id, coin.amount + ' Coins');
                        _this.wheel.addSlot(slot);
                    });
                });
            };
            MainState.prototype.setInventoryData = function (inventoryItems) {
                var _this = this;
                this.slotLoader.batchLoadInventory(inventoryItems, function () {
                    inventoryItems.forEach(function (inventoryItem) {
                        var slot = _this.slotFactory.create(inventoryItem.assetId, inventoryItem._id, inventoryItem.item.item.marketName);
                        _this.wheel.addSlot(slot);
                    });
                });
            };
            return MainState;
        })(Phaser.State);
        States.MainState = MainState;
    })(States = CrateSlot.States || (CrateSlot.States = {}));
})(CrateSlot || (CrateSlot = {}));
