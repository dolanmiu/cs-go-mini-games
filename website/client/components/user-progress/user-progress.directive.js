/*globals angular */
angular.module('csGoMiniGamesApp').directive('userProgress', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            post: '=',
            value: '=',
            max: '=',
            imageUrl: '@',
            name: '@'
        },
        templateUrl: 'components/user-progress/user-progress.html',
        link: function ($scope, $element, $attrs) {
        }
    };
});