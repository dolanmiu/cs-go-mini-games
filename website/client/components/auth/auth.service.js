/*jslint nomen: true */
/*globals angular */
angular.module('csGoMiniGamesApp').factory('Auth', function Auth($location, $rootScope, $http, User, $cookies, $q, socket) {
    'use strict';
    var currentUser = {};
    /*if ($cookieStore.get('token')) {
        currentUser = User.get();
    }*/

    return {

        /**
         * Authenticate user and save token
         *
         * @param  {Object}   user     - login info
         * @param  {Function} callback - optional
         * @return {Promise}
         */
        login: function (user, callback) {
            var cb = callback || angular.noop,
                deferred = $q.defer();

            $http.post('/auth/local', {
                username: user.username,
                password: user.password
            }).success(function (data) {
                $cookies.put('token', data.token);
                currentUser = User.get();
                deferred.resolve(data);
                return cb();
            }).error(function (err) {
                this.logout();
                deferred.reject(err);
                return cb(err);
            }.bind(this));

            return deferred.promise;
        },

        /**
         * Delete access token and user info
         *
         * @param  {Function}
         */
        logout: function () {
            $cookies.remove('token');
            currentUser = {};
        },

        changeEmail: function (email, callback) {
            var cb = callback || angular.noop;

            return User.changeEmail({}, {
                email: email
            }, function (user) {
                return cb(user);
            }, function (err) {
                return cb(err);
            }).$promise;
        },

        changeTradeUrl: function (tradeUrl, callback) {
            var cb = callback || angular.noop;

            return User.changeTradeUrl({}, {
                tradeUrl: tradeUrl
            }, function (user) {
                return cb(user);
            }, function (err) {
                return cb(err);
            }).$promise;
        },

        changeName: function (name, callback) {
            var cb = callback || angular.noop;

            return User.changeName({}, {
                firstName: name.firstName,
                lastName: name.lastName
            }, function (user) {
                return cb(user);
            }, function (err) {
                return cb(err);
            }).$promise;
        },

        /**
         * Gets all available info on authenticated user
         *
         * @return {Object} user
         */
        getCurrentUser: function () {
            return currentUser;
        },

        /**
         * Check if a user is logged in
         *
         * @return {Boolean}
         */
        isLoggedIn: function () {
            return currentUser.hasOwnProperty('role');
        },

        /**
         * Waits for currentUser to resolve before checking if user is logged in
         */
        isLoggedInAsync: function (cb) {
            if (currentUser.hasOwnProperty('$promise')) {
                currentUser.$promise.then(function () {
                    cb(true);
                }).catch(function () {
                    cb(false);
                });
            } else if (currentUser.hasOwnProperty('role')) {
                cb(true);
            } else {
                cb(false);
            }
        },

        /**
         * Check if a user is an admin
         *
         * @return {Boolean}
         */
        isAdmin: function () {
            return currentUser.role === 'admin';
        },

        /**
         * Get auth token
         */
        getToken: function () {
            return $cookies.get('token');
        },

        refreshCurrentUser: function () {
            var deferred = $q.defer();
            User.get({}, function (user) {
                currentUser = user;
                deferred.resolve(user);
            });
            return deferred.promise;
        },

        assignSocket: function (cb) {
            socket.syncUpdatesSingle('user:' + currentUser._id, currentUser, cb);
        },

        setCurrentUserToUser: function () {
            if ($cookies.get('token')) {
                currentUser = User.get();
            }
        },
        currentUser: currentUser
    };
});