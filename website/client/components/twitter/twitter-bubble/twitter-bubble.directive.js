/*globals angular */
angular.module('csGoMiniGamesApp').directive('twitterBubble', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            post: '='
        },
        templateUrl: 'components/twitter/twitter-bubble/twitter-bubble.html',
        link: function ($scope, $element, $attrs) {
        }
    };
});