/*jslint nomen: true */
/*globals angular */
angular.module('csGoMiniGamesApp').directive('chatBox', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            model: '=',
            modelName: '@',
            height: '@'
        },
        templateUrl: 'components/chat-box/chat-box.html',
        controller: function ($scope, socket, Auth) {

            Auth.isLoggedInAsync(function (response) {
                $scope.isLoggedIn = response;
            });

            function checkCommand(text) {
                var regex = /^\/\w+/i,
                    match = text.match(regex);

                if (match) {
                    return false;
                }
                return true;
            }

            $scope.sendChat = function (formValid) {
                if (!formValid) {
                    return;
                }

                if (!checkCommand($scope.chatMessage)) {
                    $scope.chat.push({
                        error: 'Sorry, commands not supported as of now.'
                    });
                    return;
                }

                $scope.model.chat({}, {
                    message: $scope.chatMessage
                }).$promise.then(angular.noop, function (err) {
                    if (err.data.error === 'spam') {
                        $scope.chat.push(err.data.error);
                    } else {
                        $scope.chat.push(err.data);
                    }
                    if ($scope.chat.length > 50) {
                        $scope.chat.shift();
                    }
                });
                $scope.chatMessage = undefined;
            };

            $scope.chat = $scope.model.getChat();
            socket.syncUpdates($scope.modelName, $scope.chat);

            $scope.$on('$destroy', function () {
                socket.unsyncUpdates($scope.modelName);
            });
        },
        link: function (scope, element, attrs) {}
    };
});