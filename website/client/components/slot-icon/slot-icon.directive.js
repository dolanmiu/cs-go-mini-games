/*globals angular */
angular.module('csGoMiniGamesApp').directive('slotIcon', function () {
    'use strict';

    return {
        restrict: 'E',
        scope: {
            icon: '@',
            link: '@'
        },
        templateUrl: 'components/slot-icon/slot-icon.html',
        link: function ($scope, $element, $attrs) {
        }
    };
});