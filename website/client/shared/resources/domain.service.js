/*jslint regexp: true */
/*globals angular */
angular.module('csGoMiniGamesShared').service('DomainService', function ($window, $location) {
    'use strict';

    function getApiDomain() {
        var url = $window.location.host,
            rawDomain = url.match(/([^.]+\.\w{2,3}(?:\.\w{2})?)$/i);

        if (rawDomain !== null) {
            return $window.location.protocol + '//api.' + rawDomain[1];
        } else {
            return '';
        }
    }

    this.apiDomain = getApiDomain();

    this.transformResource = function (url) {
        return this.apiDomain + url;
        //return url;
    };
});