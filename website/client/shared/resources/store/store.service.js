/*globals angular */
angular.module('csGoMiniGamesShared').factory('Store', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/store/:id/:controller'), {
        id: '@_id'
    }, {
        query: {
            isArray: false
        },
        count: {
            method: 'GET',
            params: {
                controller: 'count'
            }
        },
        buy: {
            method: 'POST',
            params: {
                controller: 'buy'
            }
        }
    });
});