/*globals angular */
angular.module('csGoMiniGamesShared').factory('Trade', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/trade/:id/:controller/:type'), {
        id: '@_id'
    }, {
        withdraw: {
            method: 'POST',
            isArray: true,
            params: {
                controller: 'withdraw'
            }
        },
        depositToInventory: {
            method: 'POST',
            params: {
                controller: 'deposit',
                type: 'inventory'
            }
        },
        depositToCoin: {
            method: 'POST',
            params: {
                controller: 'deposit',
                type: 'coin'
            }
        }
    });
});