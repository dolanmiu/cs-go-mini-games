/*globals angular */
angular.module('csGoMiniGamesShared').factory('LoyaltyPoints', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/loyaltyPoints/:id/:controller'), {
        id: '@_id'
    }, {
        getNextGiveaway: {
            method: 'GET',
            params: {
                controller: 'nextGiveaway'
            }
        },
        getLpLeaderboard: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'lpLeaderboard'
            }
        }
    });
});