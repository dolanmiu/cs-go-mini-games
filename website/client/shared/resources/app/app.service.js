/*globals angular */
angular.module('csGoMiniGamesShared').factory('App', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/app/:controller'), {}, {
        version: {
            method: 'GET',
            params: {
                controller: 'version'
            }
        }
    });
});