/*globals angular */
angular.module('csGoMiniGamesShared').factory('ActiveBots', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/activeBots/:id/:controller'), {
        id: '@_id'
    }, {
        query: {
            method: 'GET',
            isArray: true
        },
        stop: {
            method: 'PUT',
            params: {
                controller: 'stop'
            }
        },
        start: {
            method: 'PUT',
            params: {
                controller: 'start'
            }
        },
        create: {
            method: 'POST'
        },
        remove: {
            method: 'DELETE'
        },
        reboot: {
            method: 'PUT',
            params: {
                controller: 'reboot'
            }
        }
    });
});