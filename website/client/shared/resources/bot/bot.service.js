/*globals angular */
angular.module('csGoMiniGamesShared').factory('Bot', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/bots/:id/:controller'), {
        id: '@_id'
    }, {
        get: {
            method: 'GET'
        },
        create: {
            method: 'POST'
        },
        getStock: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'stock'
            }
        },
        setDepositBot: {
            method: 'PUT',
            params: {
                controller: 'depositBot'
            }
        },
        updateTradeUrl: {
            method: 'PUT',
            params: {
                controller: 'tradeUrl'
            }
        },
        updateStock: {
            method: 'PUT',
            isArray: true,
            params: {
                controller: 'stock'
            }
        }
    });
});