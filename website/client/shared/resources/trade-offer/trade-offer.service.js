/*globals angular */
angular.module('csGoMiniGamesShared').factory('TradeOffer', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/tradeoffers/:id/:controller'), {
        id: '@_id'
    }, {
    });
});