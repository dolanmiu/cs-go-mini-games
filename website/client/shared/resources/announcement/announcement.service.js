/*globals angular */
angular.module('csGoMiniGamesShared').factory('Announcement', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/announcements/:id/:controller'), {
        id: '@_id'
    }, {
    });
});