/*globals angular */
angular.module('csGoMiniGamesShared').factory('Twitter', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/twitter'), {
        id: '@_id'
    }, {
    });
});