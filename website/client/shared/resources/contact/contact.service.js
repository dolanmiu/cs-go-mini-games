/*globals angular */
angular.module('csGoMiniGamesShared').factory('Contact', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/contact'), {}, {
        contact: {
            method: 'POST'
        }
    });
});