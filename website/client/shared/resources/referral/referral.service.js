/*globals angular */
angular.module('csGoMiniGamesShared').factory('Referral', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/referrals/:id/:code/:controller'), {
        id: '@_id'
    }, {
        query: {
            isArray: false
        },
        redeem: {
            method: 'POST',
            params: {
                controller: 'redeem'
            }
        },
        collect: {
            method: 'POST',
            params: {
                controller: 'collect'
            }
        },
        getStats: {
            method: 'GET',
            params: {
                controller: 'stats'
            }
        }
    });
});