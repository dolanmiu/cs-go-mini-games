/*globals angular */
angular.module('csGoMiniGamesShared').factory('JackpotLog', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/jackpotLogs/:id/:controller'), {
        id: '@_id'
    }, {
        create: {
            method: 'POST'
        },
        getBalance: {
            method: 'GET',
            params: {
                id: 'me',
                controller: 'balance'
            }
        }
    });
});