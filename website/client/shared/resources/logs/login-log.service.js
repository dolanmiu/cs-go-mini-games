/*globals angular */
angular.module('csGoMiniGamesShared').factory('LoginLog', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/loginLogs/:id/:controller'), {
        id: '@_id'
    }, {
        get: {
            method: 'GET',
            isArray: true,
            params: {
                id: 'me'
            }
        }
    });
});