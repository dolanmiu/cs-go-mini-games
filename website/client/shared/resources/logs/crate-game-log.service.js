/*globals angular */
angular.module('csGoMiniGamesShared').factory('CrateGameLog', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/crateGameLogs/:id/:controller'), {
        id: '@_id'
    }, {});
});