/*globals angular */
angular.module('csGoMiniGamesShared').factory('Jackpot', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/jackpot/:id/:controller'), {
        id: '@_id'
    }, {
        deposit: {
            method: 'POST',
            isArray: true,
            params: {
                controller: 'deposit'
            }
        },
        config: {
            method: 'GET',
            params: {
                controller: 'config'
            }
        },
        chat: {
            method: 'POST',
            params: {
                controller: 'chat'
            }
        },
        time: {
            method: 'GET',
            params: {
                controller: 'time'
            }
        },
        getChat: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'chat'
            }
        }
    });
});