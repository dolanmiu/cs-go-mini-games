/*globals angular */
angular.module('csGoMiniGamesShared').factory('Crate', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/crates/:id/:controller'), {
        id: '@_id'
    }, {
        getFromName: {
            method: 'GET'
        },
        open: {
            method: 'PUT',
            params: {
                controller: 'open'
            }
        },
        openFree: {
            method: 'PUT',
            params: {
                controller: 'openFree'
            }
        },
        canOpenFree: {
            method: 'GET',
            params: {
                controller: 'canOpenFree'
            }
        },
        chat: {
            method: 'POST',
            params: {
                controller: 'chat'
            }
        },
        getChat: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'chat'
            }
        },
        getNotices: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'notice'
            }
        }
    });
});