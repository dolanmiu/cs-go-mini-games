/*globals angular */
angular.module('csGoMiniGamesShared').factory('InboxMessage', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/inboxMessages/:id/:controller'), {
        id: '@_id'
    }, {
        query: {
            isArray: false
        }
    });
});