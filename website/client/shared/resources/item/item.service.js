/*globals angular */
angular.module('csGoMiniGamesShared').factory('Item', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/items/:id/:controller/:quantity'), {
        id: '@_id'
    }, {
        get: {
            method: 'GET'
        },
        query: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'list'
            }
        },
        count: {
            method: 'GET',
            params: {
                controller: 'count'
            }
        },
        search: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'search'
            }
        },
        getBlacklist: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'blacklist'
            }
        },
        changeBlacklist: {
            method: 'PUT',
            params: {
                controller: 'blacklist'
            }
        },
        getCustom: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'custom'
            }
        },
        addItem: {
            method: 'POST'
        },
        deleteItem: {
            method: 'DELETE'
        }
    });
});