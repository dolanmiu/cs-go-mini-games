/*globals angular */
angular.module('csGoMiniGamesShared').factory('User', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/users/:id/:controller'), {
        id: '@_id'
    }, {
        changeEmail: {
            method: 'PUT',
            params: {
                id: 'me',
                controller: 'email'
            }
        },
        changeTradeUrl: {
            method: 'PUT',
            params: {
                id: 'me',
                controller: 'tradeUrl'

            }
        },
        changeName: {
            method: 'PUT',
            params: {
                id: 'me',
                controller: 'name'
            }
        },
        changeReferral: {
            method: 'PUT',
            params: {
                id: 'me',
                controller: 'referral'
            }
        },
        get: {
            method: 'GET',
            params: {
                id: 'me'
            }
        },
        geMyInventory: {
            method: 'GET',
            isArray: true,
            params: {
                id: 'me',
                controller: 'inventory'
            }
        },
        withdraw: {
            method: 'POST',
            params: {
                id: 'me',
                controller: 'withdraw'
            }
        },
        deposit: {
            method: 'POST',
            params: {
                id: 'me',
                controller: 'deposit'
            }
        }
    });
});