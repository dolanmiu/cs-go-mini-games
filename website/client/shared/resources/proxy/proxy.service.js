/*globals angular */
angular.module('csGoMiniGamesShared').factory('Proxy', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/proxy/:id/:controller'), {
        id: '@_id'
    }, {
        getAvatar: {
            method: 'GET',
            params: {
                controller: 'avatar'
            }
        },
        getSteamInventory: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'steamInventory'
            }
        }
    });
});