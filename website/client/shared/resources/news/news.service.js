/*globals angular */
angular.module('csGoMiniGamesShared').factory('News', function ($resource, DomainService) {
    'use strict';

    return $resource(DomainService.transformResource('/api/news/:id/:controller'), {
        id: '@_id'
    }, {
        query: {
            isArray: false
        },
        search: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'search'
            }
        },
        latest: {
            method: 'GET',
            isArray: true,
            params: {
                controller: 'latest'
            }
        }
    });
});