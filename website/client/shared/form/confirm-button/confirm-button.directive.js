/*globals angular */
angular.module('csGoMiniGamesShared').directive('confirmButton', function () {
    'use strict';
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            preText: '@',
            confirmText: '@',
            execute: '&',
            preClass: '@',
            confirmClass: '@'
        },
        template: '<button ng-hide="isConfirm" class="{{ preClass }}" ng-click="isConfirm = true"><ng-transclude></ng-transclude> {{ preText }}</button><button ng-show="isConfirm" class="{{ confirmClass }}" ng-click="execute()"><ng-transclude></ng-transclude> {{ confirmText }}</button>'
    };
});