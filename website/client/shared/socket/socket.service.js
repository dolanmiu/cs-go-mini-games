/*jslint nomen: true */
/*globals angular, io, _ */
angular.module('csGoMiniGamesShared').factory('socket', function (socketFactory, $window, DomainService) {
    'use strict';

    // socket.io now auto-configures its connection when we ommit a connection url
    var ioSocket = io(DomainService.apiDomain, {
        // Send auth token on connection, you will need to DI the Auth service above
        // 'query': 'token=' + Auth.getToken()
        path: '/socket.io-client'
    });

    var socket = socketFactory({
        ioSocket: ioSocket
    });

    return {
        socket: socket,

        syncUpdatesSingle: function (modelName, syncedItem, cb) {
            cb = cb || angular.noop;

            socket.on(modelName + ':save', function (item) {
                if (item._id !== syncedItem._id) {
                    cb();
                    return;
                }

                _.assign(syncedItem, item);
                cb(item);
            });
        },

        /**
         * Register listeners to sync an array with updates on a model
         *
         * Takes the array we want to sync, the model name that socket updates are sent from,
         * and an optional callback function after new items are updated.
         *
         * @param {String} modelName
         * @param {Array} array
         * @param {Function} cb
         */
        syncUpdates: function (modelName, array, cb) {
            cb = cb || angular.noop;

            /**
             * Syncs item creation/updates on 'model:save'
             */
            socket.on(modelName + ':save', function (item) {
                var oldItem = _.find(array, {
                    _id: item._id
                });
                var index = array.indexOf(oldItem);
                var event = 'created';

                // replace oldItem if it exists
                // otherwise just add item to the collection
                if (oldItem) {
                    array.splice(index, 1, item);
                    event = 'updated';
                } else {
                    array.push(item);
                }

                cb(event, item, array);
            });

            /**
             * Syncs removed items on 'model:remove'
             */
            socket.on(modelName + ':remove', function (item) {
                var event = 'deleted';
                _.remove(array, {
                    _id: item._id
                });
                cb(event, item, array);
            });

            socket.on(modelName + ':removeAll', function () {
                var event = 'deletedAll';
                array.length = 0;
                cb(event, null, array);
            });

            socket.on(modelName + ':bulk', function (items) {
                var event = 'bulk';

                items.forEach(function (item) {
                    array.push(item);
                });
                cb(event, null, array);
            });
        },

        /**
         * Removes listeners for a models updates on the socket
         *
         * @param modelName
         */
        unsyncUpdates: function (modelName) {
            socket.removeAllListeners(modelName + ':save');
            socket.removeAllListeners(modelName + ':remove');
        },

        unsync: function (eventName) {
            socket.removeAllListeners(eventName);
        }
    };
});